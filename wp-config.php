<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'vipweeklydeals');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '4c:T`RA>HVp$0EYI2I2<rBoycLiH=`e#Stuq[Rs3ZV% m-9JQbOKY=Q0]=O+=NEE');
define('SECURE_AUTH_KEY',  'lsA&&@ +FC*8iwc[d&ZdRXH;5e%O0]+:<M?8K~OBf.l 3_~CUqKa[$PFzOk;n]|I');
define('LOGGED_IN_KEY',    'QDQq:x^`@KY<61r+OFzb<X{h]@h&gPnZ7OPKJt@-KAKy=7 ~#S@9x;}4Lene!I.X');
define('NONCE_KEY',        'kjZb6GPW<5`qz+/lz0$++Spa&t(jOt`{!C;z@.bJ[#KyyakVm]T S@#w0C_F>CV9');
define('AUTH_SALT',        'dC?|QevyXt[Wp+-Q~FmHwjRJCGQ@>#G$#Vv-3LEv5wlwQ-V^>?*{{HAkTcoyUQ&B');
define('SECURE_AUTH_SALT', 'KyQ,yhf(J1u!JCaeH^+zi.AQSP(5ghh:oM$nv3QzxRLo*ju~=^#/;g8e$17=*)BZ');
define('LOGGED_IN_SALT',   '2@]x?chL[8!gi>bE![NJ-lF-i}A#v),x5D<ltlh$40vjgGIQG-V+k|1Y#ZTtx2UW');
define('NONCE_SALT',       '}WcY~|[x3zGcW*__`|4*8Bal3jwO+{~1sV?w*;9|.zy.|n&K=26X;|1|;LHcx`R]');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
