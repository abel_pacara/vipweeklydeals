<?php 
/*
Plugin Name: WooCommerce CBA (Commonwealth Bank Australia) Payment
Plugin URI: http://extensionworks.com
Description: Woocommerce payment gateway: Commonwealth Bank (Australia)
Version: 1.3.2
Author: Extension Works
Author URI: http://extensionworks.com/
Copyright: © Extension Works.

*/


/**
 * Required functions
 */
if ( ! function_exists( 'extensionworks_queue_update' ) )
    require_once( 'extensionworks-includes/extensionworks-functions.php' );

/**
 * Plugin updates
 */
extensionworks_queue_update( plugin_basename( __FILE__ ), 'e18e8be9bb9844361187c073f51728df', 'woo-commonwealth-bank' );

add_action('plugins_loaded', 'woocommerce_cba_init');

function woocommerce_cba_init() {
    if ( !(is_framework_active() && is_woo_active()) )
        return;

    class WC_Gateway_CBA extends EW_Payer {

        var $id = 'cba';
        var $method_title = 'CBA';
        // var $method_description = 'Process online payment with CommonWealth Bank Australia.';
        var $url = "https://migs.mastercard.com.au/vpcdps";
        var $query = array(
            'vpc_Version' => '1',
            'vpc_Command' => 'pay'
        );
        //var $avaiable_card_types = array('VISA','MC','AMEX','DINERS');
        var $avaiable_card_types = array('VISA','MC','DINERS');

        // The constructor
        public function __construct() {
            parent::__construct();
            $this->icon = WP_PLUGIN_URL . "/" . plugin_basename(dirname(__FILE__)) . '/images/commonwealth.png';

           

            // // Define user set variables
            // $this->enabled = $this->settings['enabled'];
            // $this->title = $this->settings['title'];
            // $this->testmode = $this->settings['testmode'];
            // $this->description = 'Secure payment through CommonWealth.';
            // $this->currency = get_option('woocommerce_currency');

            $this->query['vpc_AccessCode'] = $this->vpc_AccessCode;
            $this->query['vpc_Merchant'] = $this->vpc_Merchant;
            $this->query['vpc_OrderInfo'] = $this->vpc_OrderInfo;

            // Save admin options
            // add_action('woocommerce_update_options_payment_gateways', array(&$this, 'process_admin_options'));

        }

        /**
         * Initialise Gateway Settings Form Fields
         */
        function add_form_fields() {

            $this->form_fields['vpc_Merchant'] = array(
                'title'       => __('CommonWealth Merchant ID', 'woothemes'),
                'type'        => 'text',
                'description' => __('The Merchant ID will be of the format "ABC0010", where ABC is your unique three-letter account code.', 'woothemes'),
                'default'     => ''
            );

            $this->form_fields['vpc_AccessCode'] = array(
                'title'       => __('CommonWealth Merchant AccessCode', 'woothemes'),
                'type'        => 'text',
                'description' => __('The Access Code is provided by the CommonWealth bank', 'woothemes'),
                'default'     => ''
            );

            $this->form_fields['vpc_OrderInfo'] = array(
                'title'       => __('Order Info (optional)', 'woothemes'),
                'type'        => 'text',
                'description' => __('This will be appended to your transaction detail.', 'woothemes'),
                'default'     => ''
            );
        }

       

        /**
         * Get the users country either from their order, or from their customer data
         */
        function get_country_code() {
            global $woocommerce;

            if (isset($_GET['order_id']) && $_GET['order_id'] > 0) :

                $order = new WC_Order($_GET['order_id']);

                return $order->billing_country;

            elseif ($woocommerce->customer->get_country()) :

                return $woocommerce->customer->get_country();

            endif;

            return NULL;
        }

        /**
         * Check if this gateway is enabled and available in the user's country
         */
        function is_available() {
            if ($this->enabled == "no")
                return false;

            if (get_option('woocommerce_force_ssl_checkout') == 'no' && $this->testmode == 'no')
                return false;

            if ($this->get_currency() != "AUD")
                return false;

            // Required fields check
            if (!$this->query['vpc_AccessCode'] && !$this->query['vpc_Merchant'])
                return false;

            return true;
        }

        

        /**
         * Process the payment
         */
        function process_payment($order_id) {
            global $woocommerce;

            $order = new WC_Order($order_id);
            $card_number = $this->get_post('card_number');

            $card_csc = $this->get_post('card_csc');
            $card_exp_month = $this->get_post('card_expiration_month');
            $card_exp_year = $this->get_post('card_expiration_year');

            // Format card expiration data
            $card_exp_month = (int) $card_exp_month;
            if ($card_exp_month < 10) :
                $card_exp_month = '0' . $card_exp_month;
            endif;

            $this->query['vpc_Amount'] = $order->order_total * 100;
            $this->query['vpc_CardNum'] = $card_number;
            $this->query['vpc_MerchTxnRef'] = sha1($order->order_key . microtime(false));
            $this->query['vpc_OrderInfo'] = $this->query['vpc_OrderInfo'] . "Order number: " . $order->get_order_number();
            $this->query['vpc_CardSecurityCode'] = $card_csc;
            $this->query['vpc_CardExp'] = $card_exp_year . $card_exp_month;

            // Send request to Commonwealth Bank
            try {
                $post_data = http_build_query($this->query);

                $response = wp_remote_post($this->url, array(
                    'method' => 'POST',
                    'body' => $post_data,
                    'timeout' => 70,
                    'sslverify' => false
                    ));

                if (is_wp_error($response))
                    throw new Exception(__('There was a problem connecting to the payment gateway.', 'woothemes'));

                if (empty($response['body']))
                    throw new Exception(__('Empty response.', 'woothemes'));

                parse_str($response['body'], $parsed_response);
                $response_code = $parsed_response['vpc_TxnResponseCode'];

                switch ($response_code) :
                    case '0':

                        // Add order note
                        $order->add_order_note(sprintf(__($this->method_title . ' payment completed', 'woothemes')));

                        // Payment complete
                        $order->payment_complete();

                        // Remove cart
                        $woocommerce->cart->empty_cart();

                        // Empty awaiting payment session
                        unset($_SESSION['order_awaiting_payment']);

                        // Return thank you page redirect
                        return array(
                            'result' => 'success',
                            'redirect' => add_query_arg('key', $order->order_key, add_query_arg('order', $order_id, get_permalink(get_option('woocommerce_thanks_page_id'))))
                        );

                        break;
                    case '1':
                    case '2':
                    case '3':
                    case '4':
                    case '5':
                    case '6':
                    case '7':
                    case '8':
                    case '9':
                        // Payment failed :(
                        $order->add_order_note(sprintf(__($this->method_title . ' payment failed due to an error: %s', 'woothemes'), $parsed_response['vpc_Message']));

                        $woocommerce->add_error(__('Payment error: ', 'woothemes') . $parsed_response['vpc_Message']);
                        return;

                        break;
                    default:
                        // Payment failed :(
                        $order->add_order_note(sprintf(__($this->method_title . ' payment failed due to an error: %s', 'woothemes'), $parsed_response['vpc_Message']));

                        $woocommerce->add_error(__('Payment error: ', 'woothemes') . $parsed_response['vpc_Message']);
                        return;

                        break;
                endswitch;
            } catch (Exception $e) {
                $woocommerce->add_error(__('Connection error:', 'woothemes') . ': "' . $e->getMessage() . '"');
                return;
            }
        }

    }

    /**
     * Add the Commonwealth Bank gateway to WooCommerce
     **/
    function add_cba_gateway($methods) {
        $methods[] = 'WC_Gateway_CBA';
        return $methods;
    }

    add_filter('woocommerce_payment_gateways', 'add_cba_gateway');
}
