=== WooCommerce Commonwealth Bank Australia gateway ===

A payment gateway for Commonwealth Bank Australia. A CBA account, Curl support, and a server with SSL support and an SSL certificate is required (for security reasons) for this gateway to function.

== Important Note ==

You *must* enable SSL from the settings panel to use this plugin in live mode - this is for your customers safety and security.

To make a successful test transaction, please make sure the total value of the cart is $1.00.

== Support ==

If you have any problems, questions or suggestions please post on the Extension Works support forums: http://help.extensionworks.com
