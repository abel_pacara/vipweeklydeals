<?php 

class EW_Payer extends WC_Payment_Gateway{

	var $id;
	var $has_fields = true;

	var $method_title ='';

	var $currency;
	var $icon;

	var $ssl_required = false;

	var $avaiable_card_types;
	var $card_icon = array();
	public function __construct(){


		// Load the form fields
		$this->init_form_fields();
		$this->add_form_fields();
		// Load the settings.
		$this->init_settings();
		
		foreach ( $this->settings as $key => $value ){
			if(array_key_exists($key, $this->form_fields)) $this->$key = $value;
        }


        $this->card_icon = array(
        	"VISA"     =>	   plugins_url('images/card_visa.png', __FILE__),
			"MC" => plugins_url('images/card_mastercard.png', __FILE__),
			"AMEX"    =>   plugins_url('images/card_amex.png', __FILE__),
			"DINERS"   =>  plugins_url('images/card_diners.png', __FILE__),
			"JCB"       => plugins_url('images/card_jcb.png', __FILE__)
        );
        add_action('admin_notices', array(&$this, 'notification'));
        add_action('wp_head', array(&$this, 'checkout_css_styles'));
        add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );
	}

	function checkout_css_styles() {
			?><style type="text/css">
				.eway_card_type_radios {
					overflow: hidden;
					zoom: 1;
				}
				.eway_card_type_radios label {
					float: left; 
					margin-right: 12px;
					line-height: 25px;
				}
				.eway_card_type_radios .card_type input {
					margin-right: 6px;
				}
				.eway_card_type_radios .card_type input, .eway_card_type_radios .card_type img {
					vertical-align: middle;
					float: none;
				}
			</style><?php
		}

	function init_form_fields(){
		$this->form_fields = array(
			    'enabled' => array(
					'title'       => __('Enable/Disable', 'woothemes'),
					'label'       => __('Enable ' . $this->method_title, 'woothemes'),
					'type'        => 'checkbox',
					'description' => '',
					'default'     => 'no'
			    ),
			    'testmode' => array(
					'title'       => __( $this->method_title .' test', 'woothemes'),
					'label'       => __('Enable test mode', 'woothemes'),
					'type'        => 'checkbox',
					'description' => __('Place the payment gateway in development mode.', 'woothemes'),
					'default'     => 'no'
			    ),
			    'title' => array(
					'title'       => __('Title', 'woothemes'),
					'type'        => 'text',
					'description' => __('This controls the title which the user sees during checkout.', 'woothemes'),
					'default'     => __('Credit card (' . $this->method_title . ')', 'woothemes')
			    ),
			    'description' => array(
					'title'       => __('Description', 'woothemes'),
					'type'        => 'textarea',
					'description' => __('This controls the description which the user sees during checkout.', 'woothemes'),
					'default'     => 'Pay with your credit card via ' . $this->method_title .' gateway.'
			    )
			);
	}

	public function add_form_fields(){}


	function payment_fields(){
		?>
		<?php if ($this->testmode == 'yes') : ?><p><?php _e('TEST MODE/SANDBOX ENABLED', 'woothemes'); ?></p><?php endif; ?>
		<?php if ($this->description) : ?><p><?php echo $this->description; ?></p><?php endif; ?>
		<fieldset>
			<p class="form-row form-row-wide eway_card_type_radios">
				<label><?php echo __("Card type", 'woocommerce') ?>:</label>
				<?php foreach ($this->avaiable_card_types as $type ) : ?>
					<label class="card_type"><input type="radio" name="<?php echo $this->id; ?>_card_type" <?php checked($type, "VISA"); ?> value="<?php echo $type ?>" /><img width="38" src="<?php echo $this->card_icon[ $type ]; ?>" /></label>
				<?php endforeach; ?>
			</p>

			<div class="clear"></div>

			<p class="form-row form-row-first">
				<label for="<?php echo $this->id; ?>_card_holder"><?php echo __("Card Holder's Name", 'woocommerce') ?> <span class="required">*</span></label>
				<input type="text" class="input-text" name="<?php echo $this->id; ?> _card_holder" />
			</p>

			<p class="form-row form-row-last">
				<label for="<?php echo $this->id; ?>_card_number"><?php echo __("Credit Card number", 'woocommerce') ?> <span class="required">*</span></label>
				<input type="text" class="input-text" name="<?php echo $this->id; ?>_card_number" id="<?php echo $this->id; ?>_card_number" />
			</p>

			<div class="clear"></div>

			<p class="form-row form-row-first">
				<label for="cc-expire-month"><?php echo __("Expiration date", 'woocommerce') ?> <span class="required">*</span></label>
				<select name="<?php echo $this->id; ?>_card_expiration_month" id="cc-expire-month" class="woocommerce-select woocommerce-cc-month">
					<option value=""><?php _e('Month', 'woocommerce') ?></option>
					<?php
					$months = array();
					for ($i = 1; $i <= 12; $i++) :
						$timestamp = mktime(0, 0, 0, $i, 1);
						$months[date('n', $timestamp)] = date('F', $timestamp);
					endfor;
					foreach ($months as $num => $name)
						printf('<option value="%u">%s</option>', $num, $name);
					?>
				</select>
				<select name="<?php echo $this->id; ?>_card_expiration_year" id="cc-expire-year" class="woocommerce-select woocommerce-cc-year">
					<option value=""><?php _e('Year', 'woocommerce') ?></option>
					<?php
					for ($i = date('y'); $i <= date('y') + 15; $i++)
						printf('<option value="%u">20%u</option>', $i, $i);
					?>
				</select>
			</p>
			<p class="form-row form-row-last">
				<label for="<?php echo $this->id; ?>_card_csc"><?php _e("Card security code", 'woocommerce') ?> <span class="required">*</span></label>
				<input type="text" class="input-text" id="<?php echo $this->id; ?>_card_csc" name="<?php echo $this->id; ?>_card_csc" maxlength="4" style="width:4em;" />
				<span class="help eway_card_csc_description"></span>
			</p>
			<div class="clear"></div>
		</fieldset>
		<script type="text/javascript">    
								                                                                                                                                                			
			jQuery("#<?php echo $this->id; ?>_card_type").click(function(){
								                                                                                                                                                					
				var card_type = jQuery(".<?php echo $this->id; ?>_card_type_radios input:radio:checked").val();
				var csc = jQuery("#<?php echo $this->id ?>_card_csc").parent();    
								                                                                                                                                                					
				if (card_type == "VISA" || card_type == "MC" || card_type == "DINERS" || card_type == "JCB") {
					jQuery('.<?php echo $this-id; ?>_card_csc_description').text("<?php _e('3 digits usually found on the signature strip.', 'woocommerce'); ?>");
				} else if ( card_type == "AMEX" ) {
					jQuery('.<?php echo $this->id; ?>_card_csc_description').text("<?php _e('4 digits usually found on the front of the card.', 'woocommerce'); ?>");
				} else {
					jQuery('.<?php echo $this->id; ?>_card_csc_description').text('');
				}
								                                                                                                                                                					
			}).eq(0).click();
								                                                                                                                                                			
		</script>
		<?php
	}


	 function validate_fields() {
        return $this->validate_card();
    }

    /**
	 * receipt_page
	 */
	public function receipt_page( $order ) {
		echo '<p>' . __( 'Thank you for your order.', 'wc_realex' ) . '</p>';
	}

    public function validate_card(){
    	$card_number = $this->get_post( 'card_number' );
        $card_holder = $this->get_post( 'card_holder' );
        $card_csc = $this->get_post( 'card_csc' );
        $card_exp_month = $this->get_post( 'card_expiration_month' );
        $card_exp_year = $this->get_post( 'card_expiration_year' );
        $issue_number = $this->get_post( 'issueNumber' );
		$card_type = $this->get_post( 'card_type');

        if (!ctype_digit($card_csc)) :
        	$this->add_error_message('Card security code is invalid (only digits are allowed)');
            return false;
        endif;

        if ((strlen($card_csc) != 3 && in_array($card_type, array('VISA', 'MC'))) || (strlen($card_csc) != 4 && $card_type == 'AMEX')) :
        	$this->add_error_message('Card security code is invalid (wrong length)');
            return false;
        endif;

        if ( $card_type == 'SWITCH' ):
            
            if ( ! ctype_digit( $issue_number ) ) {
            	$this->add_error_message('Switch issue number is invalid (only digits are allowed)');
                return false;
            }
    
            if ( strlen( $issue_number ) > 3 ) {
            	$this->add_error_message('Switch issue number is invalid (wrong length)');
                return false;
            }
        endif;

        // Check card expiration data
        if (
                !ctype_digit($card_exp_month) ||
                !ctype_digit($card_exp_year) ||
                $card_exp_month > 12 ||
                $card_exp_month < 1 ||
                $card_exp_year < date('y') ||
                $card_exp_year > date('y') + 20
        ) :
            $this->add_error_message('Card expiration date is invalid');
            return false;
        endif;

        // Check card number
        $card_number = str_replace(array(' ', '-'), '', $card_number);

        if ( empty( $card_number ) || ! ctype_digit( $card_number ) || 
			     strlen( $card_number ) < 12 || strlen( $card_number ) > 19 || 
			     ! $this->luhn_check( $card_number ) ) {
				$this->add_error_message('Card number is invalid' );
				return false;
			}

        return true;
    }

    public function luhn_check( $card_number ) {
			$sum = 0;
			for ( $i = 0, $ix = strlen( $card_number ); $i < $ix - 1; $i++) {
				$weight = substr( $card_number, $ix - ( $i + 2 ), 1 ) * ( 2 - ( $i % 2 ) );
				$sum += $weight < 10 ? $weight : $weight - 9;
			}
			
			return substr( $card_number, $ix - 1 ) == ( ( 10 - $sum % 10 ) % 10 );
		}

    function add_error_message( $msg ){
    	global $woocommerce;
    	$woocommerce->add_error(__( $msg, 'extensionworks'));
    }

	function admin_options() {

		parent::admin_options();

		ob_start();
		?>
			
		<?php
	}

	public function get_post( $name ){
        $name = $this->id . '_' . $name;
        if( isset( $_POST[ $name ]))
            return woocommerce_clean( $_POST[ $name ]);
        else
            return null;
    }

	/**
	 * Get user's IP address
	 */
	function get_user_ip() {
		return (isset($_SERVER['HTTP_X_FORWARD_FOR']) && !empty($_SERVER['HTTP_X_FORWARD_FOR'])) ? $_SERVER['HTTP_X_FORWARD_FOR'] : $_SERVER['REMOTE_ADDR'];
	}

	public function notification(){

		if( $this->ssl_required){
			if (get_option('woocommerce_force_ssl_checkout') == 'no'){
				echo '<div class="error"><p>' . sprintf(__( $this->method_title . ' require ssl, but the <a href="%s">force SSL option</a> is disabled; your checkout is not secure! Please enable SSL and ensure your server has a valid SSL certificate', 'woothemes'), admin_url('admin.php?page=woocommerce')) . '</p></div>';
			}
		}

		$this->check_dependency();

	}


	public function check_dependency(){

	}

	public function get_currency(){
		return get_woocommerce_currency();
	}

}

?>