<?php
/**
 * Product Class with girth and lettermail enabled
 *
 * @class       EW_Product
 * @version     1.0
 * @package     ExtensionWorks-Shipper/Classes
 * @author      Extension Works
 */

class EW_Product{


    var $data;
    var $product_custom_fields;

    /**
     * Loads all product data from custom fields.
     *
     * @param int $id ID of the product to load
     */
    function __construct( $product ) {

        $this->data = $product['data'];


        $this->quantity = isset( $product['quantity']) ?$product['quantity']: 1 ;
        $this->id = $this->product_id = isset( $product['product_id']) ?$product['product_id'] : '';
        // $this->id = (int) $id;

        $this->product_custom_fields = get_post_custom( $this->product_id );

        // Define the data we're going to load: Key => Default value
        $load_data = array(
            'sku'           => '',
            'downloadable'  => 'no',
            'virtual'       => 'no',
            'price'         => '',
            'visibility'    => 'hidden',
            'stock'         => 0,
            'stock_status'  => 'instock',
            'backorders'    => 'no',
            'manage_stock'  => 'no',
            'sale_price'    => '',
            'regular_price' => '',
            'weight'        => '',
            'length'        => '',
            'width'     => '',
            'height'        => '',
            'tax_status'    => 'taxable',
            'tax_class'     => '',
            'upsell_ids'    => array(),
            'crosssell_ids' => array(),
            'sale_price_dates_from' => '',
            'sale_price_dates_to'   => '',
            'min_variation_price'   => '',
            'max_variation_price'   => '',
            'min_variation_regular_price'   => '',
            'max_variation_regular_price'   => '',
            'min_variation_sale_price'  => '',
            'max_variation_sale_price'  => '',
            'featured'      => 'no',
            'girth'         => '',
            'letter_mail'   => ''
        );

        // Load the data from the custom fields
        foreach ($load_data as $key => $default) $this->$key = (isset($this->product_custom_fields['_' . $key][0]) && $this->product_custom_fields['_' . $key][0]!=='') ? $this->product_custom_fields['_' . $key][0] : $default;

        if( !$this->letter_mail )
            $this->letter_mail = isset( $this->data->letter_mail) ? $this->data->letter_mail : 'no';


        if( isset( $this->data->width ) )
            $this->width = $this->data->width;
        if( isset( $this->data->length ) )
            $this->length = $this->data->length;
        if( isset( $this->data->weight ) )
            $this->weight = $this->data->weight;
        if( isset( $this->data->height ) )
            $this->height = $this->data->height;
       

        $this->dimension_unit = get_option( 'woocommerce_dimension_unit' );
        $this->weight_unit = get_option('woocommerce_weight_unit');
        
    }

    function get_width(){
        return $this->width;
    }

    function get_height(){
        return $this->height;
    }

    function get_length(){
        return $this->length;
    }

    function exists(){
        return $this->data->exists();
    }

    function get_weight() {
        if ( $this->weight ) return $this->weight;
    }

    function needs_shipping() {
        return $this->data->needs_shipping();
    }

    function get_volume(){
        return $this->width * $this->length * $this->height;
    }

    function get_shipping_class(){

        return $this->data->get_shipping_class();
    }
    function get_price(){
        return $this->data->get_price();
    }
}

?>
