<?php 

class EW_Packing_Robat{

	/**
    * @var array
    */
    var $items = array();

    /**
    * @var array
    */
    var $containers = array();

    /**
    * @var array
    */
    var $unpackable_items = array();

    /**
     * @var array
     */
    var $packable_items = array();
    /**
    * @var array
    */
    var $weight_limit = 0;

    /**
    * @var string
    */
    var $weight_unit = 'lbs';

    /**
    * @var string
    */
    var $dimension_unit = 'in';

    /**
     * @var string
     */
    var $from_weight_unit = 'lbs';

    /**
     * @var string
     */
    var $from_dimension_unit = 'in';

    /**
     * @var boolean
     */
    var $weight_only = false;

    /**
     * @var [type]
     */
    var $packing_method;

    /**
     * @var string
     */
    var $packing_type = 'togather';

    public function __construct( $products, $containers, $dimension_unit='in', $weight_unit='lbs' ){

        $this->set_weight_unit( $weight_unit );
        $this->set_dimension_unit( $dimension_unit );

    	// Get containers, with their reset dimensions
        if ( !is_array($containers) )
            $containers = array();

        foreach ( $containers as $container ) {
            $container->weight_unit = $this->weight_unit;
            $container->dimension_unit = $this->dimension_unit;
            $this->reset_position( $container );
            $this->unify_dimensions( $container );
            $this->unify_weight( $container );
            $this->containers[] = clone $container;
        }

        // Put shippable products into package regardless their order or details.
        foreach ( $products as $product_id => $product ) {
            $item = $product->data;
            $quantity = $product->quantity;
            
            if ( $item->exists() && $quantity > 0 && $item->needs_shipping() ) {
                $this->reset_position( $product );
                $this->unify_dimensions( $product );
                $this->unify_weight( $product );
            	for( $i =0; $i < $quantity; $i++ )
                	$this->items[] = $product;
            }

        }
       
        $this->items = $this->sort_products( $this->items, 'desc' );
        $this->containers = $this->sort_containers( $this->containers );
    }

    public function set_packing_type( $type ){

    	$this->packing_type = $type;
    }

    public function prepare_packing(){
    	$this->unpackable_items = $this->packing_method->find_unpackable_products( $this->items, $this->containers );
    	
  		$this->packable_items = array_udiff( $this->items, $this->unpackable_items, array($this,'compare_objects'));
        

    }

    public function compare_objects($obj_a, $obj_b) {
        return $obj_a->id - $obj_b->id;
    }

    public function packing(){

    	$this->prepare_packing();
    	$this->packable_items = $this->sort_products( $this->packable_items );
    	return $this->packing_method->packing( $this->packable_items, $this->containers, $this->packing_type, $weight_only=false );
    }

    public function set_packing_method( $method ){
    	$this->packing_method = $method;
    }

    public function get_unpackable_products(){
    	return $this->unpackable_items;
    }

    function convert_dimension( $dim, $from_unit, $to_unit ) {

		$from_unit 	= strtolower( $from_unit );
		$to_unit	= strtolower( $to_unit );

		// Unify all units to cm first
		if ( $from_unit !== $to_unit ) {

			switch ( $from_unit ) {
				case 'in':
					$dim *= 2.54;
				break;
				case 'm':
					$dim *= 100;
				break;
				case 'mm':
					$dim *= 0.1;
				break;
				case 'yd':
					$dim *= 0.010936133;
				break;
			}

			// Output desired unit
			switch ( $to_unit ) {
				case 'in':
					$dim *= 0.3937;
				break;
				case 'm':
					$dim *= 0.01;
				break;
				case 'mm':
					$dim *= 10;
				break;
				case 'yd':
					$dim *= 91.44;
				break;
			}
		}
		return ( $dim < 0 ) ? 0 : $dim;
	}

    function convert_weight( $weight, $from_unit, $to_unit ) {

	$from_unit 	= strtolower( $from_unit );
	$to_unit	= strtolower( $to_unit );

	//Unify all units to kg first
	if ( $from_unit !== $to_unit ) {

		switch ( $from_unit ) {
			case 'g':
				$weight *= 0.001;
			break;
			case 'lbs':
				$weight *= 0.4536;
			break;
			case 'oz':
				$weight *= 0.0283;
			break;
		}

		// Output desired unit
		switch ( $to_unit ) {
			case 'g':
				$weight *= 1000;
			break;
			case 'lbs':
				$weight *= 2.2046;
			break;
			case 'oz':
				$weight *= 35.274;
			break;
		}
	}
	return ( $weight < 0 ) ? 0 : $weight;
}

    public function set_dimension_unit( $unit ){

        if( $unit )
            $this->dimension_unit = $unit;
    }

    public function set_weight_unit( $unit ){
        if( $unit )
            $this->weight_unit = $unit;
    }

    /**
    * Reset the entity to have its dimension in such order:
    * Height <= Width <= Length
    */
    public function reset_position ( &$entity ) {
        $dimensions = array($entity->height, $entity->width, $entity->length);
        sort($dimensions);
        list($entity->height, $entity->width, $entity->length) = $dimensions;
    }

    /**
    * convert weight to something usable by the package
    */
    public function unify_weight ( &$item ) {
  
        $item->weight = $this->convert_weight( $item->get_weight(),$item->weight_unit, $this->weight_unit );
        $item->weight_unit = $this->weight_unit;
    }

    /**
    * convert dimensions to something usable by the package
    */
    public function unify_dimensions ( &$item ) {
        
        $dimensions = array( 'width', 'height', 'length' );
        foreach( $dimensions as $dimension ) {
            $item->$dimension = $this->convert_dimension( $item->$dimension, $item->dimension_unit, $this->dimension_unit );
        }
        $item->dimension_unit = $this->dimension_unit;

    }


	/**
	 * order true is descent,
	 * @param  [type]  $products [description]
	 * @param  boolean $order    [description]
	 * @return [type]            [description]
	 */
	function sort_products( $products, $order = 'asc' ){
		if( is_array( $products ))
			usort( $products, array('EW_Packing_Robat', 'compare_volume') );

		 if ( $order == 'desc' )
            return array_reverse( $products );
        
        return $products;
	}

	function sort_containers( $containers, $order = 'asc' ){
		if( is_array( $containers ))
			usort( $containers, array('EW_Packing_Robat', 'compare_volume') );

		 if ( $order == 'desc' )
            return array_reverse( $containers );
        
        return $containers;
	}

	public static function compare_volume ( $alpha, $omega ) {
        return ( $alpha->get_volume() > $omega->get_volume() ) ? 1 : -1;
    }

    public static function compare_container ( $alpha, $omega ) {
        return ( self::get_container_volume( $alpha ) > self::get_container_volume( $omega ) ) ? 1 : -1;
    }


}

?>