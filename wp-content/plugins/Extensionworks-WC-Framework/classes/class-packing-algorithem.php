<?php 

class EW_Packing_Algorithm{

	 /**
    * @var array
    */
    var $items = array();

    /**
    * @var array
    */
    var $containers = array();

    /**
    * @var array
    */
    var $packed_containers = array();

    /**
    * @var array
    */
    var $packed_items = array();

    /**
    * @var array
    */
    var $unpackable_items = array();

    /**
    * @var array
    */
    var $weight_limit = 0;

    /**
    * @var string
    */
    var $weight_unit = 'lbs';

    /**
    * @var string
    */
    var $dimension_unit = 'in';

	public function packing( $products, $containers ){
		
	}

	function get_container_volume( $container ){
		return $container->width * $container->length * $container->height;
	}

	function get_product_volume( $product ){
		return $product->width * $product->length * $product->height;
	}

	function get_products_total_volume( $products ){

		$total_volume = 0.0;
		foreach ($products as $product ) {
			$total_volume += $this->get_product_volume( $product );
		}
		return $total_volume;
	}

	function get_products_total_weight( $products ){

		$total_weight = 0.0;
		foreach ($products as $product ) {
			$total_weight += $product->weight;
		}
		return $total_weight;
	}

	/**
	 * order true is descent,
	 * @param  [type]  $products [description]
	 * @param  boolean $order    [description]
	 * @return [type]            [description]
	 */
	function sort_products( $products, $order = 'asc' ){
		if( is_array( $products ))
			usort( $products, array('EW_Packing_Algorithm', 'compare_product') );

		 if ( $order == 'desc' )
            return array_reverse( $products );
        
        return $products;
	}

	function sort_containers( $containers, $order = 'asc' ){
		if( is_array( $containers ))
			usort( $containers, array('EW_Packing_Algorithm', 'compare_product') );

		 if ( $order == 'desc' )
            return array_reverse( $containers );
        
        return $containers;
	}

	public static function compare_product ( $alpha, $omega ) {
        return ( self::get_product_volume( $alpha ) > self::get_product_volume( $omega ) ) ? 1 : -1;
    }

    public static function compare_container ( $alpha, $omega ) {
        return ( self::get_container_volume( $alpha ) > self::get_container_volume( $omega ) ) ? 1 : -1;
    }

}

?>