<?php

class EW_Volume_Packing extends EW_Packing_Algorithm{

	public function packing( $products, $containers, $type, $weight_only ){

		$this->containers = $containers;
		$this->products = $products;

		if( $type == 'together'){
			$unpacked_products = $this->pack_products( $products, $this->containers, $weight_only );
			while ( count( $unpacked_products) > 0 ) {
				$unpacked_products = $this->pack_products( $unpacked_products, $containers, $weight_only );
			}
		}else if( $type == 'separate'){

			foreach ($products as $product ) {
				if( $container = $this->can_fit_in_containers( $product, $containers ) ){
					$container->put( $product );
					$this->packed_containers[] = $container;
				}
			}

		}
		
		return $this->packed_containers;
	}



	function find_unpackable_products( $products, $containers ){

		$unpackable_items = array();

		foreach ($products as $product ) {
			if( !$this->can_fit_in_containers( $product, $containers ) )
				$unpackable_items[] = $product;
		}
		
		return $unpackable_items;
	}

	function can_fit_in_containers( $product, $containers ){

		foreach ($containers as $container ) {
			if( $container->can_put( $product ))
				return clone $container;
		}
		return false;
	}

	function can_fit_in_container( $product, $container, $weight =false ){

		if( $product->get_volume() > $container->get_volume() ){
			// echo '<pre>can_fit_in_container volume false</pre>';
			return false;
		}
			

		$container_weight = count( $container->items ) == 0 ? $container->get_max_weight() : $container->get_available_weight();
		if(  $container_weight && $product->get_weight() > $container_weight ){
			// echo '<pre>can_fit_in_container weight false</pre>';
			// echo '<pre>product weight:'. $product->get_weight(). ' container weight:'.$container_weight. '</pre>';
			return false;
		}
			

		if( $weight && $product->get_weight() < $container->get_available_weight() ){
			// echo '<pre>can_fit_in_container  weight only true</pre>';
			return true;
		}

		if( $product->get_length() > $container->length ){
			// echo '<pre>can_fit_in_container  length only true</pre>';
			return false;
		}

		if( !( $product->width <= $container->width && $product->length <= $container->length && $product->height <= $container->height) )
		{
			// echo '<pre>can_fit_in_container  size only true</pre>';
			return false;
		}	

		return true;
	}

	function pack_products( $products, $containers, $weight_only ){
		$unpacked_products= array();
		$loop = count( $products);
		for( $i=0; $i < $loop; $i++ ){

			//find suitable container
			if( $container = $this->find_suitable_container( $products, $containers, $weight_only ) ){
				
				//put products into container
				foreach ($products as $product) {
					$container->put( $product );
					$this->packed_items[] = $product;
				}

				$this->packed_containers[] = $container;
				return $unpacked_products;
			}else{
				//remove smallest one
				$unpacked_products[] = array_shift( $products );
			}
		}
		return $unpacked_products;
	}

	function find_suitable_container( $products, $containers, $weight_only){
		foreach ($containers as $container) {
			if( $container->can_put( $products ) ){
				$container = clone $container;
				return $container;
			}
		}
		return false;
	}

	public function can_put( $products, $container, $weight = false, $max_unit=false ){
		
		$products_volume = $this->get_products_total_volume( $products );

		$container_volume = $this->get_container_volume( $container );


		if( $this->get_products_total_volume( $products ) > $container->get_available_volume() )
			return false;

		if( $weight && ( $this->get_products_total_weight( $products) > $container->get_available_weight() ) )
			return false;

		if( $this->get_max_length_in_products( $products) > $container->get_length() || 
			$this->get_max_height_in_products( $products) > $container->get_height() ||
			$this->get_max_width_in_products( $products) > $container->get_width()
		 )
			return false;

		if( $max_unit && $container->max_unit && ($container->get_unit_count() > $container->max_unit ) ) 
			return false;

		return true;

	}

	private function get_max_length_in_products( $products ){

		$length = 0.0;
		foreach ($products as $product) {
			if( $product->get_length() > $length )
				$length = $product->get_length();
		}

		return $length;
	}
	private function get_max_height_in_products( $products ){

		$height = 0.0;
		foreach ($products as $product) {
			if( $product->get_height() > $height )
				$height = $product->get_height();
		}

		return $height;
	}
	private function get_max_width_in_products( $products ){

		$width = 0.0;
		foreach ($products as $product) {
			if( $product->get_width() > $width )
				$width = $product->get_width();
		}

		return $width;
	}


}

?>