<?php 

if( !function_exists('ew_add_response_message')){
	function ew_add_response_message( $msg, $title, $type = 'message'){

		global $woocommerce;
			
		// if ( get_class( $msg ) == 'SimpleXMLElement' ) {
		// 	$dom = dom_import_simplexml( $msg )->ownerDocument;
		// 	$dom->formatOutput = true;
		// 	$raw_message = $dom->saveXML();
		// 	$message = htmlspecialchars( preg_replace( "/\n+/", "\n", $raw_message ) );
		// } elseif ( is_object( $response ) ) {
		// 	$raw_message = $message = print_r( $response, true );
		// }

		$message = print_r( $msg, true);

		$debug_message = "<pre><strong>" . $title . "</strong>\n" . $message . "</pre>";

		if ( $type == 'message' )
				$woocommerce->add_message( $debug_message );
			else 
				$woocommerce->add_error( $debug_message );

	}
}

if( !function_exists('ew_notification')){
	function ew_notification(){

		do_action('extensionworks_notification');
	}
}
add_action('admin_notices', 'ew_notification');
?>