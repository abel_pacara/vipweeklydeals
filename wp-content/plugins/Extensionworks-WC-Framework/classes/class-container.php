<?php
/**
 * EW_Container
 *
 * EW_Container represent a shipping box that contains items
 *
 * @class       EW_Container
 * @version     1.0
 * @package     ExtensionWorks-Shipper/Classes
 * @author      Extension Works
 * @distributor   www.extensionworks.com
*/

class EW_Container {

    /**
    * @var int
    */
    var $width = 0;

    /**
    * @var int
    */
    var $length = 0;

    /**
    * @var int
    */
    var $height = 0;


    /**
    * @var int
    */
    var $weight = 0;

    /**
    * @var int
    */
    var $max_weight = 0;

    /**
    * @var int
    */
    var $max_unit = 0;

    var $girth =0;

    /**
    * @var array
    */
    var $items = array();

    public function __construct ( $container = NULL ) {
        if ( $container != NULL ) {

            foreach ( $container as $key => $value ) {
                $property = str_replace('box_', '', $key);
                if( is_numeric( $value) )
                    $this->$property = (float) $value;
                else 
                    $this->$property = $value;
            }

        }

    }

    /**
     * put an product in to container
     * @param  EW_Product $item 
     * @return 
     */
    public function put ( $item ) {
        $this->items[] =clone $item;
    }


    /**
     * check products can put
     * @param  [type] $item [description]
     * @return [type]       [description]
     */
    public function can_put( $item ){

        //items
        if( is_array( $item ) ){

            return $this->_can_put_products( $item );
        }else{

            return $this->_can_put_product( $item );
        }

    }

    private function _can_put_products( $items ){
        if( $this->get_max_height( $items ) > $this->get_height() || 
            $this->get_max_width( $items ) > $this->get_width() || 
            $this->get_max_length( $items ) > $this->get_length()
        ){
            return false;
        }
            

        //check max unit
        if( $this->max_unit && $this->get_quantity() + count( $items) > $this->max_unit ){
                return false;
            }

        //check max weight
        if( $this->get_max_weight() && ( $this->get_weight() + $this->get_total_weight( $items) ) > $this->get_max_weight() ){ 
            // echo '<pre> can put array weight false </pre>';
            return false;
        }

        //check volume
        if( $this->get_total_volume( $this->items ) + $this->get_total_volume( $items ) > $this->get_volume() ){
            // echo '<pre> can put array valume false </pre>';
            return false;
        }
        return true;
    }

    private function _can_put_product( $item ){
        if( !$this->_can_fit( $item ) )
            return false;

        //check max unit
        if( $this->max_unit && $this->get_quantity() >= $this->max_unit )
            return false;

        //check max weight
        if( $this->get_max_weight() && ( $this->get_weight() + $item->get_weight() ) > $this->get_max_weight() )
            return false;

        //check volume
        if( $this->get_available_volume() < $item->get_volume() )
            return false;

        return true;

    }

    public function _can_fit( $item ){

        //check size
        if( $item->get_length() > $this->get_length() || $item->get_height() > $this->get_height() || $item->get_width() > $this->get_width())
            return false;

        if( $this->get_max_weight() && $item->get_weight() > $this->get_max_weight() )
            return false;

        return true;
    }
    private function _can_put( $item ){

        if( !$this->can_fit( $item ) )
            return false;

        //check max unit
        if( $this->max_unit && $this->get_quantity() >= $this->max_unit )
            return false;

        //check max weight
        if( $this->get_max_weight() && ( $this->get_weight() + $item->get_weight() ) > $this->get_max_weight() )
            return false;

        //check volume
        if( $this->get_available_volume() < $item->get_volume() )
            return false;

        return true;
    }
    
    private function get_max_length( $products ){

        $length = 0.0;
        foreach ($products as $product) {
            if( $product->get_length() > $length )
                $length = $product->get_length();
        }

        return $length;
    }
    private function get_max_height( $products ){

        $height = 0.0;
        foreach ($products as $product) {
            if( $product->get_height() > $height )
                $height = $product->get_height();
        }

        return $height;
    }
    private function get_max_width( $products ){

        $width = 0.0;
        foreach ($products as $product) {
            if( $product->get_width() > $width )
                $width = $product->get_width();
        }

        return $width;
    }


    

    /**
     * calculate products total weight
     * @param  [type] $products [description]
     * @return [type]           [description]
     */
    private function get_total_weight( $products ){
        $weight = 0.0;
        foreach( $products as $product ) {
            $weight += $product->get_weight();
        }

        return $weight;
    }

    /**
     * calculate products total voluem
     * @param  [type] $products [description]
     * @return [type]           [description]
     */
    private function get_total_volume( $products ){
        $volume = 0.0;
        foreach( $products as $product ) {
            $volume += $product->get_volume();
        }

        return $volume;
    }

    public function get_quantity(){
        return count( $this->items );
    }

    /**
    * function to get box's current unit count
    */
    public function get_unit_count () {
        return count( $this->items );
    }


    /**
     * get box volume
     * @return [type] [description]
     */
    public function get_volume(){
        return $this->width * $this->length * $this->height;
    }

    /**
     * get available volume in container
     * @return [type] [description]
     */
    public function get_available_volume(){
        return $this->get_volume() - $this->get_total_volume( $this->items );
    }

    /**
    * get current container weight, include items in
    */
    public function get_weight () {

        return $this->get_total_weight( $this->items );
    }

    public function get_available_weight(){

        return $this->get_max_weight() - $this->get_weight();
    }

    /**
     * get container width
     * @return float
     */
    public function get_width(){
        return $this->width;
    }

    /**
     * get container height
     * @return float
     */
    public function get_height(){
        return $this->height;
    }

    /**
     * get container length
     * @return float
     */
    public function get_length(){
        return $this->length;
    }

    /**
     * get container max weight
     * @return float 
     */
    public function get_max_weight () {
        return $this->max_weight;
    }

    /**
     * get container girth
     * @return float
     */
    public function get_girth(){

        return ( $this->girth ) ? $this->girth :  ( round( $this->height ) + round( $this->weight ) ) * 2;
    }

    /**
     * get price for this container, inclued price of items
     * @return float
     */
    public function get_price(){

        $price = 0.0;
        foreach ($this->items as $item ) {
            $price += $item->get_price();
        }
        return $price;
    }


}

?>
