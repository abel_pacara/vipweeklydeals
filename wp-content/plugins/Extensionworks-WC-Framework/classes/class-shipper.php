<?php
/**
 * EW_Shipper
 *
 * The ew_shipper class is skeleton class to be inherented by actual shipping extension.
 * It handle some basics such as settings, availabilities etc
 *
 * @class       EW_Shipper
 * @version     1.0
 * @package     ExtensionWorks-Shipper/Classes
 * @author      Extension Works
 * @distributor www.extensionworks.com
*/

class EW_Shipper extends WC_Shipping_Method {

    /**
    * @var string
    */
    var $id = '';

    /**
     * @var string
     */
    var $carrier = '';

    /**
     * @var string
     */
    var $dimension_unit = 'in';

    /**
     * @var string
     */
    var $weight_unit = 'lbs';

    /**
     * @var string
     */
    var $description = '';

    /**
     * @var string
     */
    var $endpoint = '';

    /**
     * @var string
     */
    var $dev_endpoint = '';

    /**
    * @var array
    */
    var $carrier_boxes = array();

    /**
    * @var array
    */
    var $carrier_envelopes = array();

    /**
    * @var array
    */
    var $allowed_origin_countries = array();

    /**
    * @var array
    */
    var $allowed_currencies = array();

    /**
    * @var array
    */
    var $package_shipping_methods = array();

    /**
    * @var array
    */
    var $letter_shipping_methods = array();

    /**
    * @var boolean
    */
    var $letter_mail_available = false;

    /**
    * @var array
    */
    var $settings_order = array();

    var $unpackable_products = array();

    var $robat;

    var $log = '';

    var $enable_rename_method = true;

    public function __construct(){
        global $woocommerce;

        // Load the form fields.
        $this->load_containers();
        $this->load_method_names();
        $this->init_form_fields();

        $this->add_form_fields();
        $this->sort_form_fiels();

        // Load the settings.
        $this->init_settings();

        foreach ( $this->settings as $key => $value ){
            if(array_key_exists($key, $this->form_fields)) $this->$key = $value;
        }

        $this->setup_containers();

        $this->shipping_methods = array_merge($this->package_shipping_methods, $this->letter_shipping_methods);

        foreach ( $this->shipping_methods as $key => $method ) {
            if ( array_key_exists($key, $this->renamed_methods) ) {
                $this->package_shipping_methods[$key] = $this->renamed_methods[$key];
            }
        }

        if ( isset($this->selected_package_methods) && (!is_array($this->selected_package_methods) || empty($this->selected_package_methods)) )
            $this->selected_package_methods = array_keys( $this->package_shipping_methods );

        if ( isset($this->selected_letter_methods) && (!is_array($this->selected_letter_methods) || empty($this->selected_letter_methods)) )
            $this->selected_letter_methods = array_keys( $this->letter_shipping_methods );

        $this->origin_country = $woocommerce->countries->get_base_country();
        $this->currency = get_woocommerce_currency();

        $this->custombox_form_fields();
        $this->rename_method_form_fields();
        $this->load_category();

        add_action( 'admin_notices', array(&$this, 'notification') );
        add_action( 'woocommerce_update_options_shipping_' . $this->id, array(&$this, 'process_admin_options'), 1);

    }

    /**
    * Notification upon condition checks
    */
    public function notification($issues=array()) {

            $setting_url = 'admin.php?page=woocommerce_settings&tab=shipping&section=' . $this->id;
            $woocommerce_url = 'admin.php?page=woocommerce_settings&tab=general';

            if (!$this->origin && $this->enabled == 'yes'){
                $issues[] = 'no origin postcode entered';
            }

            if ( !empty( $this->allowed_origin_countries )){
                if (!in_array($this->origin_country, $this->allowed_origin_countries)){
                    $issues[] = 'base country is not correct';
                }
            }
        
            if (!in_array($this->currency, $this->allowed_currencies)){
                $issues[] = 'currency is not accepted';
            }

            if (!empty($issues)){
                echo '<div class="error"><p>' . sprintf(__($this->carrier . ' is enabled, but %s. 
                Please update ' . $this->carrier .' settings <a href="%s">here</a> and WooCommerce settings <a href="%s">here</a>.', 'extensionworks'),
            implode(", ", $issues), admin_url($setting_url), admin_url($woocommerce_url)) . '</p></div>';
            }

            //add addtional notification
            do_action('extensionworks_notification');
    }

    /**
    * Fields for rename shipping method
    */
    public function rename_method_form_fields() {
        $this->rename_method_form_fields = array(
            'shipping_method' => array(
                'title' => __('Pick a method to rename', 'extensionworks'),
                'type' => 'select',
                'class' => 'chosen_select',
                'description' => __('Rename one of the ' . $this->carrier . ' shipping methods to suit your shop.', 'extensionworks'),
                'css' => 'width: 25em;',
                'options' => array_merge(array('0' => ''), $this->shipping_methods)
            ),
            'new_name' => array(
                'title' => __('New Name', 'ew_shipper'),
                'type' => 'text',
                'description' => __('Leave empty will change it back to its original name.', 'ew_shipper'),
                'rules' => 'string none'
            ),
        );
    }

    /**
    * Fields for custom box
    */
    public function custombox_form_fields() {

        $available_boxes = array('0' => 'Add a new container');
        foreach( $this->available_boxes as $key => $box ){
            if ( $box['box_label'] ) {
                $available_boxes[$key] = $box['box_label'] . ': ' .$box['box_width'] . ' x ' . $box['box_length'] . ' x ' . $box['box_height'] . ' in ' . strtoupper($this->dimension_unit);;
            } else {
                $available_boxes[$key] = $box['box_width'] . ' x ' . $box['box_length'] . ' x ' . $box['box_height'] . ' in ' . strtoupper($this->dimension_unit);
            }
        }
        $this->saved_boxes_field = array(
            'saved_boxes' => array(
                'title' => __('Add/Edit a container', 'extensionworks'),
                'description' => __('These containers will be used when packing your items.', 'extensionworks'),
                'type' => 'select',
                'class' => 'chosen_select',
                'css' => 'width: 25em;',
                'options' => $available_boxes
            ),
        );

        $this->box_form_fields = array(
            'box_is_envelope' => array(
                'type' => 'checkbox'
            ),
            'box_label' => array(
                'title' => __('Label', 'ew_shipper'),
                'type' => 'text',
                'description' => __('Label your box for easier management.', 'ew_shipper'),
                'rules' => 'string none'
            ),
            'box_width' => array(
                'title' => __('Width', 'ew_shipper'),
                'type' => 'text',
                'description' => __('in ' . strtoupper($this->dimension_unit), 'ew_shipper'),
                'css' => 'width: 5em;',
                'class' => 'small',
                'rules' => 'int none'
            ),
            'box_length' => array(
                'title' => __('Length', 'ew_shipper'),
                'type' => 'text',
                'description' => __('in ' . strtoupper($this->dimension_unit), 'ew_shipper'),
                'css' => 'width: 5em;',
                'class' => 'small',
                'rules' => 'int none'
            ),
            'box_height' => array(
                'title' => __('Height', 'ew_shipper'),
                'type' => 'text',
                'description' => __('in ' . strtoupper($this->dimension_unit), 'ew_shipper'),
                'css' => 'width: 5em;',
                'class' => 'small',
                'rules' => 'int none'
            ),
            'box_girth' => array(
                'title' => __('Height', 'ew_shipper'),
                'type' => 'text',
                'description' => __('in ' . strtoupper($this->dimension_unit), 'ew_shipper'),
                'css' => 'width: 5em;',
                'class' => 'small',
                'rules' => 'int none'
            ),
            'box_max_weight'  => array(
                'title' => __('Maximum weight', 'ew_shipper'),
                'type' => 'text',
                'description' => __('The maximum weight the box can hold, in ' . strtoupper($this->weight_unit), 'ew_shipper'),
                'css' => 'width: 5em;',
                'class' => 'small',
                'rules' => 'int none'
            ),
            'box_max_unit'  => array(
                'title' => __('Max units can hold', 'ew_shipper'),
                'type' => 'text',
                'description' => __('The maximum number of items can be put into the box.', 'ew_shipper'),
                'css' => 'width: 5em;',
                'class' => 'small',
                'rules' => 'int none'
            ),
            'box_remove' => array(
                'type' => 'checkbox'
            )
        );
    }

    /**
     * Initialise Gateway Settings Form Fields
     */
    public function init_form_fields() {
        global $woocommerce;

        $usable_boxes = array();
        $usable_envelopes = array();

        if ( is_array($this->usable_boxes) ){
            foreach ( $this->usable_boxes as $key => $box) {
                $usable_boxes[$key] = $box['box_label'];
            }
        }

        if ( is_array($this->usable_envelopes) ){
            foreach ( $this->usable_envelopes as $key => $envelope) {
                $label =  (($envelope['box_label']) ? $envelope['box_label'] . ": " : "") . $envelope['box_width'] . " x " . $envelope['box_length'] . " x " . $envelope['box_height'] ." in " . strtoupper($this->dimension_unit);
                $usable_envelopes[$key] =$label;
            }
        }

        $this->copy_shipping_methods = array_merge($this->package_shipping_methods, $this->letter_shipping_methods);
        // Rename shipping methods
        foreach ( $this->package_shipping_methods as $key => $method ) {
            if ( array_key_exists($key, $this->renamed_methods) ) {
                $this->package_shipping_methods[$key] = $this->renamed_methods[$key];
            }
        }

        foreach ( $this->letter_shipping_methods as $key => $method ) {
            if ( array_key_exists($key, $this->renamed_methods) ) {
                $this->letter_shipping_methods[$key] = $this->renamed_methods[$key];
            }
        }

        $this->form_fields = array(

            'enabled' => array(
                'title' => __('Enable/Disable', 'ew_shipper'),
                'type' => 'checkbox',
                'label' => __('Enable ' . $this->carrier, 'ew_shipper'),
                'default' => 'yes'
            ),
            'title' => array(
                'title' => __('Method Title', 'ew_shipper'),
                'type' => 'text',
                'description' => __('This controls the title which the user sees during checkout.', 'ew_shipper'),
                'default' => __($this->carrier, 'ew_shipper')
            ),
            'origin' => array(
                'title' => __('Origin Postcode', 'extensionworks'),
                'type' => 'text',
                'description' => __('Enter your origin post code.', 'extensionworks'),
                'default' => __('', 'extensionworks')
            ),
            'fee' => array(
                'title' => __('Handling Fee', 'extensionworks'),
                'type' => 'text',
                'description' => __('Fee excluding tax. Enter an amount, e.g. 2.50, or a percentage, e.g. 5%.', 'extensionworks'),
                'default' => '0'
            ),
            'fee_to_cart' => array(
                'title' => __('', 'extensionworks'),
                'label' => __('Apply handling fee to the value of cart.', 'extensionworks'),
                'type' => 'checkbox',
                'description' => __('Instead of applying handling fee to shipping rate, apply it to the value of cart.', 'extensionworks'),
                'default' => 'no'
            ),
            // 'class_ship_only' => array(
            //     'title' => __('Use shipping class', 'extensionworks'),
            //     'label' => __('Ship only ' . $this->carrier . ' shipping class enabled products', 'extensionworks'),
            //     'type' => 'checkbox',
            //     'description' => __('By checking this option, only products with ' . $this->carrier . ' selected as <a target=blank href="http://wcdocs.woothemes.com/user-guide/product-shipping-classes/">Shipping Class</a> will be calculated.', 'extensionworks'),
            //     'default' => 'no'
            // ),
            'ship_type' => array(
                'title' => __('Your products will be shipped', 'extensionworks'),
                'type' => 'select',
                'default' => 'all',
                'css' => 'width: 15em;',
                'class' => 'chosen_select',
                'options' => array(
                    'together' => __('Together', 'extensionworks'),
                    'separate' => __('Separately', 'extensionworks')
                )
            ),
            'rate_type' => array(
                'title' => 'Select shipping rate to display',
                'type' => 'select',
                'default' => '0',
                'css' => 'width: 15em;',
                'class' => 'chosen_select',
                'options' => array(
                    '0' => 'All',
                    '1' => 'Cheapest rate',
                    '2' => 'Highest rate'
                )
            ),
            'selected_boxes' => array(
                'title' => __('Boxes for parcel mail', 'extensionworks'),
                'type' => 'multiselect',
                'class' => 'chosen_select',
                'css' => 'width: 25em;',
                'description' => 'Select boxes you want use when packing your products.',
                'default' => array(),
                'options' => $usable_boxes
            ),
            'selected_envelopes' => array(
                'title' => __('Envelopes for letter mail', 'extensionworks'),
                'type' => 'multiselect',
                'class' => 'chosen_select',
                'css' => 'width: 25em;',
                'description' => 'Select evelopes you want use when putting your products.',
                'default' => array(),
                'options' => $usable_envelopes
            ),
            'selected_package_methods' => array(
                'title' => __('Shipping Methods For Packages', 'extensionworks'),
                'type' => 'multiselect',
                'class' => 'chosen_select',
                'css' => 'width: 25em;',
                'description' => 'Leave empty to enable all shipping methods',
                'default' => '',
                'options' => $this->package_shipping_methods
            ),
            'selected_letter_methods' => array(
                'title' => __('Shipping Methods For Letters', 'extensionworks'),
                'type' => 'multiselect',
                'class' => 'chosen_select',
                'css' => 'width: 25em;',
                'description' => 'Leave empty to enable all shipping methods',
                'default' => '',
                'options' => $this->letter_shipping_methods
            ),
            'fallback' => array(
                'title'       => __( 'Fallback', 'extensionworks' ),
                'type'        => 'text',
                'description' => __( 'If shipping method returns no matching rates, offer this amount for shipping so that the user can still checkout. Leave blank to disable.', 'extensionworks' ),
                'default'     => ''
            ),
            'fallback_name' => array(
                'title'       => __( 'Fallback name', 'extensionworks' ),
                'type'        => 'text',
                'description' => __( 'this method name will display to customer for fallback.', 'extensionworks' ),
                'default'     => 'Fallback'
            ),
            'availability' => array(
                'title' => __('Method availability', 'extensionworks'),
                'type' => 'select',
                'default' => 'all',
                'class' => 'availability',
                'options' => array(
                    'all' => __('All allowed countries', 'extensionworks'),
                    'specific' => __('Specific Countries', 'extensionworks')
                )
            ),
            'countries' => array(
                'title' => __('Specific Target Countries', 'extensionworks'),
                'type' => 'multiselect',
                'class' => 'chosen_select',
                'css' => 'width: 25em;',
                'default' => '',
                'options' => $woocommerce->countries->countries
            ),
            'debug' => array(
                'title' => __('Debug mode', 'extensionworks'),
                'type' => 'checkbox',
                'label' => __('Enable Debug Mode', 'extensionworks'),
                'description' => __('This will output some debug information on your cart page, remember to turn this off when you done testing.', 'extensionworks'),
                'default' => 'no'
            ),
            'available_boxes' => array(
                'type' => 'hidden',
                'default' => array()
            ),
            'renamed_methods' => array(
                'type' => 'hidden',
                'default' => array()
            ),

        );

        if ( empty($this->letter_shipping_methods) ) {
            unset($this->form_fields['selected_letter_methods']);
        }

    }

    /**
    * Add additional form fields
    */
    public function add_form_fields(){}

    /**
    * Sort admin fields for displaying in order
    */
    public function sort_form_fiels(){

        $fields = array();

        // Merge fields order
        if (empty($this->settings_order)){
            $this->settings_order = array_keys($this->form_fields);
        } else {
            $this->settings_order = array_merge($this->settings_order, array_keys($this->form_fields));
        }

        // Sorting
        foreach( $this->settings_order as $order ){

            if(isset($this->form_fields[$order])){
                $fields[$order] = $this->form_fields[$order];
            }

        }
        
        $this->form_fields = $fields;

    }

    /**
     * Shipping method available conditions
     */
    public function is_available( $package ) {
        
        if ( $this->ship_type == 'together' && (!is_array($this->selected_boxes) || empty($this->selected_boxes)) )
            return false;

        if ( !in_array($this->currency, $this->allowed_currencies) )
            return false;

       if ( !empty( $this->allowed_origin_countries ) )
            if ( !in_array($this->origin_country, $this->allowed_origin_countries) )
                return false;

        if ( empty($this->letter_shipping_methods) && empty($this->package_shipping_methods) )
            return false;

        return parent::is_available( $package );

    }

    /**
    * Check if this shippment is international shipping
    */
    public function is_intel_shipping() {
        
        $country = $this->get_shipping_country();
        return !in_array( $country, $this->allowed_origin_countries );
    }

   

    /**
     * prepare products, find shippable products
     * @return [type] [description]
     */
    public function prepare_products(){
        global $woocommerce;
        $products = array();
        $cart_products = array();

        //disable shiping class
        $this->class_ship_only = 'no';
        
        foreach ( $woocommerce->cart->get_cart() as $product ) {
            $product = new EW_Product( $product );
            $cart_products[] = $product;
        }

        if ( $this->class_ship_only == 'yes' ) {

            foreach ( $cart_products as $product ) {
                $item = $product->data;

                //for woo 1.6
                if( isset( $this->category ) && $item->get_shipping_class() == $this->category->slug ){
                    $products[] = $product;
                }else if($item->get_shipping_class() == $this->method_title){//for woo 2.0
                    $products[] = $product;
                }
            }
            return $products;

        }

        return $cart_products;
    }

    
    public function prepare_packages( $products ){

        $shippable_products = $this->prepare_products();
        $this->letter_mail_available = $this->letter_mail_available( $shippable_products );
        if ( empty($shippable_products) )
                return $shippable_products;

        $containers = $this->get_containers();
        $packing_robat = new EW_Packing_Robat( $shippable_products, $containers, $this->dimension_unit, $this->weight_unit );
        $packing_robat->set_packing_method( new EW_Volume_Packing() );
        $packing_robat->set_packing_type( $this->ship_type );

        return  $this->robat = $packing_robat;
    }

    public function packing( $packages ){

        $this->prepare_packages( $products )
            ->packing();

    }

    public function calculate_shipping( $packages ){

        //it's robat
        $robat = $this->prepare_packages( $packages );
        if ( is_object( $robat ) ){
            $this->packages = $robat->packing();
        }else{
            $this->add_debug_message('No shippable Items', 'FW packable packages');
            return false;
        }
        
        $this->add_debug_message( $this->packages, 'FW packable packages');
        $this->unpackable_products = $this->robat->get_unpackable_products();
        $this->add_debug_message( $this->unpackable_products, 'FW unpackable products');
        //handle unpackable products
        if( count( $this->unpackable_products ) > 0)
            $this->packages = array_merge($this->packages, $this->handle_unpackable_products( $this->unpackable_products ) );

        $this->add_debug_message( $this->packages, 'FW  send packages');
        $available_methods = $this->get_available_methods();
        $all_shipping_methods = $this->get_all_shipping_methods();
        $rates = $this->get_package_rates( $this->packages, $available_methods );
        $this->add_debug_message( $rates, 'FW return rates');

        //remove unavailabe methods
        foreach ($rates as $key => $rate) {
            if( !in_array( $rate['id'], $available_methods ) ){
                $this->add_debug_message( $rates[$key], 'FW remove unavailable methods');
                unset($rates[$key]);
            }
                
        }
        if( is_array( $rates ) && count( $rates ) > 0  ){

            usort( $rates, array( 'EW_Shipper', 'compare_rate' ) );
             if ( '1' == $this->rate_type ){
                $rates = array( $this->get_cheapest_rate( $rates ) );
            }

            if ( '2' == $this->rate_type ){
                $rates = array( $this->get_highest_rate( $rates ) );
            }


            foreach ($rates as $key => $rate ) {
                
                if( in_array( $rate['id'], $available_methods ) ){

                    if ( $this->fee_to_cart == 'yes' ) {
                        $rate['cost'] = $rate ['cost'] + $this->get_fee( $this->fee, $this->get_products_total_value() );
                    } else {
                        $rate['cost'] = $rate ['cost'] + $this->get_fee( $this->fee, $rate['cost'] );
                    }

                    $rate['label'] = $all_shipping_methods[ $rate['id'] ];
                    $this->add_to_rate( $rate );
                }
            }
        }else if( $this->fallback ){
            $rate = array(
                'id' => 'fallback',
                'cost' => $this->fallback,
                'label' => $this->fallback_name,
            );

            $this->add_to_rate( $rate);
        }
    }

    public function get_package_rates( $packages, $methods ){

        $rates = array();

        return $rates;
    }

    function get_cheapest_rate( $rates ){
        usort( $rates, array( 'EW_Shipper', 'compare_rate' ) );
        return array_shift( $rates );
    }

    function get_highest_rate( $rates ){
        usort( $rates, array( 'EW_Shipper', 'compare_rate' ) );
        return array_pop( $rates );
    }

    function sort_rates( $rates ){
        usort( $rates, array( 'EW_Shipper', 'compare_rate' ) );
    }

    static public function compare_rate($a, $b){
        return ( $a['cost'] > $b['cost'] ) ? 1 : -1;
    }


    public function get_products_total_value(){
        global $woocommerce;
        return $woocommerce->cart->cart_contents_total;
    }

    public function add_to_rate( $rate ){

        $this->add_rate( $rate );
    }

    public function get_available_methods(){
        $methods = array();
        if( is_array($this->selected_package_methods) )
            $methods = array_merge( $methods,$this->selected_package_methods );
        if( isset( $this->selected_letter_methods ) && is_array( $this->selected_letter_methods ))
            $methods = array_merge( $methods, $this->selected_letter_methods );
        return $methods;
    }

    public function get_all_shipping_methods(){
        return $this->package_shipping_methods + $this->letter_shipping_methods;
        // return array_merge( $this->package_shipping_methods, $this->letter_shipping_methods );
    }

    public function handle_unpackable_products( $products ){

        $packages = array();
        foreach ($products as $product ) {
            $container           = new EW_Container();  
            $container->width    = $product->get_width();
            $container->length   = $product->get_length();
            $container->height   = $product->get_height();
            $container->weight   = $product->get_weight();
            $container->items[] = $product;
            $packages[] = $container;
        }

        return $packages; 
    }

    public function get_containers(){
        if ( $this->letter_mail_available )
            return $this->selected_envelopes;
        else 
            return $this->selected_boxes;
    }

    /**
    * Check if letter mail is available for this cart
    */
    public function letter_mail_available( $products ) {
        foreach ( $products as $product ) {
            if ( $product->letter_mail == 'no' )
                return false;
        }

        return true;
    }

    /**
    * Get a box from it's id
    */
    public function get_box( $id ) {
        return $this->available_boxes[$id];
    }

    /**
    * Validate a box
    */
    public function valid_box( $box ) {
        return $box['box_width'] && $box['box_length'] && $box['box_height'];
    }

    /**
    * Setup containers to be ready to use by packing
    */
    public function setup_containers() {

        // convert array containers to container objects
        $envelopes = array();
        $boxes = array();

        if ( empty($this->selected_envelopes) )
            $this->selected_envelopes = array_keys($this->usable_envelopes);

        if ( empty($this->selected_boxes) )
            $this->selected_boxes = array_keys($this->usable_boxes);

        if (is_array($this->selected_boxes)) {

            foreach ( $this->selected_boxes as $box ) {

                if ( isset($this->usable_boxes[$box]) ) {
                    $boxes[] = new EW_Container($this->usable_boxes[$box]);
                }

            }
            $this->selected_boxes = $boxes;
        }
        if (is_array($this->selected_envelopes)) {

            foreach ( $this->selected_envelopes as $envelope ) {

                if ( isset($this->usable_envelopes[$envelope]) ) {
                    $envelopes[] = new EW_Container($this->usable_envelopes[$envelope]);
                }

            }
            $this->selected_envelopes = $envelopes;
        }
    }

    /**
    * Load containers
    */
    public function load_containers() {
        $available_boxes = array();
        $available_envelopes = array();

        $form_field_settings = ( array ) get_option( $this->plugin_id . $this->id . '_settings' );
        if ( isset($form_field_settings['available_boxes']) && !empty($form_field_settings['available_boxes']) ) {

            $available_boxes = $form_field_settings['available_boxes'];

            foreach ( $available_boxes as $key => $box ) {
                if ( $box['box_label'] == '' ) $available_boxes[$key]['box_label'] = $box['box_width'] . ' x ' . $box['box_length'] . ' x ' . $box['box_height'] . ' in ' . strtoupper($this->dimension_unit);

                if ( $box['box_is_envelope'] == 'yes' ) {
                    $available_envelopes[$key] = $box;
                    unset($available_boxes[$key]);
                }
            }

        }

        // We use + because we want keep the index.
        $this->usable_boxes = $this->carrier_boxes + $available_boxes;
        $this->usable_envelopes = $this->carrier_envelopes + $available_envelopes;
    }

    public function load_method_names() {

        $method_names = array();

        if( $this->enable_rename_method ){

            $form_field_settings = ( array ) get_option( $this->plugin_id . $this->id . '_settings' );
            if ( isset($form_field_settings['renamed_methods']) && !empty($form_field_settings['renamed_methods']) ) {
                $method_names = $form_field_settings['renamed_methods'];
            }
        }

        $this->renamed_methods = $method_names;

    }

    /**
     * Validate Settings Field Data.
     *
     * Validate the data on the "Settings" form.
     *
     * @since 1.0.0
     * @uses method_exists()
     * @param bool $form_fields (default: false)
     * @return void
     */
    public function validate_settings_fields( $form_fields = false, &$other_sanitized_fields = NULL ) {

        if ( ! $form_fields )
            $form_fields = $this->form_fields;

        $sanitized_fields = array();

        foreach ( $form_fields as $k => $v ) {
            if ( ! isset( $v['type'] ) || ( $v['type'] == '' ) ) { $v['type'] = 'text'; } // Default to "text" field type.

            if ( ! isset( $v['rules'] ) || ( $v['rules'] == '' )  ) { $v['rules'] = 'none'; }

            $rules = explode(' ', $v['rules']);

            if ( method_exists( $this, 'validate_' . $v['type'] . '_field' ) ) {
                $field = $this->{'validate_' . $v['type'] . '_field'}( $k );

                if ( $field == '' &&  !in_array('none', $rules))
                    $this->errors['field_error_' . $k] = 'Value of ' . $v['title'] . ' can not be empty.';

                if ( in_array('int', $rules) && $field && !is_numeric($field))
                    $this->errors['field_error_' . $k] = 'Value of ' . $v['title'] . ' must be an integer number.';

                if ( in_array('float', $rules) && $field && !is_numeric($field))
                    $this->errors['field_error_' . $k] = 'Value of ' . $v['title'] . ' must be a float number.';

                if ( in_array('numeric', $rules) && $field && !is_numeric($field))
                    $this->errors['field_error_' . $k] = 'Value of ' . $v['title'] . ' must be a number.';

                if ( in_array('string', $rules) && $field && !is_string($field))
                    $this->errors['field_error_' . $k] = 'Value of ' . $v['title'] . ' must be a string.';

                $sanitized_fields[$k] = $field;
            } else {
                $sanitized_fields[$k] = $this->settings[$k];
            }
        }

        if ( is_array($other_sanitized_fields) ) {
            $other_sanitized_fields = $sanitized_fields;
        } else {
            $this->sanitized_fields = $sanitized_fields;
        }
    }

    /**
     * Admin Panel Options Processing
     * - Saves the options to the DB
     *
     * @since 1.0.0
     * @access public
     * @return bool
     */
    public function process_admin_options() {
        // Validate custom boxes and add to sanitized_fields
        $custom_box_fields = array();
        $selected_box = array();
        $renamed_method = array();

        $this->validate_settings_fields($this->box_form_fields, $custom_box_fields);
        $this->validate_settings_fields($this->saved_boxes_field, $selected_box);
        $this->validate_settings_fields($this->rename_method_form_fields, $renamed_method);

        // Validate normal setting fields
        $this->validate_settings_fields();

        if ( count( $this->errors ) > 0 ) {
            // $this->display_errors();
            foreach ($this->errors as $error)
                $this->add_log( $error, false );
            return false;
        } else {

            // Manipulate fields before save
            // Remove box settings to have it not saved as regular settings
            $remove_box_fields = array_keys($this->box_form_fields);
            $remove_naming_fields = array_keys($this->rename_method_form_fields);
            $remove_fields = array_merge($remove_box_fields, $remove_naming_fields);
        
            foreach ( $this->sanitized_fields as $field => $value ) {
                if ( in_array($field, $remove_fields) ) {
                    unset( $this->sanitized_fields[$field] );
                }
            }
           
            // Save renamed method
            $this->sanitized_fields['renamed_methods'] = $this->renamed_methods;
            if ($renamed_method['shipping_method'] != '') {
                $method = $renamed_method['shipping_method'];
                $new_name = $renamed_method['new_name'];
                if ( $new_name != '' )
                    $this->sanitized_fields['renamed_methods'][$method] = $new_name;
                else
                    unset($this->sanitized_fields['renamed_methods'][$method]);
            }

            // Save boxes settings
            $this->sanitized_fields['available_boxes'] = $this->available_boxes;
            $target_box = $selected_box['saved_boxes'];

            if ( $custom_box_fields['box_remove'] == 'yes' && $target_box != 0 ) {

                // Remove the box
                unset( $this->sanitized_fields['available_boxes'][$target_box] );

            } elseif ( $custom_box_fields['box_remove'] == 'no' && $this->valid_box($custom_box_fields) ) {

                // Add or update the box
                if ( $target_box == 0 )
                    $target_box = count($this->available_boxes) + 1;
                $this->sanitized_fields['available_boxes'][$target_box] = $custom_box_fields;

            } else {
                // Do nothing
            }

            //reset shipping method title
            if ( isset( $_POST['reset'])){
                $this->sanitized_fields['renamed_methods'] = array();
            }

            update_option( $this->plugin_id . $this->id . '_settings', $this->sanitized_fields );

            return true;
        }
    }

    /**
     * Admin Panel Options
     */
    public function admin_options() {
        global $woocommerce;

        $methods = $woocommerce->shipping->load_shipping_methods();

        $params= array();
        foreach ( $methods as $method ) {
            if ( $method->id == $this->id ) {
                $params['available_boxes'] = json_encode($method->available_boxes);
                $params['renamed_methods'] = json_encode($method->renamed_methods);
                break;
            }
        }
        ?>
        <style>
            .left {float: left;}
            .right { float: left;}
            table.form-table { clear: none; }
            table.wide { width: 650px; border-right: thin solid #DFDFDF; margin-right: 15px !important; }
            table.narrow { width: 500px; }
        </style>

        <h3><?php _e($this->carrier, 'extensionworks'); ?></h3>
        <p><?php _e($this->description, 'extensionworks'); ?></p>

        <div class="left">
            <table class="form-table wide">

                <?php
                    // Generate the HTML For the settings form.
                    $this->generate_settings_html();
                ?>

            </table><!--/.form-table-->
        </div>

        <div class="right">
            <h4><?php _e('Container Settings', 'extensionworks'); ?></h4>
            <table class="form-table narrow">

                <?php
                    // Generate the HTML For the available box dropdown.
                    $this->generate_settings_html( $this->saved_boxes_field );
                ?>

                <tbody>
                    <tr valign="top">
                        <th class="titledesc" scope="row"><label for="woocommerce_<?php echo $this->id; ?>_box_is_envelope">This is an envelop</label></th>
                        <td class="forminp">
                        <fieldset>
                            <legend class="screen-reader-text"><span>This is an envelop</span></legend>
                            <input type="checkbox" class="" value="1" id="woocommerce_<?php echo $this->id; ?>_box_is_envelope" name="woocommerce_<?php echo $this->id; ?>_box_is_envelope"><span> Add this container to available envelopes list.</span>
                        </fieldset>
                        </td>
                    </tr>
                    <tr valign="top">
                        <th class="titledesc" scope="row"><label for="woocommerce_<?php echo $this->id; ?>_box_label">Label</label></th>
                        <td class="forminp">
                        <fieldset>
                            <legend class="screen-reader-text"><span>Label</span></legend>
                            <input type="text" placeholder="" id="woocommerce_<?php echo $this->id; ?>_box_label" name="woocommerce_<?php echo $this->id; ?>_box_label" class="input-text regular-input "> <p class="description">Label your box for easier management.</p>
                        </fieldset>
                        </td>
                    </tr>
                    <tr valign="top">
                        <th class="titledesc" scope="row"><label for="woocommerce_<?php echo $this->id; ?>_box_width">Dimensions</label></th>
                        <td class="forminp">
                            <fieldset>
                                <input type="text" placeholder="Width" style="width: 5em;" id="woocommerce_<?php echo $this->id; ?>_box_width" name="woocommerce_<?php echo $this->id; ?>_box_width" class="input-text regular-input small">
                                <input type="text" placeholder="Length" style="width: 5em;" id="woocommerce_<?php echo $this->id; ?>_box_length" name="woocommerce_<?php echo $this->id; ?>_box_length" class="input-text regular-input small">
                                <input type="text" placeholder="Height" style="width: 5em;" id="woocommerce_<?php echo $this->id; ?>_box_height" name="woocommerce_<?php echo $this->id; ?>_box_height" class="input-text regular-input small">
                                <input type="text" placeholder="Girth" style="width: 5em;" id="woocommerce_<?php echo $this->id; ?>_box_girth" name="woocommerce_<?php echo $this->id; ?>_box_girth" class="input-text regular-input small">
                                <span class="description">in <?php echo strtoupper($this->dimension_unit); ?>.</span>
                            </fieldset>
                        </td>
                    </tr>
                    <tr valign="top">
                        <th class="titledesc" scope="row"><label for="woocommerce_<?php echo $this->id; ?>_box_max_weight">Maximum weight</label></th>
                        <td class="forminp">
                            <fieldset>
                                <legend class="screen-reader-text"><span>Maximum weight</span></legend>
                                <input type="text" placeholder="" style="width: 5em;" id="woocommerce_<?php echo $this->id; ?>_box_max_weight" name="woocommerce_<?php echo $this->id; ?>_box_max_weight" class="input-text regular-input small"> <p class="description">The maximum weight the box can hold, in <?php echo strtoupper($this->weight_unit); ?></p>
                            </fieldset>
                        </td>
                    </tr>
                    <tr valign="top">
                        <th class="titledesc" scope="row"><label for="woocommerce_<?php echo $this->id; ?>_box_max_unit">Maximum units</label></th>
                        <td class="forminp">
                            <fieldset>
                                <legend class="screen-reader-text"><span>Maximum units</span></legend>
                                <input type="text" placeholder="" style="width: 5em;" id="woocommerce_<?php echo $this->id; ?>_box_max_unit" name="woocommerce_<?php echo $this->id; ?>_box_max_unit" class="input-text regular-input small"> <p class="description">The maxiumum number of items can be put into the box.</p>
                            </fieldset>
                        </td>
                    </tr>

                    <tr valign="top">
                        <th class="titledesc" scope="row"></th>
                        <td class="forminp">
                            <fieldset>
                                <input type="checkbox" class="" value="1" id="woocommerce_<?php echo $this->id; ?>_box_remove" name="woocommerce_<?php echo $this->id; ?>_box_remove">

                                <span class="description">Remove this box</span>

                                <input type="submit" value="Save" class="button-secondary" name="save">
                            </fieldset>
                        </td>
                    </tr>

                </tbody>

            </table><!--/.form-table-->
            
        <?php if ( $this->enable_rename_method ): ?>
            <h4><?php _e('Rename shipping methods', 'extensionworks'); ?></h4>
            <table class="form-table narrow">
                <?php
                    // Generate the HTML For the available shipping methods dropdown.
                    $this->generate_settings_html( $this->rename_method_form_fields );
                ?>
            <?php if( count( $this->renamed_methods ) > 0 ): ?>
                <tbody>
                    <tr>
                        <td colspan="2">
                            <table class="widefat" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>Original Shipping Title</th>
                                        <th>New Shipping Title</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($this->renamed_methods as $key => $new): ?>
                                    <tr>
                                        <td> <?php echo $this->copy_shipping_methods[$key]; ?> </td>
                                        <td> <?php echo $new; ?> </td>
                                    </tr>           
                                <?php endforeach ?>
                                <tr>
                                    <td colspan="2" class="forminp">
                                        <fieldset>
                                            <input type="submit" value="reset" class="button-secondary" name="reset">
                                            <span class="description">reset all method titles to original shipping titles</span>
                                        </fieldset>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            <?php endif; ?>
                <tbody>
                    <tr valign="top">
                        <th class="titledesc" scope="row"></th>
                        <td class="forminp">
                            <fieldset>
                                <input type="submit" value="Save" class="button-secondary" name="save">
                            </fieldset>
                        </td>
                    </tr>
                </tbody>
            </table>
        <?php endif ?>
        </div>
        <div class="clear"></div>


        <?php $js_data = apply_filters('extensionworks_shipping_assets', $this->id); ?>
        <script type="text/javascript">
            jQuery(window).load(function(){
                var $method_id = '<?php echo $this->id; ?>';
                <?php 
                    foreach ( $params as $param => $data )
                        echo 'var $' . $param . ' = ' . $data . ';';
                ?>

                jQuery('select#woocommerce_' + $method_id + '_saved_boxes').change(function(){
                    box = $available_boxes[jQuery(this).val()];
                    for (var key in box) {
                        if (box.hasOwnProperty(key) && key != 'box_remove' && key != 'box_is_envelope') {

                            jQuery('#woocommerce_' + $method_id + '_' + key).val(box[key]);

                        }
                        if (box.hasOwnProperty(key) && key == 'box_is_envelope') {
                            checked = box[key] == 'yes';
                            jQuery('#woocommerce_' + $method_id + '_' + key).attr('checked', checked);
                        }
                    }
                });

                jQuery('select#woocommerce_' + $method_id + '_shipping_method').change(function(){
                    method = $renamed_methods[jQuery(this).val()];
                    jQuery('#woocommerce_' + $method_id + '_new_name').val(method);
                });

            });
        </script>
        <?php
    }

    /************** Help functions *****************/

    /**
     * Use WooCommerce logger if debug is enabled.
     */
    public function add_log( $message='test', $debug_on = true ) {
        global $woocommerce;

        if ( $this->debug == 'yes' || !$debug_on ) {
            if ( !$this->log ) $this->log = $woocommerce->logger();

            $this->log->add( $file=$this->id, $message );
        }
    }

    public function get_fee( $value ){

            if ( strstr( $this->fee, '%' ) )
                return $fee = number_format( ( $value / 100 ) * str_replace( '%', '', $this->fee ), 2 );
            elseif( !$this->fee)
                return 0;
            else
                return number_format( $this->fee, 2);

    }
    public function add_debug_message( $msg, $title){
        if( $this->debug != 'yes')
            return;

        ew_add_response_message( $msg, $title);
    }
    public function get_shipping_postcode(){
        global $woocommerce;
        return $woocommerce->customer->get_shipping_postcode();
    }
    public function get_shipping_country(){
        global $woocommerce;
        return $woocommerce->customer->get_shipping_country();
    }

    /**
    * Show response returns when debug is on
    */
    public function show_response( $response ){
        global $woocommerce;
        if ( $this->debug == 'yes' ) {
                $woocommerce->clear_messages();
                $woocommerce->add_message('<p>'. $this->carrier .' Response:</p><ul><li>' . implode('</li><li>', $response) . '</li></ul>');
        }
    }

    public function load_category() {
        if ( $term = term_exists( $this->carrier, 'product_shipping_class' ) ) {
            $term_id = current( $term );
            return $this->category = get_term( $term_id, 'product_shipping_class' );
        }

        return false;
    }

    /**
    * install carrier class if one doesn't exist
    * This function is triggered by woocommerce_register_post_type hook
    */
    public function install_category() {
        if ( !term_exists( $this->carrier, 'product_shipping_class' ) ) {
            wp_insert_term(
                $this->carrier, // the term 
                'product_shipping_class', // the taxonomy
                array(
                    'description'=> $this->description,
                    'slug' => trim($this->carrier)
                )
            );
            return true;
        }
        return false;
    }

    /**
     * Generate Select HTML.
     *
     * @access public
     * @param mixed $key
     * @param mixed $data
     * @since 1.0.0
     * @return string
     */
    public function generate_select_html ( $key, $data ) {
        $html = '';

        if ( isset( $data['title'] ) && $data['title'] != '' ) $title = $data['title']; else $title = '';
        $data['options'] = (isset( $data['options'] )) ? (array) $data['options'] : array();
        $data['class'] = (isset( $data['class'] )) ? $data['class'] : '';
        $data['css'] = (isset( $data['css'] )) ? $data['css'] : '';

        $html .= '<tr valign="top">' . "\n";
            $html .= '<th scope="row" class="titledesc">';
            $html .= '<label for="' . $this->plugin_id . $this->id . '_' . $key . '">' . $title . '</label>';
            $html .= '</th>' . "\n";
            $html .= '<td class="forminp">' . "\n";
                $html .= '<fieldset><legend class="screen-reader-text"><span>' . $title . '</span></legend>' . "\n";
                $html .= '<select name="' . $this->plugin_id . $this->id . '_' . $key . '" id="' . $this->plugin_id . $this->id . '_' . $key . '" style="'.$data['css'].'" class="select '.$data['class'].'">';

                foreach ($data['options'] as $option_key => $option_value) :
                    $html .= '<option value="'.$option_key.'" '.selected($option_key, esc_attr( isset($this->settings[$key]) ? $this->settings[$key] : '' ), false).'>'.$option_value.'</option>';
                endforeach;

                $html .= '</select>';
                if ( isset( $data['description'] ) && $data['description'] != '' ) { $html .= ' <p class="description">' . $data['description'] . '</p>' . "\n"; }
            $html .= '</fieldset>';
            $html .= '</td>' . "\n";
        $html .= '</tr>' . "\n";

        return $html;
    }

}


?>
