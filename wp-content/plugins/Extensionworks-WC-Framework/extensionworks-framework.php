<?php
/*
  Plugin Name: Extension Works Framework
  Plugin URI: http://www.extensionworks.com
  Description: Extensionworks shipper framework for woo commerce.
  Version: 1.3.9
  Author: Extension Works
  Author URI: http://www.extensionworks.com

  Copyright: © Extension Works. 
  
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

add_action( 'plugins_loaded', 'extensionworks_framework_ini', 0 );
// add_action( 'woocommerce_loaded', 'extensionworks_framework_ini' );

function extensionworks_framework_ini(){
    
    if ( ! class_exists('ExtensionWork_Framework') ) { // Exit if framework alreay in use.
        /**
         * Required functions
         */
        include_once 'extensionworks-includes/extensionworks-functions.php';

        if ( !(is_woo_active()) )
            return;
        if( is_shipper_active() || is_updater_active() )
            return;

        /**
         * Plugin updates
         */
        class ExtensionWork_Framework {

            /**
             * @var string
             */
            var $version = '1.0';

            var $shipper;
            var $payer;
            var $updater;

            /**
             * HipperShipper Constructor.
             *
             * @access public
             * @return void
             */
            function __construct() {

                // Define version constant
                define( 'HIPPERSHIPPER_VERSION', $this->version );

                // Include required files
                $this->init_payer();
               $this->init_shipper();
               $this->init_updater();

                // $this->shipper = new EW_Shipper();

            }

            function init_payer(){
                 include( 'classes/class-payer.php' );
            }

            function init_updater(){
                include( 'classes/class-extensionworks-updater.php');
                $GLOBALS['ew_updater'] = new EW_Updater( __FILE__ );
            }

            function init_shipper(){


                include( 'classes/class-packing-robat.php' );
                include( 'classes/short-codes.php' );
                include( 'classes/class-packing-algorithem.php' );
                include( 'classes/class-packing-volume.php' );
                include( 'classes/class-product.php' );     //contains shipping class skeleton
                // include( 'classes/class-product-variation.php' );   //contains container class
                include( 'classes/class-shipper.php' );     //contains shipping class skeleton
                include( 'classes/class-container.php' );   //contains container class
        
                include( 'classes/class-xmlparser.php' );   //contains xmlparser class
                include( 'classes/product-meta.php' );              //contains extra product meta processors


                 // add_filter('woocommerce_product_class', 'extensionworks_product_factory', 10);
                add_action( 'woocommerce_product_options_dimensions', 'woocommerce_product_girth', 10 );
                add_action( 'woocommerce_product_options_dimensions', 'woocommerce_product_lettermail', 10 );

                add_action( 'woocommerce_process_product_meta', 'woocommerce_process_product_girth_metabox', 1 );
                add_action( 'woocommerce_process_product_meta', 'woocommerce_process_product_lettermail_metabox', 1 );



            }

        }

       
        // $ew_updater = new EW_Updater( __FILE__ );
        // $ew_updater->version = '1.0.2';
        
        $GLOBALS['hippershipper'] = new ExtensionWork_Framework();
        
        $GLOBALS['hipperxmlparser'] = new HipperXMLParser();

    }

}

?>
