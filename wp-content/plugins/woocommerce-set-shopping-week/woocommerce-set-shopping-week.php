<?php
/*
  Plugin Name: WooCommerce Set Shopping Week
  Description: Set Shopping Week
  Version: 1.0
  Author: www.onebolivia.com
 */

add_filter('woocommerce_settings_tabs_array', 'my_woocommerce_settings_tabs_array');
add_action('woocommerce_update_options','update_shopping_week');

add_filter( 'post_row_actions', 'show_menu_order_column', 10, 2);
add_filter( 'views_edit-product', 'woocommerce_desc_sorting_link', 10);

function my_woocommerce_settings_tabs_array($tabs)
{
   $my_tab_name = "set_shopping_week";   
   $tabs[$my_tab_name]= __('Set Shopping Week', 'woocommerce');      
   add_action('woocommerce_settings_tabs_'.$my_tab_name, 'set_shopping_week');   
   return $tabs;   
}

function set_shopping_week()
{  
   global $woocommerce_settings;
   $my_tab_name = "set_shopping_week";   
   
   $woocommerce_settings[$my_tab_name]= array(
      array(
         'name' => __( 'Shopping Week', 'woocommerce' ),
         'desc' 		=> __( 'This is to Set Shopping Week in Page Shop', 'woocommerce' ),
         'id' 		=> 'woocommerce_shopping_week',         
         'type' 		=> 'text',
         )
   );
   woocommerce_admin_fields( $woocommerce_settings[$my_tab_name] );
}

function update_shopping_week()
{
   $woocommerce_shopping_week = "woocommerce_shopping_week";      
   if(isset($_POST[$woocommerce_shopping_week]) AND strcasecmp($_POST[$woocommerce_shopping_week],"")!=0)
   {
      update_option( 'woocommerce_shopping_week', $_POST[$woocommerce_shopping_week] );
   }
}


function show_menu_order_column( $actions, $post )
{
   echo "<br>Order:<span style='font-weight:bold'> ".$post->menu_order."</span>";   
   return $actions;
}

function woocommerce_desc_sorting_link( $views ) {
	global $post_type, $wp_query;

	if ( ! current_user_can('edit_others_pages') ) return $views;
	$class = ( isset( $wp_query->query['orderby'] ) && $wp_query->query['order'] == 'desc' ) ? 'current' : '';
	$query_string = remove_query_arg(array( 'orderby', 'order' ));
	$query_string = add_query_arg( 'orderby', urlencode('menu_order'), $query_string );
	$query_string = add_query_arg( 'order', urlencode('DESC'), $query_string );
	$views['byorder_desc'] = '<a href="'. $query_string . '" class="' . $class . '">' . __('Sort Products Desc', 'woocommerce') . '</a>';

	return $views;
}

