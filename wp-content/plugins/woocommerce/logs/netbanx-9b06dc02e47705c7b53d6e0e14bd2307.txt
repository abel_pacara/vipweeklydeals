08-27-2013 @ 12:03:01 - Payment response received. Response is: Array
(
    [encodedMessage] => PHByb2ZpbGVSZXNwb25zZSB4bWxucz0id3d3Lm9wdGltYWxwYXltZW50cy5jb20vY2hlY2tvdXQi
PgogIDxkZWNpc2lvbj5DUkVBVEVEPC9kZWNpc2lvbj4KICA8Y3VzdG9tZXJTdGF0dXM+SU5JVElB
TDwvY3VzdG9tZXJTdGF0dXM+CiAgPGNvZGU+MDwvY29kZT4KICA8ZGVzY3JpcHRpb24+Tm8gRXJy
b3I8L2Rlc2NyaXB0aW9uPgogIDxjdXN0b21lclByb2ZpbGU+CiAgICA8bWVyY2hhbnRDdXN0b21l
cklkPjIwODg8L21lcmNoYW50Q3VzdG9tZXJJZD4KICAgIDxjdXN0b21lclRva2VuSWQ+TFRFME5U
VXlORGt6TVRFPC9jdXN0b21lclRva2VuSWQ+CiAgICA8aXNOZXdDdXN0b21lcj50cnVlPC9pc05l
d0N1c3RvbWVyPgogIDwvY3VzdG9tZXJQcm9maWxlPgo8L3Byb2ZpbGVSZXNwb25zZT4=
    [signature] => 9sWv5HjxCYXFkLXfgHNm7+zXjnQ=
    [wc-api] => WC_Gateway_Netbanx
)

08-27-2013 @ 12:03:01 - Validating response...
08-27-2013 @ 12:03:01 - Decoded message: <profileResponse xmlns="www.optimalpayments.com/checkout">
  <decision>CREATED</decision>
  <customerStatus>INITIAL</customerStatus>
  <code>0</code>
  <description>No Error</description>
  <customerProfile>
    <merchantCustomerId>2088</merchantCustomerId>
    <customerTokenId>LTE0NTUyNDkzMTE</customerTokenId>
    <isNewCustomer>true</isNewCustomer>
  </customerProfile>
</profileResponse>
08-27-2013 @ 12:03:01 - Generated Signature: 9sWv5HjxCYXFkLXfgHNm7+zXjnQ=
08-27-2013 @ 12:03:01 - Validation passed.
08-27-2013 @ 12:03:01 - Response XML is: SimpleXMLElement Object
(
    [decision] => CREATED
    [customerStatus] => INITIAL
    [code] => 0
    [description] => No Error
    [customerProfile] => SimpleXMLElement Object
        (
            [merchantCustomerId] => 2088
            [customerTokenId] => LTE0NTUyNDkzMTE
            [isNewCustomer] => true
        )

)

08-27-2013 @ 12:03:01 - Unrecognized payment status was received. Payment State: CREATED
08-27-2013 @ 12:03:07 - Payment response received. Response is: Array
(
    [encodedMessage] => PHByb2ZpbGVDaGVja291dFJlc3BvbnNlIHhtbG5zPSJ3d3cub3B0aW1hbHBheW1lbnRzLmNvbS9j
aGVja291dCIgeG1sbnM6eHNpPSJodHRwOi8vd3d3LnczLm9yZy8yMDAxL1hNTFNjaGVtYS1pbnN0
YW5jZSI+CiAgPGNvbmZpcm1hdGlvbk51bWJlcj4xNjE1MDI1ODYwPC9jb25maXJtYXRpb25OdW1i
ZXI+CiAgPG1lcmNoYW50UmVmTnVtPjM5NjEtMTA3NzU0NzA5MzwvbWVyY2hhbnRSZWZOdW0+CiAg
PGFjY291bnROdW0geHNpOm5pbD0idHJ1ZSIvPgogIDxjYXJkVHlwZT5YWDwvY2FyZFR5cGU+CiAg
PGRlY2lzaW9uPkVSUk9SPC9kZWNpc2lvbj4KICA8Y29kZT4yNTA1PC9jb2RlPgogIDxhY3Rpb25D
b2RlPk08L2FjdGlvbkNvZGU+CiAgPGRlc2NyaXB0aW9uPk5vIG1lcmNoYW50IGFjY291bnQgY291
bGQgYmUgZm91bmQgZm9yIHRoZSBjdXJyZW5jeSB5b3Ugc3VibWl0dGVkLiBQbGVhc2UgdmVyaWZ5
IHlvdXIgcGFyYW1ldGVycyBhbmQgcmV0cnkgdGhlIHRyYW5zYWN0aW9uLjwvZGVzY3JpcHRpb24+
CiAgPHR4blRpbWU+MjAxMy0wOC0yN1QwODowMzowNi40NzgtMDQ6MDA8L3R4blRpbWU+CiAgPHBh
eW1lbnRNZXRob2Q+Q0M8L3BheW1lbnRNZXRob2Q+CjwvcHJvZmlsZUNoZWNrb3V0UmVzcG9uc2U+

    [signature] => lEkOVpyN7CY4dtz0axShGVD+Fuo=
    [wc-api] => WC_Gateway_Netbanx
)

08-27-2013 @ 12:03:07 - Validating response...
08-27-2013 @ 12:03:07 - Decoded message: <profileCheckoutResponse xmlns="www.optimalpayments.com/checkout" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <confirmationNumber>1615025860</confirmationNumber>
  <merchantRefNum>3961-1077547093</merchantRefNum>
  <accountNum xsi:nil="true"/>
  <cardType>XX</cardType>
  <decision>ERROR</decision>
  <code>2505</code>
  <actionCode>M</actionCode>
  <description>No merchant account could be found for the currency you submitted. Please verify your parameters and retry the transaction.</description>
  <txnTime>2013-08-27T08:03:06.478-04:00</txnTime>
  <paymentMethod>CC</paymentMethod>
</profileCheckoutResponse>
08-27-2013 @ 12:03:07 - Generated Signature: lEkOVpyN7CY4dtz0axShGVD+Fuo=
08-27-2013 @ 12:03:07 - Validation passed.
08-27-2013 @ 12:03:07 - Response XML is: SimpleXMLElement Object
(
    [confirmationNumber] => 1615025860
    [merchantRefNum] => 3961-1077547093
    [accountNum] => SimpleXMLElement Object
        (
        )

    [cardType] => XX
    [decision] => ERROR
    [code] => 2505
    [actionCode] => M
    [description] => No merchant account could be found for the currency you submitted. Please verify your parameters and retry the transaction.
    [txnTime] => 2013-08-27T08:03:06.478-04:00
    [paymentMethod] => CC
)

08-27-2013 @ 12:03:07 - There was an error during the payment process. Error message: No merchant account could be found for the currency you submitted. Please verify your parameters and retry the transaction.
08-27-2013 @ 12:03:30 - Payment response received. Response is: Array
(
    [encodedMessage] => PHByb2ZpbGVDaGVja291dFJlc3BvbnNlIHhtbG5zPSJ3d3cub3B0aW1hbHBheW1lbnRzLmNvbS9j
aGVja291dCIgeG1sbnM6eHNpPSJodHRwOi8vd3d3LnczLm9yZy8yMDAxL1hNTFNjaGVtYS1pbnN0
YW5jZSI+CiAgPGNvbmZpcm1hdGlvbk51bWJlcj4xNjE1MDI2ODYwPC9jb25maXJtYXRpb25OdW1i
ZXI+CiAgPG1lcmNoYW50UmVmTnVtPjM5NjEtMTA3NzU0NzA5MzwvbWVyY2hhbnRSZWZOdW0+CiAg
PGFjY291bnROdW0geHNpOm5pbD0idHJ1ZSIvPgogIDxjYXJkVHlwZT5YWDwvY2FyZFR5cGU+CiAg
PGRlY2lzaW9uPkVSUk9SPC9kZWNpc2lvbj4KICA8Y29kZT4yNTA1PC9jb2RlPgogIDxhY3Rpb25D
b2RlPk08L2FjdGlvbkNvZGU+CiAgPGRlc2NyaXB0aW9uPk5vIG1lcmNoYW50IGFjY291bnQgY291
bGQgYmUgZm91bmQgZm9yIHRoZSBjdXJyZW5jeSB5b3Ugc3VibWl0dGVkLiBQbGVhc2UgdmVyaWZ5
IHlvdXIgcGFyYW1ldGVycyBhbmQgcmV0cnkgdGhlIHRyYW5zYWN0aW9uLjwvZGVzY3JpcHRpb24+
CiAgPHR4blRpbWU+MjAxMy0wOC0yN1QwODowMzoyOS44OTMtMDQ6MDA8L3R4blRpbWU+CiAgPHBh
eW1lbnRNZXRob2Q+Q0M8L3BheW1lbnRNZXRob2Q+CjwvcHJvZmlsZUNoZWNrb3V0UmVzcG9uc2U+

    [signature] => cgOeoADMPpCHjUZlJ+KYzzHdDos=
    [wc-api] => WC_Gateway_Netbanx
)

08-27-2013 @ 12:03:30 - Validating response...
08-27-2013 @ 12:03:30 - Decoded message: <profileCheckoutResponse xmlns="www.optimalpayments.com/checkout" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <confirmationNumber>1615026860</confirmationNumber>
  <merchantRefNum>3961-1077547093</merchantRefNum>
  <accountNum xsi:nil="true"/>
  <cardType>XX</cardType>
  <decision>ERROR</decision>
  <code>2505</code>
  <actionCode>M</actionCode>
  <description>No merchant account could be found for the currency you submitted. Please verify your parameters and retry the transaction.</description>
  <txnTime>2013-08-27T08:03:29.893-04:00</txnTime>
  <paymentMethod>CC</paymentMethod>
</profileCheckoutResponse>
08-27-2013 @ 12:03:30 - Generated Signature: cgOeoADMPpCHjUZlJ+KYzzHdDos=
08-27-2013 @ 12:03:30 - Validation passed.
08-27-2013 @ 12:03:30 - Response XML is: SimpleXMLElement Object
(
    [confirmationNumber] => 1615026860
    [merchantRefNum] => 3961-1077547093
    [accountNum] => SimpleXMLElement Object
        (
        )

    [cardType] => XX
    [decision] => ERROR
    [code] => 2505
    [actionCode] => M
    [description] => No merchant account could be found for the currency you submitted. Please verify your parameters and retry the transaction.
    [txnTime] => 2013-08-27T08:03:29.893-04:00
    [paymentMethod] => CC
)

08-27-2013 @ 12:03:30 - There was an error during the payment process. Error message: No merchant account could be found for the currency you submitted. Please verify your parameters and retry the transaction.
08-27-2013 @ 12:06:55 - Payment response received. Response is: Array
(
    [encodedMessage] => PHByb2ZpbGVDaGVja291dFJlc3BvbnNlIHhtbG5zPSJ3d3cub3B0aW1hbHBheW1lbnRzLmNvbS9j
aGVja291dCIgeG1sbnM6eHNpPSJodHRwOi8vd3d3LnczLm9yZy8yMDAxL1hNTFNjaGVtYS1pbnN0
YW5jZSI+CiAgPGNvbmZpcm1hdGlvbk51bWJlcj4xNjE1MDM0NjkwPC9jb25maXJtYXRpb25OdW1i
ZXI+CiAgPG1lcmNoYW50UmVmTnVtPjM5NjEtMjkwOTUxNTIzPC9tZXJjaGFudFJlZk51bT4KICA8
YWNjb3VudE51bSB4c2k6bmlsPSJ0cnVlIi8+CiAgPGNhcmRUeXBlPlhYPC9jYXJkVHlwZT4KICA8
ZGVjaXNpb24+RVJST1I8L2RlY2lzaW9uPgogIDxjb2RlPjI1MDU8L2NvZGU+CiAgPGFjdGlvbkNv
ZGU+TTwvYWN0aW9uQ29kZT4KICA8ZGVzY3JpcHRpb24+Tm8gbWVyY2hhbnQgYWNjb3VudCBjb3Vs
ZCBiZSBmb3VuZCBmb3IgdGhlIGN1cnJlbmN5IHlvdSBzdWJtaXR0ZWQuIFBsZWFzZSB2ZXJpZnkg
eW91ciBwYXJhbWV0ZXJzIGFuZCByZXRyeSB0aGUgdHJhbnNhY3Rpb24uPC9kZXNjcmlwdGlvbj4K
ICA8dHhuVGltZT4yMDEzLTA4LTI3VDA4OjA2OjU0LjU2Ni0wNDowMDwvdHhuVGltZT4KICA8cGF5
bWVudE1ldGhvZD5DQzwvcGF5bWVudE1ldGhvZD4KPC9wcm9maWxlQ2hlY2tvdXRSZXNwb25zZT4=
    [signature] => LUi7oE7PPbfoyxjDWXSVd4A04ZQ=
    [wc-api] => WC_Gateway_Netbanx
)

08-27-2013 @ 12:06:55 - Validating response...
08-27-2013 @ 12:06:55 - Decoded message: <profileCheckoutResponse xmlns="www.optimalpayments.com/checkout" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <confirmationNumber>1615034690</confirmationNumber>
  <merchantRefNum>3961-290951523</merchantRefNum>
  <accountNum xsi:nil="true"/>
  <cardType>XX</cardType>
  <decision>ERROR</decision>
  <code>2505</code>
  <actionCode>M</actionCode>
  <description>No merchant account could be found for the currency you submitted. Please verify your parameters and retry the transaction.</description>
  <txnTime>2013-08-27T08:06:54.566-04:00</txnTime>
  <paymentMethod>CC</paymentMethod>
</profileCheckoutResponse>
08-27-2013 @ 12:06:55 - Generated Signature: LUi7oE7PPbfoyxjDWXSVd4A04ZQ=
08-27-2013 @ 12:06:55 - Validation passed.
08-27-2013 @ 12:06:55 - Response XML is: SimpleXMLElement Object
(
    [confirmationNumber] => 1615034690
    [merchantRefNum] => 3961-290951523
    [accountNum] => SimpleXMLElement Object
        (
        )

    [cardType] => XX
    [decision] => ERROR
    [code] => 2505
    [actionCode] => M
    [description] => No merchant account could be found for the currency you submitted. Please verify your parameters and retry the transaction.
    [txnTime] => 2013-08-27T08:06:54.566-04:00
    [paymentMethod] => CC
)

08-27-2013 @ 12:06:55 - There was an error during the payment process. Error message: No merchant account could be found for the currency you submitted. Please verify your parameters and retry the transaction.
08-28-2013 @ 01:13:54 - Payment response received. Response is: Array
(
    [encodedMessage] => PHByb2ZpbGVDaGVja291dFJlc3BvbnNlIHhtbG5zPSJ3d3cub3B0aW1hbHBheW1lbnRzLmNvbS9j
aGVja291dCIgeG1sbnM6eHNpPSJodHRwOi8vd3d3LnczLm9yZy8yMDAxL1hNTFNjaGVtYS1pbnN0
YW5jZSI+CiAgPGNvbmZpcm1hdGlvbk51bWJlcj4xNjE3MTU3NzAwPC9jb25maXJtYXRpb25OdW1i
ZXI+CiAgPG1lcmNoYW50UmVmTnVtPjM5NjItMTM0NzIyNTU2MjwvbWVyY2hhbnRSZWZOdW0+CiAg
PGFjY291bnROdW0geHNpOm5pbD0idHJ1ZSIvPgogIDxjYXJkVHlwZT5YWDwvY2FyZFR5cGU+CiAg
PGRlY2lzaW9uPkVSUk9SPC9kZWNpc2lvbj4KICA8Y29kZT4yNTA1PC9jb2RlPgogIDxhY3Rpb25D
b2RlPk08L2FjdGlvbkNvZGU+CiAgPGRlc2NyaXB0aW9uPk5vIG1lcmNoYW50IGFjY291bnQgY291
bGQgYmUgZm91bmQgZm9yIHRoZSBjdXJyZW5jeSB5b3Ugc3VibWl0dGVkLiBQbGVhc2UgdmVyaWZ5
IHlvdXIgcGFyYW1ldGVycyBhbmQgcmV0cnkgdGhlIHRyYW5zYWN0aW9uLjwvZGVzY3JpcHRpb24+
CiAgPHR4blRpbWU+MjAxMy0wOC0yN1QyMToxMzo1Mi41MTQtMDQ6MDA8L3R4blRpbWU+CiAgPHBh
eW1lbnRNZXRob2Q+Q0M8L3BheW1lbnRNZXRob2Q+CjwvcHJvZmlsZUNoZWNrb3V0UmVzcG9uc2U+

    [signature] => 6Ai+8MsbVRdh72dOwepc12gfR6s=
    [wc-api] => WC_Gateway_Netbanx
)

08-28-2013 @ 01:13:54 - Validating response...
08-28-2013 @ 01:13:54 - Decoded message: <profileCheckoutResponse xmlns="www.optimalpayments.com/checkout" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <confirmationNumber>1617157700</confirmationNumber>
  <merchantRefNum>3962-1347225562</merchantRefNum>
  <accountNum xsi:nil="true"/>
  <cardType>XX</cardType>
  <decision>ERROR</decision>
  <code>2505</code>
  <actionCode>M</actionCode>
  <description>No merchant account could be found for the currency you submitted. Please verify your parameters and retry the transaction.</description>
  <txnTime>2013-08-27T21:13:52.514-04:00</txnTime>
  <paymentMethod>CC</paymentMethod>
</profileCheckoutResponse>
08-28-2013 @ 01:13:54 - Generated Signature: 6Ai+8MsbVRdh72dOwepc12gfR6s=
08-28-2013 @ 01:13:54 - Validation passed.
08-28-2013 @ 01:13:54 - Response XML is: SimpleXMLElement Object
(
    [confirmationNumber] => 1617157700
    [merchantRefNum] => 3962-1347225562
    [accountNum] => SimpleXMLElement Object
        (
        )

    [cardType] => XX
    [decision] => ERROR
    [code] => 2505
    [actionCode] => M
    [description] => No merchant account could be found for the currency you submitted. Please verify your parameters and retry the transaction.
    [txnTime] => 2013-08-27T21:13:52.514-04:00
    [paymentMethod] => CC
)

08-28-2013 @ 01:13:54 - There was an error during the payment process. Error message: No merchant account could be found for the currency you submitted. Please verify your parameters and retry the transaction.
