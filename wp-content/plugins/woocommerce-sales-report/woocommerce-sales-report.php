<?php
/*
  Plugin Name: WooCommerce Sales Report
  Description: Sales Report with Variations
  Version: 1.0
  Author: www.onebolivia.com
 */

add_filter( 'woocommerce_reports_charts', 'my_woocommerce_reports_charts' );

function my_woocommerce_reports_charts( $array_options )
{
   $array_options[ 'sales' ][ 'charts' ]['sales_report'] = array(
      'title'=>__( 'Sales Report', 'woocommerce' ),
      'description'=>'',
      'function'=>'my_woocommerce_sales_report'
   );

   return $array_options;
}
function orders_within_range( $where = '' ) {
	global $start_date, $end_date;

	$after = date('Y-m-d', $start_date);
	$before = date('Y-m-d', strtotime('+1 day', $end_date));

	$where .= " AND post_date > '$after'";
	$where .= " AND post_date < '$before'";

	return $where;
}
function my_woocommerce_sales_report()
{
   /**
    * Output the top sellers chart.
    *
    * @access public
    * @return void
    */
   global $start_date, $end_date, $woocommerce;

   $start_date = (isset( $_POST[ 'start_date' ] )) ? $_POST[ 'start_date' ] : '';
   $end_date = (isset( $_POST[ 'end_date' ] )) ? $_POST[ 'end_date' ] : '';

   if ( ! $start_date )
      $start_date = date( 'Ymd', strtotime( date( 'Ym', current_time( 'timestamp' ) ) . '01' ) );
   if ( ! $end_date )
      $end_date = date( 'Ymd', current_time( 'timestamp' ) );

   $start_date = strtotime( $start_date );
   $end_date = strtotime( $end_date );

   // Get orders to display in widget
   add_filter( 'posts_where', 'orders_within_range' );

   $args = array(
      'numberposts'=>-1,
      'orderby'=>'post_date',
      'order'=>'ASC',
      'post_type'=>'shop_order',
      'post_status'=>'publish',
      'suppress_filters'=>0,
      'tax_query'=>array(
         array(
            'taxonomy'=>'shop_order_status',
            'terms'=>array( 'completed', 'processing', 'on-hold' ),
            'field'=>'slug',
            'operator'=>'IN'
         )
      )
   );


   $orders = get_posts( $args );

   $found_products = array( );

   $found_products_is_variation = array( );
   
   
   
   if ($orders){
		foreach ($orders as $order){
         
         $my_order = new WC_Order($order->ID);
         
			$order_items = $my_order->get_items();
         
			foreach ($order_items as $item) 
         {
            if(isset($item['variation_id']) AND 
               strcasecmp(trim($item['variation_id']),"")!=0 AND 
               strcasecmp(trim($item['variation_id']),"0")!=0)
            {
               $item['id'] = $item['variation_id'];
               $found_products_is_variation[$item['id']]['is_variation'] = true;
            }
            else
            {
               $item['id'] = $item['product_id'];
               $found_products_is_variation[$item['id']]['is_variation'] = false;
            }
            
            $found_products[$item['id']] = isset($found_products[$item['id']]) ? $found_products[$item['id']] + $item['qty'] : $item['qty'];            
         }
      }
   }
   #-----------------------------------------------------------------------
   //$array_available_variations = $product->get_available_variations();

   #-----------------------------------------------------------------------
   
   /*
   echo "<pre>";
   echo print_r($found_products);
   echo "</pre>";
   */
   
   /*
   echo "<pre>";
   echo "<br>COUNT = ".count($found_products);
   echo "</pre>";
   */
   
   
   
   asort( $found_products );
   $found_products = array_reverse( $found_products, true );
   //$found_products = array_slice( $found_products, 0, 25, true );
   reset( $found_products );
   
   remove_filter( 'posts_where', 'orders_within_range' );
    
   
    /*
   echo "<pre>";
   echo "<br>COUNT = ".count($found_products);
   echo "</pre>";
   */
   ?>
   <form method="post" action="">
      <p><label for="from"><?php _e( 'From:', 'woocommerce' ); ?></label> <input type="text" name="start_date" id="from" readonly="readonly" value="<?php echo esc_attr( date( 'Y-m-d', $start_date ) ); ?>" /> 
         <label for="to"><?php _e( 'To:', 'woocommerce' ); ?></label> <input type="text" name="end_date" id="to" readonly="readonly" value="<?php echo esc_attr( date( 'Y-m-d', $end_date ) ); ?>" /> 
         <input type="submit" class="button" value="<?php _e( 'Show', 'woocommerce' ); ?>" />
      </p>
   </form>
   <table class="bar_chart">
      <thead>
         <tr>
            <th><?php _e( 'Product', 'woocommerce' ); ?></th>
            <th><?php _e( 'Sales', 'woocommerce' ); ?></th>
         </tr>
      </thead>
      <tbody>
         <?php
         /*
         echo "<pre>";
         print_r($found_products);
         echo "</pre>";
         */
         $max_sales = current( $found_products );
         foreach ( $found_products as $product_id=>$sales )
         {
            $width = ($sales > 0) ? ($sales / $max_sales) * 100 : 0;
            
            //$str_postfix = " [".$product_id."]";
            //$product = get_post( $product_id );
            
            $product_title = "";            
            //$my_product = new WC_Product($product_id);
            $variation_name = "";
            
            if(isset($found_products_is_variation[$product_id]['is_variation']) AND
               $found_products_is_variation[$product_id]['is_variation']===true)
            {
               //woocommerce_get_product_terms( $product_id, "pa_date_expiration", "names");                                 
               //$product_title = $my_product->get_title()." [variation]";
               $my_product = new WC_Product_Variable($product_id);               
               $product_title = $my_product->get_title();               
               
               $array_available_variations  = $my_product->get_available_variations();               
               
               if(isset($array_available_variations) AND count($array_available_variations)>0)
               {
                  for($i=0; $i<count($array_available_variations); $i++)
                  {
                     if(strcasecmp($array_available_variations[$i]['variation_id'],$product_id)==0)
                     {
                        $attributes = $array_available_variations[$i]['attributes'];
                        
                        foreach($attributes AS $key_attr => $value_attr)
                        {
                           if(isset($value_attr) AND strcasecmp(trim($value_attr), "")!=0)
                           {
                              $variation_name = " (".$value_attr.") ";
                              $i=count($array_available_variations);
                              break;
                              //break;
                           }
                        }
                     }
                  }
               }
            }
            else if(strcasecmp(trim($product_id),"0")!=0)
            {
               $my_product = new WC_Product_Simple($product_id);
               $product_title = $my_product->get_title();
            }
            else
            {
               echo "ID = 0";
            }
             
            if(isset($my_product)) 
            {
               $product_name = '<a href="' . get_permalink( $my_product->id ) . '">' . $product_title. '</a>' .$variation_name;
               $orders_link = admin_url( 'edit.php?s&post_status=all&post_type=shop_order&action=-1&s=' . urlencode( $my_product->get_title() ) . '&shop_order_status=completed,processing,on-hold' );
               echo '<tr><th>' .$product_name . '</th><td width="1%"><span>' . $sales . '</span></td><td class="bars"><a href="' . $orders_link . '" style="width:' . $width . '%">&nbsp;</a></td></tr>';
            }
            /*else 
            {
               $product_name = __( 'Product does not exist', 'woocommerce' );
               $orders_link = admin_url( 'edit.php?s&post_status=all&post_type=shop_order&action=-1&s=&shop_order_status=completed,processing,on-hold' );
            }*/
         }
         ?>
      </tbody>
   </table>
   <script type="text/javascript">
      jQuery(function() {
   <?php woocommerce_datepicker_js(); ?>
      });
   </script>
   <?php
}