<?php
/*
  Plugin Name: WooCommerce Postal CSV
  Description: WooCommerce Postal CSV
  Version: 1.0
  Author: www.onebolivia.com
 */
//echo "<br>DIR = ".getcwd();
global $woocommerce_settings;

//include('../woocommerce/admin/woocommerce-admin-settings.php');

add_action('woocommerce_reports_charts', 'my_woocommerce_chart_tab_postal_csv');
add_action('woocommerce_update_options','update_postal_csv');

function my_woocommerce_chart_tab_postal_csv($tabs)
{
   $my_tab_name = "postal_csv";   
   $tabs[$my_tab_name]= array(
                  'title' 	=>  __( 'Postal CSV', 'woocommerce' ),
                  'charts' 	=> array(
                     array(
                        'title' => __('Postal CSV', 'woocommerce'),
                        'description' => '',
                        'hide_title' => true,
                        'function' => 'postal_csv'
                     ),
                  )
               );
   //add_action('woocommerce_reports_tabs', 'postal_csv');   
   return $tabs;   
}
#-------------------------------------------------------------------------------------
function postal_csv()
{
   global $woocommerce_settings;
   $my_tab_name = "postal_csv"; 
   $woocommerce_order_start = "woocommerce_order_start";      
   $woocommerce_order_end = "woocommerce_order_end";      
   
   
   if(isset($_REQUEST['saved']))
   {
      global $option_woocommerce_order_start;
      global $option_woocommerce_order_end;
      
      update_option($woocommerce_order_start, $_REQUEST[$woocommerce_order_start]);
      update_option($woocommerce_order_end, $_REQUEST[$woocommerce_order_end]);

      $option_woocommerce_order_start =  $_REQUEST[$woocommerce_order_start];
      $option_woocommerce_order_end = $_REQUEST[$woocommerce_order_end];

      
      
      function my_filter_where_after_postal_csv( $where = '' ) 
      { 
         global $option_woocommerce_order_start;      
         global $option_woocommerce_order_end;

         $where .= " AND  ID >= ".$option_woocommerce_order_start;
         $where .= " AND  ID <= ".$option_woocommerce_order_end."  ";
         return $where;
      }
      add_filter( 'posts_where', 'my_filter_where_after_postal_csv' ); 
      
      $path_file =dirname(__FILE__)."/postal.csv";
      
      $fp = fopen($path_file, 'w');      
      
      $postal_columns = array(
            "ORDER",
            //"status",
            "Name",
            "Company Name",
            "Shipping Address 1",
            "Shipping Address 2",
            "",
            "",
            "Shipping City",
            "Shipping State",
            "Shipping Postcode",
            "Shipping Country",
            "",
            "Billing Phone",
            "",
            "Billing Email",
            "Customer Note",
      );
      fputcsv($fp, $postal_columns);
      
      
      $orders_per_page = 100;
      $order_offset = 0;
      
      
      
      do
      {
         $args_orders = array(
         'post_type' => 'shop_order',
         'orderby'=>'ID',
         'posts_per_page'=>$orders_per_page,
         'offset'=>$order_offset,
         'nopaging'=>false,
         'order'=>'ASC',
         'tax_query'=> 
                  array(
                     array(
                        'taxonomy' => 'shop_order_status',
                        'field' => 'slug',
                        'terms' => array('processing')
                        )
               )
         );
         
         $query_orders = new WP_Query( $args_orders );
                 
         $my_orders = $query_orders->posts;
        
         if(count($my_orders)>0)
         {
            $order_offset = $order_offset+count($my_orders);
            for($i=0; $i<count($my_orders);$i++)
            {
               $postal_item['order'] = $my_orders[$i]->ID;  
               
               /*$post_terms = wp_get_post_terms($my_orders[$i]->ID, 'shop_order_status', array("fields" => "names")); 
         
               foreach($post_terms AS $term_item)
               {
                  $postal_item['shop_order_status'] = $term_item;
               }*/
               
               $postal_item['name'] = get_post_meta($my_orders[$i]->ID, "_shipping_first_name", true)." ".
                                      get_post_meta($my_orders[$i]->ID, "_shipping_last_name", true);
               
               $postal_item['shipping_company'] = get_post_meta($my_orders[$i]->ID, "_shipping_company", true);
               
               $postal_item['shipping_address_1'] = get_post_meta($my_orders[$i]->ID, "_shipping_address_1", true); 
               $postal_item['shipping_address_2'] = get_post_meta($my_orders[$i]->ID, "_shipping_address_2", true); 
               $postal_item['after_sa1'] ="";
               $postal_item['after_sa2'] ="";
               $postal_item['shipping_city'] = get_post_meta($my_orders[$i]->ID, "_shipping_city", true); 
               $postal_item['shipping_state'] = get_post_meta($my_orders[$i]->ID, "_shipping_state", true); 
               $postal_item['shipping_postcode'] = get_post_meta($my_orders[$i]->ID, "_shipping_postcode", true); 
               $postal_item['shipping_country'] = get_post_meta($my_orders[$i]->ID, "_shipping_country", true); 
               $postal_item['before_phone'] = ""; 
               $postal_item['billing_phone'] = get_post_meta($my_orders[$i]->ID, "_billing_phone", true);  
               $postal_item['after_phone'] = ""; 
               $postal_item['billing_email'] = get_post_meta($my_orders[$i]->ID, "_billing_email", true);  
               $postal_item['customer_note'] = $my_orders[$i]->post_excerpt; 
               fputcsv($fp, $postal_item);
            }
         }
      }
      while($orders_per_page<=count($my_orders));
      
      remove_filter("posts_where", "my_filter_where_after_postal_csv");      
      fclose($fp);
      
      //exit;
      #-------------------------------------------------------------------------------------------------------------
      
      ob_start();
      header('Content-Description: File Transfer');
      header('Content-Type: application/octet-stream');
      header('Content-Disposition: attachment; filename=postal.csv');
      header('Content-Transfer-Encoding: binary');
      header('Expires: 0');
      header('Cache-Control: must-revalidate');
      header('Pragma: public');
      
      $fp = fopen($path_file, 'a+');  
      
      header('Content-Length: ' . filesize($path_file));
      fclose($fp);
      
      ob_end_flush();
      ob_clean();
      flush();
      
      readfile($path_file);
      unlink($path_file);
   }
   #--------------------------------------------------
   $args_max_order = array(
      'post_type' => 'shop_order',
      'no_found_rows' 		=> 1,
      'orderby'=>'ID',
      'order'=>'DESC',
      'posts_per_page'=>1,
      'tax_query'=> array(
                     array(
                        'taxonomy' => 'shop_order_status',
                        'field' => 'slug',
                        'terms' => array('processing')
                     )
               )
      );

   $max_query_orders = new WP_Query( $args_max_order );
   
   $max_my_order = $max_query_orders->posts;
   
   if(!isset($_REQUEST['saved']))
   {
      update_option( $woocommerce_order_start, 1 );
      update_option( $woocommerce_order_end,  $max_my_order[0]->ID);
   }
   
   #--------------------------------------------------
   //echo "<br>MAX ID = ".$max_my_order[0]->ID;
   #--------------------------------------------------
   $woo_order_start = get_option( $woocommerce_order_start);
   $woo_order_end = get_option( $woocommerce_order_end);
   
   if(!isset($woo_order_start) OR (isset($woo_order_start) AND strcasecmp(trim($woo_order_start),"")==0))
   {
      update_option( $woocommerce_order_start, 1 );
   }
   
   if(!isset($woo_order_end) OR (isset($woo_order_end) AND strcasecmp(trim($woo_order_end),"")==0))
   {
      update_option( $woocommerce_order_end,  $max_my_order[0]->ID);
   }
   
   //woocommerce_admin_fields( $woocommerce_settings[$my_tab_name] );
   
   $my_uri = "wp-admin/admin.php?page=woocommerce_reports&tab=postal_csv";
   /*
   $option_woocommerce_order_start = get_option($woocommerce_order_start);
   $option_woocommerce_order_end = get_option($woocommerce_order_end);
    * 
    */
   $option_woocommerce_order_start = get_option($woocommerce_order_start);
   $option_woocommerce_order_end = get_option($woocommerce_order_end);

   ?>
<form id="mainform" action="<?php echo site_url()."/".$my_uri; ?>" method="post">
   <input type="text" value="<?php echo $option_woocommerce_order_start?>" name="woocommerce_order_start"/>
   <input type="text" value="<?php echo $option_woocommerce_order_end?>" name="woocommerce_order_end"/>
   
   
   <br/>
   <div><?php echo "Last order: ".$max_my_order[0]->ID;?></div>
   
   <input type="submit" class="button-primary" name="saved" value="download Postal CSV ( <?php echo date("M / d / Y H:i:s")?> )">   
   </form>
   <?php   
}
#-------------------------------------------------------------------------------------
function update_postal_csv()
{   
   $woocommerce_order_start = "woocommerce_order_start";      
   $woocommerce_order_end = "woocommerce_order_end";      
   
   if(isset($_POST[$woocommerce_order_start]) AND strcasecmp($_POST[$woocommerce_order_start],"")!=0)
   {
      update_option( $woocommerce_order_start, $_POST[$woocommerce_order_start] );
   }
   if(isset($_POST[$woocommerce_order_end]) AND strcasecmp($_POST[$woocommerce_order_end],"")!=0)
   {
      update_option( $woocommerce_order_end, $_POST[$woocommerce_order_end] );
   }
}

//remove_filter('woocommerce_settings_tabs_array', 'my_woocommerce_settings_tab_postal_csv');