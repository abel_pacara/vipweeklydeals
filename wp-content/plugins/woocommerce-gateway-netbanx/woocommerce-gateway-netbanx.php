<?php
/*
 * Plugin Name: WooCommerce Netbanx Payment Gateway
 * Plugin URI: http://woothemes.com/woocommerce
 * Description: Allows you to use <a href="http://www1.netbanx.com">Netbanx</a> payment processor with the WooCommerce plugin.
 * Version: 1.0.4
 * Author: Ivan Andreev
 * Author URI: http://www.vanbodevelops.com
 * 
 *	Copyright: (c) 2012 - 2013 Ivan Andreev
 *	License: GNU General Public License v3.0
 *	License URI: http://www.gnu.org/licenses/gpl-3.0.html
 */

/**
 * Required functions
 */
if ( ! function_exists( 'woothemes_queue_update' ) )
	require_once( 'woo-includes/woo-functions.php' );

/**
 * Plugin updates
 */
woothemes_queue_update( plugin_basename( __FILE__ ), 'a356067d101646331ab9daa636dc0d6e', '122157' );

/**
 * Check for a callback response
 **/
function netbanx_api_check() {
	if ( class_exists( 'WC_Gateway_Netbanx' ) ) {
		if ( isset($_POST['wc-api']) && $_POST['wc-api'] == 'WC_Gateway_Netbanx' ) {
			$init_gateway = new WC_Gateway_Netbanx();
			do_action('woocommerce_api_wc_gateway_netbanx');
		}
	}
}
add_action( 'init', 'netbanx_api_check' );

add_action('plugins_loaded', 'add_wc_netbanx_gateway', 0);
function add_wc_netbanx_gateway() {

	if ( ! class_exists( 'WC_Payment_Gateway' ) ) { return; }

	class WC_Gateway_Netbanx extends WC_Payment_Gateway {

		public function __construct() {
			global $woocommerce;

			$this->id		= 'netbanx';
			$this->icon		= apply_filters('woocommerce_netbanx_icon', plugin_dir_url(__FILE__) .'images/netbanx_logo.png');
			$this->has_fields	= false;
			$this->posturl		= 'www.optimalpayments.com/checkout';
			//$this->liveurl		= 'https://checkout.optimalpayments.com/securePayment/op/profileCheckoutRequest.htm';
         //$this->testurl		= 'https://checkout.test.optimalpayments.com/securePayment/op/profileCheckoutRequest.htm';
         $this->liveurl		= 'https://checkout.optimalpayments.com/securePayment/standalone/profileCheckoutRequest.htm';			
         $this->testurl		= 'https://checkout.test.optimalpayments.com/securePayment/standalone/profileCheckoutRequest.htm';
         
         $this->liveurl_op_create_pro = "https://checkout.optimalpayments.com/securePayment/op/createProfileRequest.htm";
         $this->testurl_op_create_pro = "https://checkout.test.optimalpayments.com/securePayment/op/createProfileRequest.htm";
         
         
			$this->method_title	= __( 'Netbanx', 'wc_netbanx' );
			// Load the form fields.
			$this->init_form_fields();

			// Load the settings.
			$this->init_settings();

			// Define user set variables
			$this->title		= $this->settings['title'];
			$this->description	= $this->settings['description'];
			$this->enabled		= isset( $this->settings['enabled'] ) ? $this->settings['enabled'] : 'no';
			$this->testmode		= isset( $this->settings['testmode'] ) ? $this->settings['testmode'] : 'yes';
			
         if ( $this->testmode == 'yes' ) 
         {
           $this->shop_id		= $this->settings['shop_id_test'];
           $this->shop_key		= $this->settings['shop_key_test'];
         }
         else
         {
            $this->shop_id		= $this->settings['shop_id'];
            $this->shop_key		= $this->settings['shop_key'];
         }
         
         
			$this->account_num	= $this->settings['account_num'];
			$this->debug		= isset( $this->settings['debug'] ) ? $this->settings['debug'] : 'no';
			$this->is_wc_2		= ( defined('WOOCOMMERCE_VERSION') && version_compare(WOOCOMMERCE_VERSION, '2.0', '>=') );

			// Logs
			if ( 'yes' == $this->debug ) $this->log = $woocommerce->logger();

			// Actions
			add_action( 'woocommerce_api_wc_gateway_netbanx', array(&$this, 'check_status_response_netbanx') );
			
			add_action( 'woocommerce_receipt_netbanx', array(&$this, 'receipt_page') );
			
			// Save options
			add_action( 'woocommerce_update_options_payment_gateways', array( $this, 'process_admin_options') );
			add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );
			
			add_action( 'admin_notices', array( $this, 'netbanx_check_ssl' ) );
			add_action( 'init', array( $this, 'on_init' ) );
			
		}
		
		/**
		 * Any on init steps
		 */
		function on_init() {
			// Localisation
			load_plugin_textdomain('wc_netbanx', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/');
		}

		/**
		 * Admin Panel Options
		 **/
		public function admin_options() {
			?>
			<h3><?php _e('Netbanx', 'wc_netbanx'); ?></h3>
			<p><?php _e('Netbanx work by sending the customer to NetBanx page to make a payment and then presents the customer with an appropriate payment response page.', 'wc_netbanx'); ?></p>

			<p><?php
			    _e('Give this URL to NETBANX as "Callback URL": ', 'wc_netbanx'); ?>
			    <span style="color: red">
				    <strong><?php echo str_replace( 'http:', 'https:', add_query_arg('wc-api', 'WC_Gateway_Netbanx', home_url( '/' ) ) ); ?></strong>
			    </span>
			    <?php
			    _e('Or you can provide a different page, but it has to be SSL enabled and with the "wc-api=WC_Gateway_Netbanx" parameter appended.', 'wc_netbanx'); ?>
			</p>

			<table class="form-table">
			<?php
				// Generate the HTML For the settings form.
				$this->generate_settings_html();
			?>
			</table><!--/.form-table-->
			<?php
		} // End admin_options()

		/**
		 * Initialise Gateway Settings Form Fields
		 **/
		function init_form_fields() {

			$this->form_fields = array(
				'enabled' => array(
					'title'		=> __( 'Enable/Disable', 'wc_netbanx' ),
					'type'		=> 'checkbox',
					'label'		=> __( 'Enable Netbanx', 'wc_netbanx' ),
					'default'	=> 'no'
				),
				'testmode'	=> array(
					'title'		=> __( 'Sandbox', 'wc_netbanx' ),
					'type'		=> 'checkbox',
					'label'		=> __( 'Enable Sandbox', 'wc_netbanx' ),
					'default'	=> 'yes'
				),
				'title'		=> array(
					'title'		=> __( 'Method Title', 'wc_netbanx' ),
					'type'		=> 'text',
					'description'	=> __( 'This controls the title which the user sees during checkout.', 'wc_netbanx' ),
					'default'	=> __( 'Netbanx', 'wc_netbanx' )
				),
				'description'	=> array(
					'title'		=> __( 'Description', 'wc_netbanx' ),
					'type'		=> 'textarea',
					'description'	=> __( 'This controls the description which the user sees during checkout.', 'wc_netbanx' ),
					'default'	=> __("Pay using your credit/debit card.", 'wc_netbanx')
				),
				
            'shop_id'	=> array(
					'title'		=> __( 'Shop ID', 'wc_netbanx' ),
					'type'		=> 'text',
					'description'	=> __( "Once your account has been set up, NETBANX will provide you with Shop ID, used as identification in requests sent to NETBANX.", 'wc_netbanx' ),
					'default'	=> ''
				),
				'shop_key'	=> array(
					'title'		=> __( 'Shop Key', 'wc_netbanx' ),
					'type'		=> 'password',
					'description'	=> __( 'Once your account has been set up, NETBANX will provide you with Shop Key, used to digitally sign requests sent to NETBANX.', 'wc_netbanx' ),
					'default'	=> ''
				),
            
            'shop_id_test'	=> array(
					'title'		=> __( 'Shop ID Test', 'wc_netbanx' ),
					'type'		=> 'text',
					'description'	=> __( "Once your account has been set up, NETBANX will provide you with Shop ID, used as identification in requests sent to NETBANX.", 'wc_netbanx' ),
					'default'	=> ''
				),
				'shop_key_test'	=> array(
					'title'		=> __( 'Shop Key Test', 'wc_netbanx' ),
					'type'		=> 'password',
					'description'	=> __( 'Once your account has been set up, NETBANX will provide you with Shop Key, used to digitally sign requests sent to NETBANX.', 'wc_netbanx' ),
					'default'	=> ''
				),
            
				'account_num'	=> array(
					'title'		=> __( 'Merchant Account Number', 'wc_netbanx' ),
					'type'		=> 'text',
					'description'	=> __( 'This is the merchant account number. If you have more than one merchant account with the same currency, you must provide the Account Number, if not leave empty.', 'wc_netbanx' ),
					'default'	=> ''
				),
				'debug'		=> array(
					'title'		=> __( 'Debug Log', 'wc_netbanx' ),
					'type'		=> 'checkbox',
					'label'		=> __( 'Recommended: Test Mode only', 'wc_netbanx' ),
					'default'	=> 'no',
					'description'	=> __( 'Debug log will provide you with most of the data and events generated by the payment process. Logged inside <code>woocommerce/logs/netbanx.txt</code>.' ),
				)
			);

		} // End init_form_fields()

		/**
		 * Show Description in place of the payment fields
		 **/
		function payment_fields() {
			if ($this->description) echo wpautop(wptexturize($this->description));
		}

		/**
		 * Generate Netbanx form
		 **/
		function generate_netbanx_form( $order_id ) {
			global $woocommerce;

			$order = new WC_Order( $order_id );

			if ( $this->testmode == 'yes' ) {
				$nbx_url = $this->testurl;
			} else {
				$nbx_url = $this->liveurl;
			}

			// Debug log
			if ( 'yes' == $this->debug ) $this->log->add( 'netbanx', 'Generating payment form for order #' . $order_id);

			// Generate the xml request
			$xml_request = $this->generate_netbanx_xml_request($order);

			// Debug log
			if ( 'yes' == $this->debug ) $this->log->add( 'netbanx', 'Generated XML string: ' . $xml_request);

			// Generate the hashed payment request
			$netbanx_args = array(
				'shopId' => $this->shop_id,
				'encodedMessage' => base64_encode($xml_request),
				'signature' => base64_encode(hash_hmac("sha1", $xml_request, $this->shop_key, true))
			);

    
         
         
			// Debug log
			if ( 'yes' == $this->debug ) $this->log->add( 'netbanx', 'Order form parameters: ' . print_r( $netbanx_args, true ));

			$netbanx_form_array = array();

			foreach ($netbanx_args as $key => $value) {
				$netbanx_form_array[] = '<input type="hidden" name="'.esc_attr( $key ).'" value="'.esc_attr( $value ).'" />';
			}

			$woocommerce->add_inline_js('
                jQuery("body").block({
                        message: "<img src=\"'.esc_url( $woocommerce->plugin_url() ).'/assets/images/ajax-loader.gif\" alt=\"Redirecting...\" style=\"float:left; margin-right: 10px;\" />'.__('Thank you for your order. We are now redirecting you to Netbanx to make payment.', 'wc_netbanx').'",
                        overlayCSS:
                        {
                            background: "#fff",
                            opacity: 0.6
                        },
                        css: {
                            padding:        20,
                            textAlign:      "center",
                            color:          "#555",
                            border:         "3px solid #aaa",
                            backgroundColor:"#fff",
                            cursor:         "wait",
                            lineHeight:		"32px"
                        }
                    });
                jQuery("#submit_netbanx_payment_form").click();
            ');

			return '<form action="'.esc_url( $nbx_url ).'" method="post" id="netbanx_payment_form" target="_top">
                    ' . implode('', $netbanx_form_array) . '
                    <input type="submit" class="button-alt" id="submit_netbanx_payment_form" value="'.__('Pay via Netbanx', 'wc_netbanx').'" /> <a class="button cancel" href="'.esc_url( $order->get_cancel_order_url() ).'">'.__('Cancel order &amp; restore cart', 'wc_netbanx').'</a>
                </form>';

		}

		/**
		 * Generate the order xml request
		 **/
		private function generate_netbanx_xml_request( $order ) {
			global $woocommerce;
         
         
         
         
         /**
          * CREATE PROFILE
          * 
          */
         
         /* GET ID FROM LOGED USER*/

         
       global $current_user;
      get_currentuserinfo();
          
          $user_meta_token_key = "_my_token";

         $my_token = get_user_meta($current_user->ID, $user_meta_token_key, true);          
         /****************************************/         
         
         
         $my_checkout = $woocommerce->checkout(); //-> WC_Checkout
         
         
         if(empty($my_token) OR strcasecmp($my_token,"-1")==0)
         {
            if ( $this->testmode == 'yes' ) {
               $createUrl = $this->testurl_op_create_pro;
            } else {
               $createUrl = $this->liveurl_op_create_pro;
            }
            
            //$checkoutUrl = "https://checkout.test.optimalpayments.com/securePayment/op/profileCheckoutRequest.htm";

            //$secretKey must be stored in a secure location and not directly within your code.
            $secretKey = $this->shop_key;//"MlNhkT40IBKPO0Wx2j5Q8MtA8w==";
            $shopId = $this->shop_id;//"17089";

            $merchantCustomerId = $current_user->ID;//rand(); //"jim56";
            //$merchantRefNum = "ref56";

            
            $xmlDoc = "<?xml version='1.0' encoding='ISO-8859-1'?>";
            $xmlDoc .= "<createProfileRequest xmlns='www.optimalpayments.com/checkout'>";
            $xmlDoc .= "<merchantCustomerId>" . $merchantCustomerId . "</merchantCustomerId>";
            $xmlDoc .= "<billingDetails>";
            $xmlDoc .= "<firstName>".$current_user->user_firstname."</firstName>";
            $xmlDoc .= "<lastName>".$current_user->user_lastname."</lastName>";
            $xmlDoc .= "<street>".$my_checkout->get_value('billing_address_1')."</street>";
            $xmlDoc .= "<city>".$my_checkout->get_value('billing_city')."</city>";
            $xmlDoc .= "<region>".$my_checkout->get_value('billing_city')."</region>";
            $xmlDoc .= "<country>".$my_checkout->get_value('billing_country')."</country>";
            $xmlDoc .= "<zip>".$my_checkout->get_value('billing_postcode')."</zip>";
            $xmlDoc .= "<timeAtAddress>12</timeAtAddress>";
            $xmlDoc .= "</billingDetails>";
            $xmlDoc .= "<phone>".$my_checkout->get_value('billing_phone')."</phone>";
            //$xmlDoc .= "<mobilePhone>".$my_checkout->get_value('billing_mobile_phone')."</mobilePhone>";            
            $xmlDoc .= "<notificationEmail>".$current_user->user_email."</notificationEmail>";
            $xmlDoc .= "<locale><language>en</language><country>US</country></locale>";
            $xmlDoc .= "</createProfileRequest>";	
            
            
            //echo '<pre>', htmlentities($xmlDoc), '</pre>';

            $encodedMessage = base64_encode($xmlDoc);
            $signature = base64_encode(hash_hmac("sha1", $xmlDoc, $secretKey, true));

            $post_data = array(
                'shopId' => $shopId,
                'encodedMessage' => $encodedMessage,
                'signature' => $signature
            );

            $httpParams = http_build_query($post_data);

            //echo "httpParams: " . $httpParams . "<br/>";

            $ch = curl_init();
            //echo "handle: " . $ch . "<br/>";

            //set the url, number of POST vars, POST data
            curl_setopt($ch,CURLOPT_URL,$createUrl);
            curl_setopt($ch,CURLOPT_POST,true);
            curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
            curl_setopt($ch,CURLOPT_POSTFIELDS,$httpParams);
            curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);

            //execute post
            $result = curl_exec($ch);
            //echo "handle: " . $result . "<br/>";

            //close connection
            curl_close($ch);

            // extract the token
            $idx = stripos( $result, "=" );
            $token = substr( $result, ($idx + 1) );

            //echo "token: " . $token . "<br/>";
                        
            //add token to user profile
            update_user_meta($current_user->ID, $user_meta_token_key, $token);
            $my_token = get_user_meta($current_user->ID, $user_meta_token_key, true);
            
            //echo "<br>New User with token: ".$my_token;
         }
         //echo "<br>TOKEN DB =".$my_token;
         /****************************************/
			$currency = get_woocommerce_currency();
			$success_url = get_permalink( woocommerce_get_page_id('thanks') );

			// Initialize the simplexml object
			$xml = simplexml_load_string( "<?xml version='1.0' encoding='utf-8'?><profileCheckoutRequest />" );

			// Attribute to checkoutRequest
			$xml->addAttribute('xmlns', $this->posturl);

			// Order number
         $str_rand = "-".rand();
         
			$xml->addChild('merchantRefNum', $order->id.$str_rand);

			// Return URL
			$return_url_node = $xml->addChild('returnUrl');
			$return_url_node->addAttribute('page', $success_url);
			$param_key = $return_url_node->addChild('param');
			$param_key->addChild('key', $order->order_key);
			$param_key->addChild('value', $order->id);
			

			// Cancel URL
			$cancel_url_node = $xml->addChild('cancelUrl');
			$cancel_url_node->addAttribute('page', home_url('/'));
			$cancel_order_param = $cancel_url_node->addChild('param');
			$cancel_order_param->addChild('key', $order->order_key);
			$cancel_order_param->addChild('value', $order->id);
			

			if ( !empty($this->account_num) ) {
				$xml->addChild('accountNum', $this->account_num);
			}

			// payment method
			$payment_method=$xml->addChild('paymentMethod', 'CC');
			$payment_method->addAttribute('settleAuth', 'true');
			// Currency code
			$xml->addChild('currencyCode', $currency);

			// If prices include tax send the order as one item with all products in the name
			if ( get_option('woocommerce_prices_include_tax') == 'yes' ) {

				$desc = '';
				if (sizeof($order->get_items())>0) {
					foreach ($order->get_items() as $item) {

						if ($item['qty']) {

							$product = $order->get_product_from_item($item);

							$item_name  = $item['name'];

							if ( $this->is_wc_2 ) {
								$item_meta = new WC_Order_Item_Meta( $item['item_meta'] );
							} else {
								$item_meta = new order_item_meta( $item['item_meta'] );	
							}
							
							if ($meta = $item_meta->display( true, true )) {
								$item_name .= ' ('.$meta.')';
							}

							$desc .= $item['qty'] .' x '. $item_name .', ';
						}
					}
				}

				// Add the description
				$desc = substr($desc, 0, -2 );

				// Cart items
				$cart_items_node = $xml->addChild('shoppingCart');
				$cart_items_node->addChild('description', $desc);
				$cart_items_node->addChild('quantity', 1);
				$cart_items_node->addChild('amount', number_format( $order->get_total(), 2, '.', '' ));

			} else {

				// Cart Contents
				if (sizeof($order->get_items())>0) {
					foreach ($order->get_items() as $item) {
                  /*
                  print_r(array_keys($item));
                  exit;
                  */
						if ($item['qty']) {

							$product = $order->get_product_from_item($item);

							$item_name  = $item['name'];

							if ( $this->is_wc_2 ) {
								$item_meta = new WC_Order_Item_Meta( $item['item_meta'] );
							} else {
								$item_meta = new order_item_meta( $item['item_meta'] );	
							}
							
							if ($meta = $item_meta->display( true, true )) {
								$item_name .= ' ('.$meta.')';
							}
							// Cart items
							$cart_items_node = $xml->addChild('shoppingCart');
							$cart_items_node->addChild('description', $item_name);
							$cart_items_node->addChild('quantity', $item['qty']);
                     
                     //$cart_item['data']->get_price();
                     //$product->get_price()
                     
							$cart_items_node->addChild('amount',  number_format( $item['line_subtotal'] , 2, '.', '' )); ;
                     //$cart_items_node->addChild('amount',  number_format( $cart_item['data']->get_price() * $item['qty'], 2, '.', '' )); ;
						}
					}
				}

				// Add the order discount
				if ( $order->get_total_discount() > 0 ) {
					$discount_node = $xml->addChild('ancillaryFees');
					$discount_node->addChild('description', __('Discount', 'wc_netbanx'));
					$discount_node->addChild('amount', - number_format( $order->get_total_discount(), 2, '.', '' ));
				}

				// Add the order tax
				if ( $order->get_total_tax() > 0 ) {
					$tax_node = $xml->addChild('ancillaryFees');
					$tax_node->addChild('description', __('Tax', 'wc_netbanx'));
					$tax_node->addChild('amount', number_format( $order->get_total_tax(), 2, '.', '' ));
				}

				// Add the shipping parameter
				if ( $order->get_shipping() > 0 ) {
					$tax_node = $xml->addChild('ancillaryFees');
					$tax_node->addChild('description', __('Shipping', 'wc_netbanx'));
					$tax_node->addChild('amount', number_format($order->get_shipping(), 2, '.', ''));
				}

			} // End include tax if

			// Total amount
			$xml->addChild('totalAmount', number_format($order->get_total(), 2, '.', ''));

			$customer_profile_node= $xml->addChild('customerProfile');
			$customer_profile_node->addChild('merchantCustomerId',$current_user->ID);
         $customer_profile_node->addChild('customerTokenId', $my_token);
         
			$customer_profile_node->addChild('isNewCustomer', 'false');

			// Locale
			$locale_node = $xml->addChild('locale');
			$locale_node->addChild('language', 'en');
			//$locale_node->addChild('country', $woocommerce->countries->get_base_country());
         $locale_node->addChild('country', "US");

			if ( $order->billing_country == 'US' || $order->billing_country == 'CA' ) {
				$state_region = 'state';
			} else {
				$state_region = 'region';
			}
/*
			// Billing details
			$billing_node = $xml->addChild('billingDetails');
			$billing_node->addChild('firstName', $order->billing_first_name);
			$billing_node->addChild('lastName', $order->billing_last_name);
			$billing_node->addChild('street', $order->billing_address_1);
			if ( $order->billing_address_2 ) {
				$billing_node->addChild('street2', $order->billing_address_2);
			}
			$billing_node->addChild('city', $order->billing_city);
			$billing_node->addChild($state_region, empty($order->billing_state) ? $order->billing_city : $order->billing_state);
			$billing_node->addChild('country', $order->billing_country);
			$billing_node->addChild('zip', $order->billing_postcode);
			$billing_node->addChild('phone', $order->billing_phone);
			$billing_node->addChild('email', $order->billing_email);

			// Shipping details
			if ( $order->shipping_address_1 ) {
				$shipping_node = $xml->addChild('shippingDetails');
				$shipping_node->addChild('firstName', $order->shipping_first_name);
				$shipping_node->addChild('lastName', $order->shipping_last_name);
				$shipping_node->addChild('street', $order->shipping_address_1);
				if ( $order->shipping_address_2 ) {
					$shipping_node->addChild('street2', $order->shipping_address_2);
				}
				$shipping_node->addChild('city', $order->shipping_city);
				if ( !empty( $order->shipping_state ) ) {
                                        $shipping_node->addChild($state_region, $order->shipping_state);
                                }
				$shipping_node->addChild('country', $order->shipping_country);
				$shipping_node->addChild('zip', $order->shipping_postcode);
			}
*/
			// Debug log
			if ( 'yes' == $this->debug ) $this->log->add( 'netbanx', 'Object XML request: ' . print_r( $xml, true ));

         
         
			return $xml->asXML();
		}

		/**
		 * Process the payment and return the result
		 **/
		function process_payment( $order_id ) {
			global $woocommerce;

			$order = new WC_Order( $order_id );
			return array(
				'result'  => 'success',
				'redirect' => add_query_arg('order', $order->id, add_query_arg('key', $order->order_key, get_permalink( woocommerce_get_page_id('pay') )))
			);
		}

		/**
		 * receipt_page
		 **/
		function receipt_page( $order ) {

			echo '<p>'.__('Thank you for your order, please click the button below to pay with Netbanx.', 'wc_netbanx').'</p>';

			echo $this->generate_netbanx_form( $order );

		}

		/**
		 * Check Netbanx
		 **/
		function validate_request_origins() {

			// Debug log
			if ( 'yes' == $this->debug ) $this->log->add( 'netbanx', 'Validating response...' );

			$en_message = isset($_POST['encodedMessage']) ? $_POST['encodedMessage'] : '';
			$signature = isset($_POST['signature']) ? $_POST['signature'] : '';

			// Decode the received message
			$decode_message = base64_decode($en_message);

			// Generate a signature
			$gen_signature = base64_encode(hash_hmac("sha1", $decode_message, $this->shop_key, true));

			// Debug log
			if ( 'yes' == $this->debug ) {
				$this->log->add( 'netbanx', 'Decoded message: '. $decode_message );
				$this->log->add( 'netbanx', 'Generated Signature: '. $gen_signature );
			}

			if ( $gen_signature == $signature ) {
				// Debug log
				if ( 'yes' == $this->debug ) $this->log->add( 'netbanx', 'Validation passed.' );
				return true;
			} else {
				return false;
			}

		}

		/**
		 * Check for Netbanx Payment Response.
		 * Process Payment based on the Response.
		 **/
		function check_status_response_netbanx() {

			$_POST = stripslashes_deep($_POST);

			// Debug log
			if ( 'yes' == $this->debug ) $this->log->add( 'netbanx', 'Payment response received. Response is: ' . print_r( $_POST, true ) );

			if ($this->validate_request_origins()) {

				$en_message = isset($_POST['encodedMessage']) ? $_POST['encodedMessage'] : '';

				// Decode the received message
				$decoded_message = base64_decode($en_message);

				$response_xml = simplexml_load_string($decoded_message);

				// Debug log
				if ( 'yes' == $this->debug ) $this->log->add( 'netbanx', 'Response XML is: ' . print_r( $response_xml, true ) );

				$order = new WC_Order((int) $response_xml->merchantRefNum);

				// Check if order was processed already
				if ( $order->status == 'complete' || $order->status == 'processing' ) {
					$order->add_order_note( __( 'Netbanx: Received response, but order was already paid for.', 'wc_netbanx' ) );
					// Debug log
					if ( 'yes' == $this->debug ) $this->log->add( 'netbanx', 'Error: Order was already paid for. Aborting.' );
					exit;
				}

				switch ($response_xml->decision) :
				case 'ACCEPTED' :

					// Update order
					$order->add_order_note( sprintf( __( 'Netbanx Payment Completed.
                                                                Status: %s.
                                                                Transaction Confirmation Number: %s.', 'wc_netbanx' ),
							$response_xml->decision, $response_xml->confirmationNumber ) );

				// Debug log
				if ( 'yes' == $this->debug ) $this->log->add( 'netbanx', 'Payment completed.' );

				$order->payment_complete();

				break;
			case 'DECLINED' :

				// Debug log
				if ( 'yes' == $this->debug ) $this->log->add( 'netbanx', 'Payment failed.' );

				$order->update_status( 'failed' );

				// Update order
				$order->add_order_note( sprintf( __( 'Netbanx Payment Failed.
                                                            Error Code: %s.
                                                            Error Description: %s.', 'wc_netbanx' ),
						$response_xml->code, $response_xml->description ) );

				break;
			case 'ERROR' :

				// Debug log
				if ( 'yes' == $this->debug ) $this->log->add( 'netbanx', 'There was an error during the payment process. Error message: '. $response_xml->description );

				$order->update_status( 'failed' );

				$order->add_order_note( sprintf( __( 'There was an error during the payment process.
                                                            Error message: %s', 'wc_netbanx' ), $response_xml->description ) );

				break;
			default :

				// Debug log
				if ( 'yes' == $this->debug ) $this->log->add( 'netbanx', 'Unrecognized payment status was received. Payment State: '. $response_xml->decision );
				$order->update_status( 'failed' );

				$order->add_order_note( sprintf( __( 'Unrecognized payment status was received.
                                                            Payment State: %s', 'wc_netbanx' ), $response_xml->decision ) );

				break;
				endswitch;
			} else {
				// Debug log
				if ( 'yes' == $this->debug ) $this->log->add( 'netbanx', 'Response validation failed. Aborting.' );
			}
			
			// Return 204 status TODO
			header("HTTP/1.0 204 No Content");
			exit;

		}

		/**
		 * Check if SSL is forced on the checkout page.
		 **/
		function netbanx_check_ssl() {
			if ( get_option( 'woocommerce_force_ssl_checkout' ) == 'no' && $this->enabled == 'yes' && $this->testmode == 'no' ) {
				echo __( '<div class="error"><p>Netbanx is enabled for live payment, but your checkout page is not secured with SSL.
        		Your customers sensitive information is not protected.<br/>
        		Please enable the "Force secure checkout" option under Settings > General > Checkout and Accounts > Security.</p></div>' , 'wc_netbanx' );
			}
		}

	} //end netbanx class

} //end add_wc_netbanx_gateway

/**
 * Add the gateway to WooCommerce
 **/
function add_netbanx_gateway( $methods ) {
	$methods[] = 'WC_Gateway_Netbanx'; return $methods;
}

add_filter('woocommerce_payment_gateways', 'add_netbanx_gateway' );