<?php
/**
 * Single product quantity inputs
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */
global $quantity_is_hidden; //, $quantity_hidden_value;

$str_hidden="";
if($quantity_is_hidden===true)
{
   $str_hidden=" type='hidden' ";
}
/*
if(strcasecmp($quantity_hidden_value,"")!=0)
{
   $input_value = $quantity_hidden_value;
}
*/
?>



<div class="quantity"><input id="my_quantity" <?php echo $str_hidden?> name="<?php echo $input_name; ?>" data-min="<?php echo $min_value; ?>" data-max="<?php echo $max_value; ?>" value="<?php echo $input_value; ?>" size="4" title="Qty" class="input-text qty text" maxlength="12" /></div>