<?php
/**
* Sidebar Single Product
*
* @author 		WooThemes
* @package 	WooCommerce/Templates
* @version     1.6.4
*/
global $product, $post;




  $terms_regular_price = woocommerce_get_product_terms( $product->id, "pa_regular-price", "names"); 
  foreach ($terms_regular_price as $key => $value)
  {
      $regular_price = $value;
  }		           
  $terms_sale_price = woocommerce_get_product_terms( $product->id, "pa_sale-price", "names"); 
     foreach ($terms_sale_price as $key => $value)
     {
         $sale_price = $value;
     }		       

    $you_save_money =  $regular_price - $sale_price;

    if((isset($regular_price) AND strcasecmp($regular_price,"")!= 0) AND  
       isset($regular_price) AND strcasecmp($regular_price,"")!= 0) 
    {
    $save_percent = ceil((1-($sale_price/$regular_price)) * 100);    

    }
    else
    {
      $save_percent = 0;
    }  
    ?>
         <div class="content_list">
               <div class="buton_shadow_item  hide_shadow_item_sidebar">

                  <div class="title_product_shadow">
                     <?php 
                        echo $post->post_title;
                        ?>
                  </div>

                    <div class="sale_price_shadow_item">
                                
                        
                       Only <span id="regular_price_amount"><?php echo woocommerce_price($sale_price); ?></span> 
                      
                        
                      </div>
                 <a style="margin-left:10px;display:inline-block" href='<?php echo get_bloginfo("url")."/?product=".$post->post_name ?>'>
                     <button type="button" name="" value="" class="css3button3 sidebar_buton">Shop Now</button> 
                 </a>
             </div>

            <div class="shadow_item_sidebar hide_shadow_item_sidebar">



            </div>
    <li>




      <div class="price_ofert_sidebar">
         <?php echo woocommerce_price($sale_price)?> instead of <span style="text-decoration: line-through;"><?php echo woocommerce_price($regular_price)?></span>
      </div>
      <div class="title_deals_sidebar">
         <a href='<?php echo get_bloginfo("url")."/?product=".$post->post_name ?>'>
                     <?php 
                        echo $post->post_title;
                     ?>
               </a>
      </div>
      <div class="content_sidebar">
         <div class="ticket_sidebar">
            <img src="<?php echo get_bloginfo('stylesheet_directory')?>/images/ticket-sidebar-right.png">
         </div>
         <div class="price_sidebar">
            <div class="real_price_sidebar"><?php echo woocommerce_price($sale_price)?></div>
            <div class="sale_price_sidebar"><?php echo woocommerce_price($regular_price)?> </div>
         </div>
         <div class="thumbnail_sidebar">
            <a href='<?php echo get_bloginfo("url")."/?product=".$post->post_name ?>' >
                     <?php
                     //the_post_thumbnail( $product->post_id,"sidebar-feature");
                     the_post_thumbnail(array(105,70)); 
                     ?>
                     </a>
         </div>
      </div>
      <!--div>
               <a style="margin-left:10px;" href='<?php echo get_bloginfo("url")."/?product=".$post->post_name ?>'>
               <button type="button" name="" value="" class="css3button3 sidebar_buton">View</button> 
            </a>
             </div-->

   </li>	
</div>