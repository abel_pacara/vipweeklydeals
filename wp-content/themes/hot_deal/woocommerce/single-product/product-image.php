<?php
/**
 * Single Product Image
 *
 * @author WooThemes
 * @package WooCommerce/Templates
 * @version 1.6.4
 */
global $post, $woocommerce;
?>
<style>
   #nivo-slider{
      position: relative;
      width:450px;
      height:280px;
      /*background-color: #cccccc;*/
      text-align: center;
   }

   .container_main_thumbnail{
      margin-left: auto;
      margin-right: auto;
   }
</style>
<script type="text/javascript" src="<?php echo get_bloginfo( 'stylesheet_directory' ) ?>/js/jquery.nivo.slider.js"></script>
<link rel="stylesheet" href="<?php echo get_bloginfo( 'stylesheet_directory' ) ?>/css/nivo/default.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php echo get_bloginfo( 'stylesheet_directory' ) ?>/css/nivo/nivo-slider.css" type="text/css" media="screen" />
<div class="images">
   <div class="main_thumbnail">
<?php
$media_query = new WP_Query(
      array(
         'post_type'=>'attachment',
         'post_status'=>'inherit',
         'post_parent'=>$post->ID,
         'nopagging'=>true,
      )
);
/*
  echo "<br><pre>";
  print_r( $media_query );
  echo "</pre><BR>";
 */
if ( $media_query->found_posts > 1 )
{?>
      <div id="nivo-slider">
         <?php
         $loop = 0;
         $columns = apply_filters( 'woocommerce_product_thumbnails_columns', $media_query->found_posts );

         $i = 0;

         foreach ( $media_query->posts as $attachment_i )
         {
            //$image_attributes = wp_get_attachment_image_src( $attachment_i->ID, apply_filters( 'bundled_product_large_thumbnail_size', 'shop_single' ) );


            /* if($i>0)
              { */
            $style_thumb = ""; //"style='visibility: hidden; display:inline-block'";
            /* } */

            $classes = array( 'zoom' );

            if ( $loop == 0 || $loop % $columns == 0 )
               $classes[ ] = 'first';

            if ( ( $loop + 1 ) % $columns == 0 )
               $classes[ ] = 'last';

            ?>
            <a href="<?php echo wp_get_attachment_url( $attachment_i->ID )?>"
               title="<?php echo esc_attr( $attachment_i->post_title )?>"
               class="<?php echo implode( ' ', $classes )?>"
               <?php echo " ".$style_thumb?>
               >
               <?php 
               //echo wp_get_attachment_image( $attachment_i->ID,'my_single_feature');
               echo wp_get_attachment_image( $attachment_i->ID,'thumbnail');
               ?>
            </a>
            <?php
            $i++;
            $loop ++;
         }
         ?>
      </div>
      <script>
         jQuery(document).ready(function($){
            $("#slide_0").css('visibility','visible');

            $("#slide_0").ready(function(){
               $('#nivo-slider').nivoSlider({
                  effect: 'slideInLeft', // Specify sets like: 'fold,fade,sliceDown'
                  //slices: 15, // For slice animations
                  boxCols: 8, // For box animations
                  boxRows: 4, // For box animations
                  animSpeed: 300, // Slide transition speed
                  pauseTime: 4000, // How long each slide will show
                  startSlide: 0, // Set starting Slide (0 index)
                  directionNav: true, // Next & Prev navigation
                  directionNavHide: true, // Only show on hover
                  controlNav: true, // 1,2,3... navigation
                  controlNavThumbs: false, // Use thumbnails for Control Nav
                  controlNavThumbsFromRel:false, //Use image rel for thumbs
                  pauseOnHover: true, // Stop animation while hovering
                  manualAdvance: false, // Force manual transitions
                  prevText: 'Prev', // Prev directionNav text
                  nextText: 'Next', // Next directionNav text
                  randomStart: false, // Start on a random slid
                  afterChange: function(){

                  }, // Triggers before a slide transition
                  beforeChange: function(){$("#slide_0").css('visibility','hidden');},
                  slideshowEnd: function(){}, // Triggers after all slides have been shown
                  lastSlide: function(){}, // Triggers when last slide is shown
                  afterLoad: function(){


                  } // Triggers when slider has loaded
               });
            });
         });
      </script>
   <?php
}
else
{
   $post_i = $media_query->posts[ 0 ];
   //$image_attributes = wp_get_attachment_image_src( $post_i->ID, apply_filters( 'bundled_product_large_thumbnail_size', 'shop_single' ) );
   $image_attributes = wp_get_attachment_image_src( $post_i->ID, 'thumbnail' );
   
   $style_thumb = "";
   $classes = array( 'zoom' );
   foreach ( $media_query->posts as $attachment_i )
   {?>
      <a href="<?php echo wp_get_attachment_url( $attachment_i->ID )?>"
               title="<?php echo esc_attr( $attachment_i->post_title )?>"
               class="<?php echo implode( ' ', $classes )?>"
               <?php echo " ".$style_thumb?>
               >
         <img src="<?php echo $image_attributes[ 0 ]; ?>" width="<?php echo $image_attributes[ 1 ]; ?>" height="<?php echo $image_attributes[ 2 ]; ?>"/>
      </a>
   <?php 
   }
}?>
</div>
<?php //do_action('woocommerce_product_thumbnails');  ?>
</div>


