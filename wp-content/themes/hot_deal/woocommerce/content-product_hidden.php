<?php
/**
 * The template for displaying product content within loops.
 *
 * Override this template by copying it to yourtheme/woocommerce/content-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

global $product, $woocommerce_loop;
/*echo "<pre>";
print_r($product);
echo "</pre>";*/

// Store loop count we're currently on
if ( empty( $woocommerce_loop['loop'] ) )
	$woocommerce_loop['loop'] = 0;

// Store column count for displaying the grid
if ( empty( $woocommerce_loop['columns'] ) )
	$woocommerce_loop['columns'] = apply_filters( 'loop_shop_columns', 4 );

// Ensure visibilty

//$param_visible = get_query_var('visible');
$param_visible = "";
if(isset($_REQUEST['visible']))
{
   $param_visible = $_REQUEST['visible'];
}
/*
if(!($product->is_visible() OR (!$product->is_visible() AND strcasecmp($param_visible,"false")==0)))
   return;
*/

// Increase loop count
$woocommerce_loop['loop']++;









			$terms_regular_price = woocommerce_get_product_terms( $product->id, "pa_regular-price", "names"); 
        foreach ($terms_regular_price as $key => $value)
        {
            $regular_price = $value;
        }		           
		$terms_sale_price = woocommerce_get_product_terms( $product->id, "pa_sale-price", "names"); 
        foreach ($terms_sale_price as $key => $value)
        {
            $sale_price = $value;
        }		       
	    
	    $you_save_money =  $regular_price - $sale_price;
	  
	    if((isset($regular_price) AND strcasecmp($regular_price,"")!= 0) AND  
	  	    isset($regular_price) AND strcasecmp($regular_price,"")!= 0) 
	    {
	    $save_percent = ceil((1-($sale_price/$regular_price)) * 100);    

	    }
	    else
	    {
	    $save_percent = 0;
		} 

      ################################
      global $counter_post;
      $style_grid_cell = " margin_grid_cell_right";
      if($counter_post%2==0)
      {
         $style_grid_cell = " margin_grid_cell_left";
      }
      ?>





<li class="product <?php echo $style_grid_cell?> grill_deals<?php
	if ( $woocommerce_loop['loop'] % $woocommerce_loop['columns'] == 0 )
		echo 'last';
	elseif ( ( $woocommerce_loop['loop'] - 1 ) % $woocommerce_loop['columns'] == 0 )
		echo 'first';
	?>">

	<?php do_action( 'woocommerce_before_shop_loop_item' ); ?>
      <div>
         <a href="<?php the_permalink(); ?>">
            <div class="image_grill">


            <?php
               /**
                * woocommerce_before_shop_loop_item_title hook
                *
                * @hooked woocommerce_show_product_loop_sale_flash - 10
                * @hooked woocommerce_template_loop_product_thumbnail - 10
                */
                //the_post_thumbnail( $post->ID,"my_shop_catalog-feature");
               //echo get_the_post_thumbnail($post->ID, 'my_shop_catalog-feature');
               echo get_the_post_thumbnail($product->id, 'thumbnail');
               //return '<img src="'. woocommerce_placeholder_img_src() .'" alt="Placeholder" width="' . $placeholder_width . '" height="' . $placeholder_height . '" />';

               //echo get_the_post_thumbnail($post->ID);

               //do_action( 'woocommerce_before_shop_loop_item_title' ); //muestra la imagen, owner_name, location
            ?>
            </div>
         </a>
      </div>
   
		<div id="ticket_grill">
         <img src="<?php echo get_bloginfo('stylesheet_directory')?>/images/ticket.jpg" />
      </div>
		<div class="content_grill">
			
			<div class="description_grill">
               <?php
               //echo "<br>LOOP =".$woocommerce_loop['loop'];
               ?>
					<div id="title_grill_product">
						
					
						<a  href="<?php echo get_bloginfo("url")."/?product=".$post->post_name ?>'">
							<h1 style="text-align:left;">
                        <?php the_title(); ?>
							</h1>  <!-- MUESTRA TITULO DEL PRODUCTO-->
						</a>
					
						
						<!--div class="owner_website">
				          <?php 
				            $terms_website = woocommerce_get_product_terms( $product->id, "pa_owner_name", "names");                     

				            foreach ($terms_website as $key => $value) {
				                echo $value;
				            }
				           
				          ?>
				        </div-->
				    </div>	
				    </a>	
                 
			        <!--div class="location">
				        <h2>
				          <?php 
                        /*
				              $terms_location = woocommerce_get_product_terms( $product->id, "pa_location", "names");
				              foreach ($terms_location as $key => $value) {
				                echo $value;
				              }*/
				           ?>
			           </h2>
			        </div-->
			</div>

			<div class="footer_grill">
	                	
            	<div id="content_price">
            		<div class="real_price">
            			<span style="font-size: 12px">From</span> $<?php echo $sale_price?>
            		</div>
            		<div class="sale_price">
                        <!--instead of <br/-->
                           <div class="regular_price_shop">
                              $<?php echo $regular_price?>
                           </div>
                      </div>

            	</div>



            	<div id="category_product">
            		<?php
            			global $product;
						$product_category = $product->get_categories();
						//echo $cat;
								
						//print_r($cat);
						
$string = <<<XML
<?xml version='1.0'?> 
<document>
 $product_category
</document>
XML;


$data_result = simplexml_load_string($string);
$category_name = $data_result->a;
echo "<div class='".$category_name[0]."'><a>".$category_name[0]."</a></div>";
?>
            	</div>


            	
            	<div id="buton_featured">
            		<!--a style="display:inline-block" href='<?php echo get_bloginfo("url")."/?post_type=product&add-to-cart=".$product->id ?>'>
                     <?php
                     
                     if(strcasecmp($product->product_type,"variable")==0)
                     {
                        $link 	= apply_filters( 'variable_add_to_cart_url', get_permalink( $product->id ) );
                        $label 	= apply_filters( 'variable_add_to_cart_text', __('Options', 'woocommerce') );
                        ?>
                        <a
                           href="<?php echo $link?>"
                           rel="nofollow"
                           data-product_id="<?php echo $product->id?>"
                           class="buton_grilldeals_options"
                           >
                           <?php echo $label?>
                        </a>
                        <?php
                     }
                     else
                     {?>
                        <a style="display:inline-block" href='<?php echo get_bloginfo("url")."/?post_type=product&add-to-cart=".$product->id ?>'>
                           <button type="button" name="" value="" class="css3button">View</button>
                        </a>
                     <?php
                     }
                     ?>
                	</a-->
            		<a style="display:inline-block" href='<?php echo get_bloginfo("url")."/?product=".$post->post_name ?>'>
                    	<div class="button_shop_buy">Shop Now</div>
                	</a>
                	
            	</div>
	                
          	</div>
				    <!--div class="price_grill">
						<?php
							/**
							 * woocommerce_after_shop_loop_item_title hook
							 *
							 * @hooked woocommerce_template_loop_price - 10
							 */
							#do_action( 'woocommerce_after_shop_loop_item_title' );  //muestra el precio
						?>
					</div-->
				

			<!--?php 
			#do_action( 'woocommerce_after_shop_loop_item' ); //muestra boton add to cart
			?--> 
		</div>
	
</li>
<script type="text/javascript">
            jQuery(document).ready(function($){
               $("ul.products li.product").mouseover(function(){
                  $(this).addClass('product_mouseover');
               });
               $("ul.products li.product").mouseout(function(){
                  $(this).removeClass('product_mouseover');
               });

            });
         </script>