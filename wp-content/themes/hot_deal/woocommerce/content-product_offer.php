<?php
/**
 * The template for displaying product content within loops.
 *
 * Override this template by copying it to yourtheme/woocommerce/content-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

global $product, $woocommerce_loop;
/*echo "<pre>";
print_r($product);
echo "</pre>";*/

// Store loop count we're currently on
if ( empty( $woocommerce_loop['loop'] ) )
	$woocommerce_loop['loop'] = 0;

// Store column count for displaying the grid


$woocommerce_loop['columns'] =3;

/*
if ( empty( $woocommerce_loop['columns'] ) )
	$woocommerce_loop['columns'] = apply_filters( 'loop_shop_columns', 4 );
*/
// Ensure visibilty
/*if ( ! $product->is_visible() )
	return;
*/
// Increase loop count
$woocommerce_loop['loop']++;










			$terms_regular_price = woocommerce_get_product_terms( $product->id, "pa_regular-price", "names"); 
        foreach ($terms_regular_price as $key => $value)
        {
            $regular_price = $value;
        }		           
		$terms_sale_price = woocommerce_get_product_terms( $product->id, "pa_sale-price", "names"); 
        foreach ($terms_sale_price as $key => $value)
        {
            $sale_price = $value;
        }		       
	    
	    $you_save_money =  $regular_price - $sale_price;
	  
	    if((isset($regular_price) AND strcasecmp($regular_price,"")!= 0) AND  
	  	    isset($regular_price) AND strcasecmp($regular_price,"")!= 0) 
	    {
	    $save_percent = ceil((1-($sale_price/$regular_price)) * 100);    

	    }
	    else
	    {
	    $save_percent = 0;
		} 

      ################################
      global $counter_post;
      /*
      $style_grid_cell = " margin_grid_cell_right";
      if($counter_post%$woocommerce_loop['columns']==0)
      {
         $style_grid_cell = " margin_grid_cell_left";
      }*/
      ?>





<li class="product <?php echo $style_grid_cell?> grill_deals <?php
	if ( $woocommerce_loop['loop'] % $woocommerce_loop['columns'] == 0 )
		echo 'last';
	elseif ( ( $woocommerce_loop['loop'] - 1 ) % $woocommerce_loop['columns'] == 0 )
		echo 'first';
	?>">
   
   <?php   
   $offer_status = $product->get_attribute("offer-status");   
   
   if(strcasecmp($offer_status, "open")==0 OR is_admin())
   {
      ?>
      <a  href="<?php the_permalink(); ?>">
   <?php
   }?>
   
   <div>
      <?php do_action( 'woocommerce_before_shop_loop_item' ); ?>
      
            <?php               
      
            if(strcasecmp($offer_status, "close")!=0)
            {
               //echo get_the_post_thumbnail($product->id, array(295, 295));

               echo get_the_post_thumbnail($product->id, 'christmas_feature');
            }
            ?>
   </div>
   
   <div class="offer_status <?php echo "offer_status_".strtolower($offer_status)?>">
      <?php
      if(strcasecmp($offer_status, "close")!=0 OR is_admin())
      {?>
         <div class="text_day">
         <?php
         echo get_post_meta($product->id, "offer_day", true)." TH";
         ?>
         </div>
      
         <div class="christmas_description">
            <div class="christmas_title">
                  <h1 style="text-align:left;">
                     <?php the_title(); ?>
                  </h1>                 
            </div>
            <div class="christmas_sale_price">            			
               From <?php echo woocommerce_price($sale_price)?>
            </div>         
            <div class="christmas_regular_price">
               <?php echo woocommerce_price($regular_price)?>
            </div>         
         </div>
      <?php
      }
      else
      {?>
         <div class="offer_day_close">
         <?php
         echo get_post_meta($product->id, "offer_day", true)." TH";
         ?>
         </div>
         <?php
      }
      ?>
   </div>
   <?php
      if(strcasecmp($offer_status, "open")==0 OR is_admin())
   {
    ?>
    
      </a>
   <?php
   }
   ?>
         
</li>
