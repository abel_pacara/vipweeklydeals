<?php
/**
 * The Template for displaying all single products.
 *
 * Override this template by copying it to yourtheme/woocommerce/single-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */
get_header(); 
?>


<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo get_bloginfo('stylesheet_directory') ?>/js/jquery.countdown.js"></script>

<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>

<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-4715900-1");
pageTracker._initData();
pageTracker._trackPageview();
</script>

<style type="text/css">
	#page {
	    background-color: #FFFFFF;
	    overflow: hidden;
	    padding-left: 10px;
	    padding-right: 10px;
	}
	div.product, #content div.product {
	    margin-bottom: 0;
	    /*position: inherit;*/
	}
	.link_fine_print{
		font-size: 12px;
		text-decoration: none;
		color: #346AAC;
	}
	.link_fine_print:hover{
		font-size: 12px;
		text-decoration: underline;
		color: #346AAC;
	}
</style>


<style>
      
         #locked_for_lightbox{
         visibility:hidden; 
         z-index:1001; 
         background:#000000; 
         opacity:0.4; 
         filter: Alpha(Opacity=40);
         position:absolute;
         
      }

         .shading{
            opacity:0.8;
            filter: Alpha(Opacity=80);
            
         }


   
               .ligthbox_options
               {
                   background: none repeat scroll 0 0 #FFFFFF;
                   
                   padding: 8px 17px 8px 8px; 
                   position:absolute;
                   
                   /*visibility: hidden;*/
                   z-index: 1002;
                   border-radius: 25px;
                   
               }
               .arrow_left_lightbox{
                   float: left;
                   margin-left: -25px;
                   margin-top: 70px;
               }
               .content_ligthbox_options{
                float: right;
               }
          
                      
            
            .variations{
               clear: both;
               float: right;
            }

            .page{
               position: relative !important;
            }

            #prices_options{
               float: left;
               padding-top: 15px;
               width: 100%
            }
            #button_options{

            }
            .detail_prices_options{
               font-size: 11px;
               white-space:nowrap;
               text-align: left;
               padding-top: 10px;
               padding-bottom: 5px;
            }

            .price_table{
               width: auto;
            }

            .price_table td {
             padding-left: 20px;
             vertical-align: top;
             padding-top: 5px;
             padding-bottom: 5px;
            }

            .price_table_td_align_left{
               text-align: left;
            }

            .cell_price_table_align_left{
               text-align: left;
            }

            .price_table tr{
                /*border-top: 1px solid #CCCCCC;*/
                margin-bottom: 68px;
            }
            #title_ligthbox_options{
               font-size: 24px;
              /* color: #3469ab;*/
               color: #1a1a1a;
               font-weight: bold;
               padding-left: 15px;
               padding-top: 20px;
            }
            .title_deal_options{
               font-size: 20px;
               color: #3469ab;
               font-weight: bold;
               text-align: left;
            }
            .cell_price_table{
               font-style: italic;
               color:#A3A3A3;
            }
            .cell_price_table_strikethrough{
               font-style:normal;
               color:#A3A3A3;
               text-decoration: line-through;
            }

            .quantity_bought_options{
               font-size: 14px;
               white-space:nowrap;
            }


            .radio_styled {
                border: 2px solid #C6C6C6;
                color: #666666;
                display: inline-block;
                font-weight: normal;
                margin-right: 10px;
                margin-top: 0px;
                padding-top: 0px;
                padding-bottom: 1px;
                padding-left: 1px;
                padding-right: 1px;
                font-size: 14px;
                margin-bottom: 10px;
                cursor: pointer;
            }
            /*
            .radio_styled:hover{
               border: 2px solid #000000;
               color: #000000;
            }*/
            
            .text_sold_out{
               font-size: 10px;
               font-style: italic;
               color: #000000;
            }

            .radio_styled_disabled {
                border: 2px solid #C6C6C6;
                color: #6c6c6c;
                display: inline-block;
                font-weight: normal;
                margin-right: 10px;
                margin-top: 0px;
                padding-top: 0px;
                padding-bottom: 1px;
                padding-left: 1px;
                padding-right: 1px;
                font-size: 14px;
                margin-bottom: 10px;
                cursor: auto;
                opacity: 0.5;
            }
            

            .radio_styled .radio_styled_text_content,
            .radio_styled_disabled .radio_styled_text_content{
               padding-top: 3px;
               padding-bottom: 3px;
               padding-left: 12px;
               padding-right: 12px;
            }

             .radio_styled_selected{
                border:2px solid #000000;
                font-size: 14px;
                color: #000000;
             }
             .radio_styled_content{
                padding-top: 11px;
                padding-bottom: 11px;
                padding-left: 11px;
                padding-right: 11px;
             }
             .price_table .tr_message{
                border-top:none;
             }

             #message_alert{
                background-color: #ffcccc;                         
                border-color: 1px solid red;
                line-height: 20px;
             }
             .size_chart{
                padding-left: 0px;
                padding-right: 0px;
                padding-top:15px;
                padding-bottom: 15px;
                text-align: left;
             }
             .size_chart a, .size_chart a:hover{
                font-size: 10px;
                text-decoration: none;
                color: #124170;
                font-weight: bold;
             }
             .price_table .attribute_label{
                 padding-left: 20px;
                 vertical-align: middle;
                 padding-top: 10px;
                 padding-bottom: 5px;
                 white-space: nowrap;
                 vertical-align: top;
             }
             .row_odd{
                background-color: #F7F7F7;
             }
             .thumb_variation{
                display: inline-block;
             }


          .lightbox_close{
             background-color: #346AAC;
             border: 2px solid #FFFFFF;
             border-radius: 20px 20px 20px 20px;
             color: #FFFFFF;
             height: 20px;
             position: absolute;
             right: 0;
             top: -5px;
             width: 20px;
             cursor: pointer;
             padding-left: 2px;
              padding-right: 2px;
              padding-top: 3px;
              text-align: center;
          }
          .lightbox_close_control{
             
          }
          
          button.button, button.button:hover,
          .button_css3button {
            font-family: "Lucida Sans","Lucida Grande",Garuda,sans-serif;
             font-size: 13px;
            color: #ffffff !important;
            background-color: #346aac !important;

            border-radius: 25px;
            border: 1px solid #124170;

            white-space: nowrap;
            margin-bottom: 5px;
            padding: 5px 19px;
            cursor: pointer;
            font-weight: bold;

            display: inline-block;
         }
         .container_button_css3{
            /*padding-left: 5px;*/
         }
         .button_buy_disable{
            cursor: auto;
         }
         #submit_process{
            height: 50px; 
            width: 50px; 
            margin-left: auto;
            margin-right: auto;
            background-image: url(<?php echo get_bloginfo('stylesheet_directory')?>/images/loader.gif);
            background-repeat: no-repeat;
            visibility: hidden;
         }
   </style>

   
   
   <script type="text/javascript">
   function resize_shadow()
   {
      var shading = $('#locked_for_lightbox');

      var wrapper = $('#wrapper');

      $(shading).height($(wrapper).height());
      $(shading).width($(wrapper).width());                   

      $(shading).css('top','0px');
      $(shading).css('left','0px');

   }


   function show_lightbox(){                   
      resize_shadow();
      var shading = $('#locked_for_lightbox');
      $(shading).css('visibility','visible');

      var offset = $('#content_detail').offset();


      $("#ligthbox_options").css('top', offset.top-57);
      $("#ligthbox_options").css('left', offset.left-20);

      $("#ligthbox_options").css('visibility','visible');
   }
   function hidden_lightbox(){
      var shading = $('#locked_for_lightbox');
      $(shading).css('visibility','hidden');
      $("#ligthbox_options").css('visibility','hidden');                   
   }
   function show_progress_bar()
   {
      $("#submit_process").css('visibility','visible');
   }



   jQuery(document).ready(function($){
     resize_shadow();
   });



   /********************************************/

   jQuery("#ligthbox_options").ready(function($){


     $("#run_lightbox").click(function(){
        show_lightbox();                     
     });
     $("#locked_for_lightbox").click(function(){
        hidden_lightbox();
     });


     /*#####################################################################*/                     
     

     $(".radio_styled").click(function(){
         var attr_name = $(this).attr('name');                     
         var attr_value = $(this).attr('value');

         /*
         $('.radio_styled_'+attr_name).removeClass('radio_styled_selected');
         $(this).addClass('radio_styled_selected');
         */

         $("#"+attr_name+" option[value='" + attr_value+ "']").attr('selected',true);

         var variation_id = $('#variation_id_'+attr_value).attr('value');

         $("input[name='variation_id']").attr('value',variation_id);
     });

     /*#####################################################################*/                     
     $(".button_buy_now_variable").click(function(){                     
        var value_buy_quantity = 1;//$(this).attr('value');
        /*#--------------------------------------------------------*/
        $(this).attr('disabled','yes');
        $(this).click(true);
        /*-------------------------------------------------*/
        var attr_name = $(this).attr('name');                     

        if(attr_name !=null && attr_name!='')
        {
           var attr_value = $(this).attr('value');                     
           var variation_names = attr_name.split("|");
           var variation_values = attr_value.split("|");

           for(i=0;i<variation_names.length;i++)
           {
              if(variation_names[i]!='')
              {
                 $("#"+variation_names[i]+" option[value='" + variation_values[i] + "']").attr('selected',true);
              }
           }
        }
        else
        {
           var array_radio_styles = $(".radio_styled");
           var is_checked=false;
           var quantities = $(".radio_styled input[name='quantity']");
           var count_quantities =0;
           
           for(j=0;j<quantities.length;j++)
           {
              var variation_quantity = $(quantities[j]).attr('value');
              
              if(parseInt(variation_quantity)>0)
              {
                 $(this).attr('disabled','disabled');                 
                 $(this).click(false);
                 is_checked = true;
                 show_progress_bar();
                 count_quantities=1;
                 
                 break;
              }
           }
           if(!is_checked)
           {
               $("#has_errors").attr("value",1);
               $("#message_alert").html("Please enter some amount");
           }
           else
           {
               setTimeout(function(){
                     var quantities = $(".radio_styled input[name='quantity']");
                     for(i=0;i<quantities.length;i++)
                     {
                        var variation_quantity = $(quantities[i]).attr('value');
                        var variation_id = $(quantities[i]).attr('variation_id');
                        var variation_slug = $(quantities[i]).attr('variation_slug');
                        var product_slug = $(quantities[i]).attr('product_slug');
                        var product_id = $(quantities[i]).attr('product_id');
                        var attribute_key = $(quantities[i]).attr('attribute_key');


                        if(parseInt(variation_quantity)>0)
                        {
                           /*$("#submit_process").css('visibility','visible').ready(function(){*/
                           var data = { variation_id: variation_id, product_id: product_id, quantity: variation_quantity};
                           data[attribute_key] = variation_slug;
                           var url = '<?php echo site_url()?>/?product='+product_slug+'&add-to-cart='+product_id;

                           $.ajax({
                                   type: "POST",
                                   url: url,
                                   data: data,
                                   async:false
                           });
                        }
                     }
                     
                     /*****************************************************************/
                     /*
                     var url = '<?php echo site_url()?>/?product=arm-band-strap-case-for-iphone-4-4s-3g-3gs-ipod-touch&add-to-cart=2890';
                     var data = {quantity: 2};                 
                     $.ajax({
                              type: "POST",
                              url: url,
                              data: data,
                              async:false
                           });
                     */
                     /*****************************************************************/
               
                     $("#has_errors").attr("value",0);
                     location.href ='<?php echo get_permalink(6)?>';
        
               },3000);
           }
           //jQuery.post('http://localhost/test_woo/?product=jewe1&add-to-cart=variation&product_id=16', { variation_id: 40, quantity: 3, attribute_pa_color: "blanco" } );
           
        }
        
         return;
        
        //$("#variable_form").submit();
     });
     /*#####################################################################*/                     
     $(".button_buy_now_simple").click(function(){

        var value_buy_quantity = $(this).attr('value');                     
        $('#my_quantity').attr('value',value_buy_quantity);

        var has_errors = $("#has_errors").attr("value");

        if(has_errors=='0')
        {
           //hidden_lightbox();
           show_progress_bar();
        }
        $("#simple_form").submit();
     });

     $(".button_buy_now_bundle").click(function(){
        if($('.bundle_wrap').css('display')=='block')
        {
           $(".bundle_form").submit();
        }        
     });

   });
   </script>
 
   
   
   
   
 
<?php
	/**
	 * woocommerce_before_main_content hook
	 *
	 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
	 * @hooked woocommerce_breadcrumb - 20
	 */
	#do_action('woocommerce_before_main_content');
?>
<?php while ( have_posts() ) { the_post(); 
	global $product, $post;
	/*echo "<pre>";
	print_r($product);
	echo "<pre>";*/
   
   
   
   
$terms_date_expiration = woocommerce_get_product_terms( $product->id, "pa_date-expiration", "names");

foreach ($terms_date_expiration as $key => $value) 
{
  $date_expiration = $value;
}
$date_to = $date_expiration;

if(isset($date_to) AND strcasecmp( $date_to, "")!=0)
{
   
}
else
{
   //$ts_date_from = $product->sale_price_dates_from;
   $ts_date_to = $product->sale_price_dates_to;
   /*
   if(isset($ts_date_from) AND strcasecmp($ts_date_from, "")!=0)
       {
           $date_from = date("Y-m-d",$ts_date_from);
       }*/
   if(isset($ts_date_to) AND strcasecmp($ts_date_to, "")!=0)
   {
     $date_to = date("Y-m-d",$ts_date_to);
   }
}


$count_stock_simple = get_post_meta($product->id, '_stock', true);


$array_available_variations = null;
$attributes = $array_variation_attributes = null;
if(strcasecmp($product->product_type,"variable")==0)
{
   #---------------------------------------------------------------------------------------
   //$array_attributes  = $product->get_attributes();
   $array_available_variations  = $product->get_available_variations();
   //$array_variation_default_attributes  = $product->get_variation_default_attributes();
   $attributes = $array_variation_attributes  = $product->get_variation_attributes();
}
else 
{
   $attributes = $product->get_attributes();
}



#---------------------------------------------------------------------------------------
if(isset($date_to) AND strcasecmp( trim($date_to), "")!=0)
{
   
   #-------------------------------------------------------------------
   global $wpdb;

   $date_ts_product_expiration = strtotime( $date_to );
   $sql_date_time = "SELECT now() AS date_ts_now;";
   $system_date_time = $wpdb->get_row($sql_date_time);

   $date_ts_now = strtotime( $system_date_time->date_ts_now );
   #-------------------------------------------------------------------  
}   

  $terms_regular_price = woocommerce_get_product_terms( $product->id, "pa_regular-price", "names"); 
  foreach ($terms_regular_price as $key => $value)
  {
      $regular_price = $value;
  }		           
  $terms_sale_price = woocommerce_get_product_terms( $product->id, "pa_sale-price", "names"); 
  foreach ($terms_sale_price as $key => $value)
  {
      $sale_price = $value;
  }		       

 $you_save_money =  $regular_price - $sale_price;

 if((isset($regular_price) AND strcasecmp($regular_price,"")!= 0) AND  
    isset($regular_price) AND strcasecmp($regular_price,"")!= 0) 
 {
   $save_percent = ceil((1-($sale_price/$regular_price)) * 100);    
 }
 else
 {
   $save_percent = 0;
 }   
 ?>

<div id="details">
	<div id="title_featured_deal"> 
    	<?php 
            $terms_website = woocommerce_get_product_terms( $product->id, "pa_owner_name", "names");                     
            foreach ($terms_website as $key => $value) 
            {
                echo $value;
            }		           
        ?>
    </div>
	<div id="title_detail">
	   <h1 class="page-title">
	   		<?php 
            
            //save buffer
            ob_start();            
            the_title(); 
            $product_title = ob_get_clean();
            
            echo $product_title;
            ?>
	 	</h1>
	</div>

	<div class="title_short_description">
      <?php echo nl2br($post->post_excerpt);?>
	</div>
	<div id="page_detail">	
   	  	

        <div id="content_detail">
            <?php
            $str_bundle = "";
            if(strcasecmp($product->product_type,"bundle")==0)
            {
               ob_start();
               do_action( 'woocommerce_before_single_product' );
               do_action( 'woocommerce_single_product_summary' );
               $str_bundle = ob_get_clean();
            }
            ?>
	       	<div id="thumb_detail">
	       		<?php woocommerce_get_template_part( 'content', 'single-product' ); ?>
	       	</div>
                  <?php
                  
                  
                  $all_options = array();
                  $all_options_slugs = array();
                  ?>
                   
                  <?php 
                  $loop = 0; 
                  foreach ( $attributes as $name => $options ) 
                  { 
                      $loop++; 
                      if ( is_array( $options ) ) 
                      {
                        // Get terms if this is a taxonomy - ordered
                        if ( taxonomy_exists( sanitize_title( $name ) ) ) 
                        {

                           $terms = get_terms( sanitize_title($name), array('menu_order' => 'ASC') );


                           foreach ( $terms as $term ) {
                              if ( ! in_array( $term->slug, $options ) ) continue;
                              $all_options[$name][] = apply_filters( 'woocommerce_variation_option_name', $term->name );
                              $all_options_slugs[$name][] = $term->slug;
                           }
                        } 
                        else 
                        {
                           foreach ( $options as $option )
                           {
                              $all_options[$name][] = apply_filters( 'woocommerce_variation_option_name', $option );
                              $all_options_slugs[$name][] = apply_filters( 'woocommerce_variation_option_name', $option );
                           }
                        }
                      }
                   }
                   #--------------------------------------------------------------------------------
                     
                   #--------------------------------------------------------------------------------
                    $columns_order = array();                    
                    $options_quantity = 1;

                    foreach ($attributes as $key=>$attribute) 
                    {  
                          $count_options = 0;
                          foreach ($attribute as $attr_key => $attr_value)
                          {
                             $count_options++;
                             #------------------------------------------------------------------
                          }
                       
                          $options_quantity = $options_quantity * $count_options;
                          
                          //$all_options[$attribute['name']] = $options;                           
                          $columns_order[$key] = "ASC";
                       //}
                    }
                    
                    ?>
                   
             

       </div>      
      <?php
      
      ob_start();
      ?>
      
                   
               <div id="content_ligthbox_options">    
               <div id="title_ligthbox_options">
                  Choose your deal:
               </div>
                  <?php
                     //if(strcasecmp($product->type, $sql_date_time))
                  ?>
                    <div id="description_lightbox_options">
                       <div id="prices_options">
                          <table class="price_table">
                             <?php  
                             $dynamic_pricing = get_post_meta($product->id, '_pricing_rules');                             
                             
                             if($options_quantity>1)
                             {
                                ?>
                                <tr class="tr_message">
                                   <td colspan="2">        
                                      <div id="message_alert"></div>
                                   </td>
                                </tr>
                                <?php
                                $str_all_attributes = "";
                                $count_variable_stocks=0;
                                foreach($all_options AS $attribute_key=>$attribute_values)
                                {                                   
                                   list(,$attribute_name) = explode("pa_", $attribute_key);
                                   $str_all_attributes = $str_all_attributes."|".$attribute_key;
                                   ?>
                                   <tr>
                                      <td class="title_price_table" style="white-space: nowrap">Select <?php echo ucfirst(strtolower($attribute_name));?>:</td>
                                      <td>
                                         <div style="text-align:left">
                                         <?php
                                         foreach($attribute_values AS $key=>$value)
                                         {
                                            $buff_thumb_variation ="";
                                            $is_thumb = false;
                                            $available_stock = 1;
                                            
                                            $variation_id_selected = null;
                                            $variation_stock = 0;
                                            
                                            foreach($array_available_variations AS $variation_key => $variation_value)
                                            {
                                               if(strcasecmp($all_options_slugs[$attribute_key][$key],$variation_value['attributes']['attribute_'.$attribute_key])==0)
                                               {
                                                   if(intval($variation_value['max_qty'])<=0)
                                                   {
                                                      $available_stock = 0;
                                                   }
                                                   else
                                                   {
                                                      $count_variable_stocks++;
                                                   }
                                                   ob_start();
                                                   ?>
                                                   <div class="thumb_variation">
                                                         <?php
                                                         echo get_the_post_thumbnail($variation_value['variation_id'], array(105,70));                                                                                                                            
                                                         ?>
                                                   </div>
                                                   <div id="variation_id_<?php echo $all_options_slugs[$attribute_key][$key]?>" value="<?php echo $variation_value['variation_id']?>" style="visibility: hidden"></div>
                                                   
                                                   <?php
                                                   $is_thumb = true;
                                                   $buff_thumb_variation = ob_get_clean();
                                                   
                                                   $variation_id_selected = $variation_value['variation_id'];
                                                   
                                                   $variation_stock = $variation_value['max_qty'];
                                                   break;
                                               }
                                            }
                                            
                                            $style_radio = " radio_styled "; 
                                            $text_sold_out = "";
                                            if($available_stock==0)
                                            {
                                               $style_radio = " radio_styled_disabled "; 
                                               $text_sold_out = "<br/><span class='text_sold_out'>Sold out</span>";
                                            }
                                            ?>
                                               <span name="<?php echo strtolower($attribute_key)?>" 
                                                     class="<?php echo $style_radio?> radio_styled_<?php echo strtolower($attribute_key)?>"                                                      
                                                     
                                                     id="<?php echo $all_options_slugs[$attribute_key][$key];?>"
                                                     value="<?php 
                                                     //echo strtolower($value)
                                                     echo $all_options_slugs[$attribute_key][$key];
                                                     ?>">
                                                      <?php 
                                                      
                                                      if(strcasecmp($attribute_key,"pa_color")!=0)
                                                      {?>
                                                         <div>
                                                            <div class="radio_styled_text_content" style="text-align: center">
                                                               <?php  
                                                               if($is_thumb===true)                                                               
                                                               {
                                                                  echo $buff_thumb_variation;
                                                               }
                                                               ?>
                                                               <div>
                                                                  <?php
                                                                  echo $value.$text_sold_out; 
                                                                  ?>
                                                               </div>
                                                            </div>                                                            
                                                         </div>
                                                         <?php
                                                      }
                                                      else
                                                      {?>                                                      
                                                         <div class="radio_styled_content"
                                                              title="<?php echo $value?>"
                                                              alt="<?php echo $value?>"                                                              
                                                              style="background-color:<?php echo $value?>"
                                                            >
                                                            <?php
                                                            echo $value.$text_sold_out; 
                                                            ?>
                                                         </div>
                                                         <?php
                                                      }?>
                                                  <!--div class="quantity buttons_added"-->
                                                  <div class="quantity buttons_added">
                                                      <input type="button" class="minus" value="-"
                                                             <?php
                                                             if($variation_value['max_qty']<=0)
                                                             {
                                                                echo " disabled='yes' ";
                                                             }
                                                             ?>
                                                             >               
                                                      <input maxlength="12" class="input-text qty text" 
                                                             title="Qty" size="4" 
                                                             value="0"
                                                             <?php
                                                             if($variation_value['max_qty']<=0)
                                                             {
                                                                echo " disabled='yes' ";
                                                             }
                                                             ?>
                                                             add_to_cart_url="<?php echo $product->add_to_cart_url()?>"
                                                             attribute_key="attribute_<?php echo $attribute_key?>"
                                                             product_id="<?php echo $product->id?>"
                                                             product_slug="<?php echo $post->post_name ?>"
                                                             variation_slug="<?php echo $all_options_slugs[$attribute_key][$key]?>"
                                                             variation_id="<?php echo $variation_id_selected;?>"
                                                             id="quantity_<?php echo $variation_id_selected;?>"
                                                             data-max="10" data-min="0" name="quantity">
                                                      <input type="button" class="plus" value="+"
                                                             <?php
                                                             if($variation_value['max_qty']<=0)
                                                             {
                                                                echo " disabled='yes' ";
                                                             }
                                                             ?>
                                                             >
                                                   </div>
                                               </span>
                                               
                                              <?php
                                            
                                             //$index_variation++;
                                         }
                                         if(strcasecmp($attribute_key,"pa_size")==0)
                                         {?>
                                             <!--div class="size_chart">
                                                <a href="">
                                                   SIZE CHART
                                                </a>
                                             </div-->
                                         <?php
                                         }
                                         ?>
                                         </div>
                                      </td>
                                   </tr>                                   
                                   <?php
                                   
                                }
                                ?>
                                 <tr>
                                    <td colspan="2">
                                       <div style="text-align: center; width: 100%">
                                              <span class="button_css3button 
                                                 <?php
                                                 if($count_variable_stocks>0)
                                                 {
                                                    echo " bg_buy button_buy_now_variable button_buy_now_simple ";
                                                 }
                                                 else 
                                                 {
                                                     echo " lightbox_close_control ";
                                                 }
                                                 ?>
                                                 "
                                                value="1"
                                                style="margin-left: auto; margin-right: auto;" 
                                                id="button_options">                                                   
                                              <?php
                                              if($count_variable_stocks<=0)
                                              {?>
                                                 Sold out <span style="font-style:italic; font-size:10px">(close window)</span>
                                              <?php
                                              }
                                              else
                                              {?>
                                                 Buy Now
                                              <?php
                                              }
                                              ?>     
                                              </span>
                                        </div>
                                    </td>
                                 </tr>
                                 <?php
                                
                             }
                             #---------------------------------------------------------------------------------------------------------------------------------------
                             
                             ?>
                                   
                             <?php
                             #--------------------------------------------------------------------------------------------------
                             if($options_quantity<=1)
                             {
                                $combinations = cartesian($all_options);
                                $combinations = naturalSort2D($combinations);                             
                              
                                foreach($combinations AS $key_=>$value_)
                                {
                                   $str_combination = "";
                                   $str_combination_key = "";
                                   $str_combination_value = "";
                                   foreach($value_ AS $key=>$value)
                                   {
                                      if(  strcasecmp( $str_combination,"")!=0)
                                      {
                                         $str_combination .= ", ";                                          
                                      }
                                      $str_combination .= $value;
                                      $str_combination_value .= $value."|";
                                      $str_combination_key .= $key."|";
                                   }?>
                                   <tr>
                                      <td class="cell_price_table_align_left"><span style="font-weight:bold;"><?php echo $product_title." "?></span><?php echo $str_combination ?></td>
                                      <td> 
                                         <div style="text-align: center; width: 100%">
                                            <span value="<?php echo strtolower($str_combination_value);?>" name="<?php echo $str_combination_key?>" 
                                                  class="bg_buy button_buy_now_variable button_css3button" id="button_options" >		       	  	
                                               Buy Now
                                            </span>
                                         </div>
                                      </td>
                                   </tr>
                                <?php                                    
                                }
                             }
                             ?>
                             <tr style="visibility:hidden; border: none">
                                <td>
                                   <div id="all_attributes" value="<?php echo $str_all_attributes ?>" style="visibility:hidden;"></div>
                                   <div id="has_errors" value="0" style="visibility:hidden;"></div>
                                </td>
                             </tr>
                             <tr>
                                <td colspan="2" style="text-align: center">
                                   <div id="submit_process"></div>
                                </td>
                             </tr>
                          </table>
                       </div>
                    </div>
              </div>
              <?php
              $light_box_content = ob_get_clean();
              
      
              ?>
         <div id="sidebar_detail">
   	  		<div id="ticket_detail">
               <img src="<?php echo get_bloginfo('stylesheet_directory')?>/images/buy_detail.png">
            </div>
            <div class="sidebar_price">	
            	<div class="amount_price_sd">
                 <div id="buton_sidebar_detail">
                 <?php
                 $price_comment="";
                 $terms_price_comment = woocommerce_get_product_terms( $product->id, "pa_price_comment", "names");                     
                  foreach ($terms_price_comment as $key => $value) 
                  {
                     if(  strcasecmp( trim($value),"" )!=0)
                     {
                        $price_comment = $value;
                        break;
                     }
                  }
                 
                 if( strcasecmp($product->product_type,"variable")==0 AND !$product->is_in_stock() )
                 {?>
                     <div class="bg_buy" id="run_lightbox" value="1">                
                        <span style="padding-left:5px;">
                           <div class="only_price_sidebar">
                              From <br/> <span id="regular_price_amount"><?php echo woocommerce_price($sale_price); ?></span> 
                             <br/><span><?php echo $price_comment?></span>
                           </div>
                          <button class="button_buy_sidebar" 
                                  value="(variable && product_expiration<date_now) || (variable && count_stock<=0)" 
                                  name="" type="button">Sold Out</button>         
                        </span>              
                     </div> 
                    <?php
                 }
                 else if(strcasecmp($product->product_type,"simple")==0 AND !$product->is_in_stock() )
                 //else if($date_ts_product_expiration<$date_ts_now)
                 {?>
                    <div>                
                        <span style="padding-left:5px;">
                           <div class="only_price_sidebar">
                             From <br/> <span id="regular_price_amount"><?php echo woocommerce_price($sale_price); ?></span> 
                             <br/><span><?php echo $price_comment?></span>
                           </div>
                          <button class="button_buy_sidebar button_buy_disable" value="" name="" type="button">Sold out</button>         
                        </span>              
                     </div> 
                 <?php
                 }
                 else if( strcasecmp($product->product_type,"variable")==0 AND $product->is_in_stock()) // OR isset($dynamic_pricing[0]))
                 {?>
                     <div id="run_lightbox" class="bg_buy"  value="1">                
                        <span style="padding-left:5px;">
                           <div class="only_price_sidebar">
                             From <br/> <span id="regular_price_amount"><?php echo woocommerce_price($sale_price); ?></span> 
                             <br/><span><?php echo $price_comment?></span>
                           </div>
                          <button class="button_buy_sidebar" 
                                  value="variable && count(all_options)>0" 
                                  name="" type="button">Buy Now</button>         
                        </span>              
                     </div> 
                    <?php
                 }
                 else if(strcasecmp($product->product_type,"bundle")==0)
                 {?>
                    <div id="run_lightbox" class="bg_buy" value="1">                
                        <span style="padding-left:5px;">
                           <div class="only_price_sidebar">
                             From <br/> <span id="regular_price_amount"><?php echo woocommerce_price($sale_price); ?></span> 
                             <br/><span><?php echo $price_comment?></span>
                           </div>
                          <button class="button_buy_sidebar" 
                                  value="bundle" 
                                  name="" type="button">Buy Now</button>         
                        </span>              
                     </div> 
                 <?php
                 }
                 else //$product->is_in_stock()
                 {?>
                    <div class="bg_buy button_buy_now_simple" value="1">                
                        <span style="padding-left:5px;">
                           <div class="only_price_sidebar">
                             From <br/> <span id="regular_price_amount"><?php echo woocommerce_price($sale_price); ?></span> 
                             <br/><span><?php echo $price_comment?></span>
                           </div>
                          <button class="button_buy_sidebar" 
                                  value="simple" 
                                  name="" type="button">Buy Now</button>         
                        </span>              
                     </div> 
                    <?php
                 }
                 ?>
            </div>


            

            </div>
            
            <div class="price_ofert">
               <div class="you_save">You save up to <?php echo $save_percent?>%!! </div>
            </div>
            
            </div>	        
            
            
           
            
            
            
            
            
            
             <?php
            
            $args_orders = array(
					'post_type' => 'shop_order',
					'orderby'=>'post_date',
					'order'=>'DESC',
               'nopaging'=>1,
               'post_status'=>'publish',
               'fields'=>'ids',
               'tax_query'=>array(
                     array(
                        'taxonomy'=>'shop_order_status',
                        'terms'=>array( 'completed', 'processing' ),
                        'field'=>'slug',
                        'operator'=>'IN'
                     )
                  )
					);
   
               
               
               $query_orders = new WP_Query( $args_orders );
               
               $my_orders = $query_orders->posts;               
               
					$quantity_bought=0;

					//for($i=0; $i<count($my_orders);$i++)
               foreach($my_orders AS $my_order_key=>$my_order_id)
					{
                  
                     #---------------------------------------------------------------------------------
                     
                     $my_order = new WC_Order($my_order_id);
                     
                     //$order_items = (array) maybe_unserialize( get_post_meta($my_orders[$i]->ID, '_woocommerce_order_itemmeta', true) );
                     $order_items = $my_order->get_items();
                     /*
                     echo "<pre>";
                     print_r($order_items);
                     echo "</pre>";*/

                     if(strcasecmp($product->product_type,"variable")==0)
                     {
                        foreach($array_available_variations AS $variaton_key=>$variation_value)
                        {  
                           foreach($order_items AS $key=>$item)
                           {
                              if(strcasecmp($item['variation_id'], $variation_value['variation_id'])==0)
                              {
                                 $quantity_bought += $item['qty'];                                 
                              }
                           }
                        }
                     }
                     else
                     {
                        foreach($order_items AS $key=>$item)
                        {
                           if(strcasecmp($item['product_id'],$product->id)==0)
                           {
                              $quantity_bought += $item['qty'];
                           }
                        }
                     }
                  
					}
            ?>
            
            
            
            <div class="bought">
                <?php
               
               
               
               $terms_sold = woocommerce_get_product_terms( $product->id, "pa_sold", "names"); 
               $sold =0 ;
                 foreach ($terms_sold as $key => $value)
                 {
                     $sold = $value;
                 }
               
					echo "THIS DEAL IS ON! <br/>".($quantity_bought+$sold)." Offers sold so far";
               ?>
            </div>
            
            
            
            
            
            
            
            
            
            
            <div class="timer">
                <div id="time_to_buy">
                	TIME LEFT TO BUY
                </div>
                <div id="defaultCountdown" ></div>
            </div>
            
             <div id="color_product">
            	<div class="summary">
					<?php
						/**
						 * woocommerce_single_product_summary hook
						 *
						 * @hooked woocommerce_template_single_title - 5
						 * @hooked woocommerce_template_single_price - 10
						 * @hooked woocommerce_template_single_excerpt - 20
						 * @hooked woocommerce_template_single_add_to_cart - 30
						 * @hooked woocommerce_template_single_meta - 40
						 * @hooked woocommerce_template_single_sharing - 50
						 */
                  global $quantity_is_hidden, $quantity_hidden_value;
                  $quantity_is_hidden=true;
                  $quantity_hidden_value = 2;
                  
                  if(strcasecmp($product->product_type,"bundle")!=0)
                  {
                     do_action( 'woocommerce_single_product_summary' );
                  }
					?>
                  
               </div><!-- .summary-->
          	</div><!-- #color_product-->
            
       </div>        <!--END sibedar_detail --> 
         


<?php



list($expire_year, $expire_month, $expire_day) = explode("-", $date_to);

if(isset($date_to) AND strcasecmp( trim($date_to), "")!=0)
{
   
   if($date_ts_product_expiration<$date_ts_now)
   {
      //echo "Expiration Date";
   }
   
    ?>
   <script type="text/javascript">
       $(function () {
          var date_from = new Date();
           //var date_to = new Date(2012,12-1,28);
           var date_to = new Date(<?php echo $expire_year?>,<?php echo $expire_month?>-1,<?php echo $expire_day?>);
             /*
                             $('#defaultCountdown').countdown(
                               {
                                  until: liftoffTime, 
                                  compact: true, 
                               description: ''
                         });
             */
           $('#defaultCountdown').countdown(
           {
               until: date_to,  timezone: <?php echo date("O")?>,
               compact: true, 
               labels: ['Years', 'Months', 'Weeks', 'Days', 'Hrs', 'Mins', 'Secs']
           }
         );
       });
   </script>
<?php   
}
?>

	</div><!-- END #page_detail-->

	<?php
	if (isset($post->post_content) AND strcasecmp(trim($post->post_content), "")!=0) 
		{
		?>
	
	<div class="page_detail_owner">
		<div id="description_owner">			
		    <?php the_content();?>
		</div>
		
	</div>
	<!--div id="owner_location">
			<div id="title_owner_location">
				<h1><?php the_title(); ?></h1>
				<span class="content">
					<?php 
		            $terms_website = woocommerce_get_product_terms( $product->id, "pa_owner_name", "names");                     
		            foreach ($terms_website as $key => $value) 
		            {
		                echo $value;
		            }		           
		          ?>
				</span>
			</div>
			<div>
				<?php
					ob_start();
				//wpgeo_post_map( $product->id );
				?>
					<?php
						$str_buffer = ob_get_clean();
						if (isset($str_buffer) AND strcasecmp(trim($str_buffer), "")!=0) 
						{
							echo $str_buffer;
						?>
							<a href="<?php echo wpgeo_map_link( $args = array("post_id"=>$product->id) );?>" target="_blank">
								View full size
							</a>
					<?php
						}
					?>
			</div>
			<div id="content_location">
				<h2>
					<?php 
		                $terms_location = woocommerce_get_product_terms( $product->id, "pa_location", "names");
		                foreach ($terms_location as $key => $value) 
		                {
		                	echo $value;
		                }
		           ?>
				</h2>
				<span class="content">
					<?php 
		                $terms_address = woocommerce_get_product_terms( $product->id, "pa_address", "names");
		                foreach ($terms_address as $key => $value) 
		                {
		                    echo $value;
		                }
		            ?>
				</span>
			
			<div id="phone_owner">
				<?php 
			        $terms_phone = woocommerce_get_product_terms( $product->id, "pa_phone", "names");
			        foreach ($terms_phone as $key => $value) 
			        {
			            echo $value;
			        }          
			    ?>
			</div>
			</div>
		</div-->
    <?php		
	}
   /*
   echo "<br><pre>";
   print_r( $post );
   echo "</pre><BR>";*/
	?>
</div><!-- END #details-->
                     
<div id="sidebar_deal_detail">
<?php
	global $product, $woocommerce_loop;	
   
   #------------------------------------------------------------------------------   
   function my_filter_where_after( $where = '' ) 
   {       
      global $post;      
      $where .= " AND CAST('".$post->post_date."' AS datetime) >  CAST(post_date AS datetime)";
      return $where;
   }
   
   add_filter( 'posts_where', 'my_filter_where_after' );   
   #------------------------------------------------------------------------------
   
   
   if(isset($_REQUEST['week']))
   {
      $shopping_week = $_REQUEST['week'];
   }
   else 
   {
      $shopping_week = get_query_var('week');
   }
   
   if(strcasecmp(trim($shopping_week), "")==0)
   {
      $shopping_week = get_option( 'woocommerce_shopping_week');
   }
   
   $tax_query = array(
   'relation' => 'AND',
      array(
         'taxonomy' => 'pa_week',
         'field' => 'slug',
         'terms' => array( $shopping_week )
         ),      
   );

   $my_product_cat = null;

   if (isset($_REQUEST['product_cat']) AND strcasecmp( trim($_REQUEST['product_cat']), "")!=0 )
   {
      $my_product_cat = $_REQUEST['product_cat'];
   }
   else
   {
      $my_product_cat = get_query_var('product_cat');
   }


   if (isset($my_product_cat) AND strcasecmp( trim($my_product_cat),"")!=0)
   {

      $tax_query[] = array(
            'taxonomy' => 'product_cat',
            'field' => 'slug',
            'terms' => array( $my_product_cat )
            );
   }
   
   /*
   $args =  array(
      'post_type'				=> 'product',
      'ignore_sticky_posts'	=> 1,
      'no_found_rows' 		=> 1,      
      'orderby'=>'post_date',
      'order'=>'DESC',
      'nopagging' => true,
      'meta_query' => array(
         array(
           'key' => '_visibility',
           'value' => 'visible',
         )
      ),
      'tax_query'=> $tax_query
   );   
   */
   
   $args =  array(
      'post_type'				=> 'product',
      'orderby'=>'post_date',
      'order'=>'DESC',
      'nopagging' => 1,
      'meta_query' => array(
         array(
           'key' => '_visibility',
           'value' => 'visible',
         )
      ),
      'tax_query'=> $tax_query
   );
  
   
   $products = new WP_Query( $args );
   
   /*
   echo "<pre>";
   print_r($args);
   echo "<pre>";
   */
   
  /*
   echo "<br><pre>";
   echo( $products->request );
   echo "</pre><BR>";
  */
   #------------------------------------------------------------------------------
   remove_filter( 'posts_where', 'my_filter_where_after' );
   
   
   #------------------------------------------------------------------------------
   #------------------------------------------------------------------------------
   #------------------------------------------------------------------------------   
   function my_filter_where_before( $where = '' ) 
   { 
      global $post;      
      $where .= " AND CAST('".$post->post_date."' AS datetime) <= CAST(post_date AS datetime)";
      return $where;
   }
   
   add_filter( 'posts_where', 'my_filter_where_before' );   
   #------------------------------------------------------------------------------
   #------------------------------------------------------------------------------
   
   $products_before_curret = new WP_Query( $args );
  /*
   echo "<br><pre>";
   echo( $products_before_curret->request );
   echo "</pre><BR>";
   */
   #------------------------------------------------------------------------------
   remove_filter( 'posts_where', 'my_filter_where_before' );
   #------------------------------------------------------------------------------   
   
   
   
   //$woocommerce_loop['columns'] 	= $columns;   
   //$products = array_merge($products_after_current, $products_before_curret);
   
   if ( $products->have_posts() OR $products_before_curret->have_posts()) 
   {?>
      <div class="related products">
         <div id="title_moredeal_sidebar">
                More Deals <!--a class="link_content" href="<?php //echo esc_url( home_url( '/' ) ); ?>">See All</a-->
         </div>
         <ul class="products">
            <?php 
            while ( $products->have_posts() ) 
            {  
               global $products;               
               $products->the_post(); 
               //woocommerce_get_template_part( 'sidebar', 'single-product' );               
               woocommerce_get_template( 'single-product/sidebar-single-product.php' );
            } // end of the loop. 
            
            
            while ( $products_before_curret->have_posts() ) 
            {  
               global $products_before_curret;               
               $products_before_curret->the_post();              
               woocommerce_get_template( 'single-product/sidebar-single-product.php' );
            }
            
         
         
         
         
         ?>
         </ul>
         <script type="text/javascript">
		            jQuery(document).ready(function($){
		               $(".content_list").mouseover(function(){
		               		var child_node= $(this).find('.shadow_item_sidebar');
		               		var child_node_buton= $(this).find('.buton_shadow_item');

		               			var height_parent = $(this).height();
		               			$(child_node).height(height_parent);


		               			
		               	  $(child_node).removeClass('hide_shadow_item_sidebar');
		               	  $(child_node_buton).removeClass('hide_shadow_item_sidebar');
		                      
		               });
		               $(".content_list").mouseout(function(){
							var child_node= $(this).find('.shadow_item_sidebar');	
							var child_node_buton= $(this).find('.buton_shadow_item');	               	
		                $(child_node).addClass('hide_shadow_item_sidebar');
		                $(child_node_buton).addClass('hide_shadow_item_sidebar');
		               });

		            });
		         </script>
      </div>
   <?php 
   }
   
   ?>
	<?php
		/**
		 * woocommerce_after_main_content hook
		 *
		 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
		 */
		do_action('woocommerce_after_main_content');
	?> 
	<?php
		/**
		 * woocommerce_sidebar hook
		 *
		 * @hooked woocommerce_get_sidebar - 10
		 */
		do_action('woocommerce_sidebar');
?>
   
   
   
   
   
   
   
   
   <div id="locked_for_lightbox"></div>
   
   <div id="ligthbox_options" class="ligthbox_options" style="visibility: hidden;">
      <div class="arrow_left_lightbox">
         <img src="<?php echo get_bloginfo('stylesheet_directory')?>/images/arrow.png">
      </div>
               
                
      <div class="lightbox_close lightbox_close_control">                  
         X
      </div>

      <script>
       $(document).ready(function(){
          $('.lightbox_close_control').click(function (){
             hidden_lightbox();
          });
       }); 
      </script>
      <?php
      if(strcasecmp($product->product_type,"bundle")==0)
      {
         echo $str_bundle;
      }
      else 
      {
         echo $light_box_content;
      }
      ?>
   </div>
   
           
   
   
   
   <?php } // end of the loop. ?>
   
   
   
   
   
   
<?php
##################################################################################      
function cartesian($input) {
    $result = array();

    while (list($key, $values) = each($input)) {        
        if (empty($values)) {
            continue;
        }
        if (empty($result)) {
            foreach($values as $value) {
                $result[] = array($key => $value);
            }
        }
        else {            
            $append = array();
            foreach($result as &$product) {                
                $product[$key] = array_shift($values);
                $copy = $product;
                foreach($values as $item) {
                    $copy[$key] = $item;
                    $append[] = $copy;
                }
                array_unshift($values, $product[$key]);
            }
            $result = array_merge($result, $append);
        }
    }
    return $result;
}
##################################################################################
 //array_multiSort with natural sort
function naturalSort2D($array){

   if (!function_exists(naturalSort2DCompare)){
      function naturalSort2DCompare($a, $b){
         global $columns_order;

         foreach($columns_order as $key => $value){

            if (!isset($a[$key])) continue;
            unset($compareResult);
            //Case insensitive string comparisons using a "natural order" algorithm
            $compareResult = strnatcasecmp($a[$key], $b[$key]);

            if ($compareResult === 0) continue;

            $value = strtoupper(trim($value));

            if ($value === 'DESC'){
               $compareResult = $compareResult*-1;
            }
            return $compareResult;
         }
         return 0;
      }
   }	
   uasort($array, 'naturalSort2DCompare');
	
   return $array;
}

      
      
      
	?>
</div><!-- END #sidebar_deal_detail-->
<?php get_footer(); ?>