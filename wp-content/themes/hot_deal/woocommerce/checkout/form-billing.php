<?php
/**
 * Checkout billing information form
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

global $woocommerce;
?>

<style type="text/css">
.entry-content {
		width: 870px;
	}
	form .form-row:after{
	clear: both;
	/*float: none !important;*/
	/*padding-top: 10px;*/
}
form .form-row-first, form .form-row-last{
	/*float: none !important; */
}

form .form-row label{
	display: block;
	padding-bottom: 5px;
}
form .form-row input.input-text, form .form-row textarea{
	-moz-box-sizing: border-box;
    line-height: 1em;
    margin: 0;
    outline: 0 none;
    /*width: 50%;*/
    border-radius: 5px;
    line-height: 15px;
    border:1px solid #3469ab;
    padding-left: 10px;
}
form .form-row{
	/*margin: 14px 0 6px;
	padding: 3px;*/
}
</style>
<script>
   jQuery(document).ready(function($){
      $('#billing_address_1_field').append('<span class="we_not_accept">We do not accept PO box</span>');
      $('#shipping_address_1_field').append('<span class="we_not_accept">We do not accept PO box</span>');
      
   });
   
</script>

<?php if ( $woocommerce->cart->ship_to_billing_address_only() && $woocommerce->cart->needs_shipping() ) : ?>

	<h3><?php _e('Billing &amp; Shipping', 'woocommerce'); ?></h3>

<?php else : ?>

	<h3><?php _e('Billing Address', 'woocommerce'); ?></h3>

<?php endif; ?>

<?php do_action('woocommerce_before_checkout_billing_form', $checkout); ?>

<?php 
foreach ($checkout->checkout_fields['billing'] as $key => $field) {
	woocommerce_form_field( $key, $field, $checkout->get_value( $key ) );
}
?>

<?php do_action('woocommerce_after_checkout_billing_form', $checkout); ?>

<?php if (!is_user_logged_in() && get_option('woocommerce_enable_signup_and_login_from_checkout')=="yes") : ?>

	<?php if (get_option('woocommerce_enable_guest_checkout')=='yes') : ?>

		<p class="form-row">
			<input class="input-checkbox" id="createaccount" <?php checked($checkout->get_value('createaccount'), true) ?> type="checkbox" name="createaccount" value="1" /> <label for="createaccount" class="checkbox"><?php _e('Create an account?', 'woocommerce'); ?></label>
		</p>

	<?php endif; ?>

	<?php do_action( 'woocommerce_before_checkout_registration_form', $checkout ); ?>

	<div class="create-account">

		<p><?php _e('Create an account by entering the information below. If you are a returning customer please login at the top of the page.', 'woocommerce'); ?></p>

		<?php foreach ($checkout->checkout_fields['account'] as $key => $field) : ?>

			<?php woocommerce_form_field( $key, $field, $checkout->get_value( $key ) ); ?>

		<?php endforeach; ?>

	</div>

	<?php do_action( 'woocommerce_after_checkout_registration_form', $checkout ); ?>

<?php endif; ?>