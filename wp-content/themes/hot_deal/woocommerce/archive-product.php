<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive.
 *
 * Override this template by copying it to yourtheme/woocommerce/archive-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

get_header(); 

?>

	<?php
		/**
		 * woocommerce_before_main_content hook
		 *
		 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
		 * @hooked woocommerce_breadcrumb - 20
		 */
		//do_action('woocommerce_before_main_content');
	?>
	<div id="title_page">
      <div id="title_shop_page">
         <h1 class="page-title">
            <?php if ( is_search() ) : ?>
               <?php
                  printf( __( 'Search Results: &ldquo;%s&rdquo;', 'woocommerce' ), get_search_query() );
                  if ( get_query_var( 'paged' ) )
                     printf( __( '&nbsp;&ndash; Page %s', 'woocommerce' ), get_query_var( 'paged' ) );
               ?>
            <?php elseif ( is_tax() ) : ?>
               <?php echo single_term_title( "", false ); ?>
            <?php else : ?>
               <?php
                  $shop_page = get_post( woocommerce_get_page_id( 'shop' ) );

                  echo apply_filters( 'the_title', ( $shop_page_title = get_option( 'woocommerce_shop_page_title' ) ) ? $shop_page_title : $shop_page->post_title );
               ?>
            <?php endif; ?>
         </h1>
      </div>
      <!--div id="aux_counter">
         <h1><strong id="product_counter"></strong> Results </h1>
      </div-->
	</div>
<?php
global $woocommerce;

function limit_words($string, $word_limit=25) {
$words = explode(' ', $string);
return implode(' ', array_slice($words, 0, $word_limit));
}

$shopping_week = get_option( 'woocommerce_shopping_week');
      
if(isset($_REQUEST['week']))
{
   $shopping_week = $_REQUEST['week'];
}

$tax_query = array();

$my_product_cat = null;

if (isset($_REQUEST['product_cat']) AND strcasecmp( trim($_REQUEST['product_cat']), "")!=0 )
{
   $my_product_cat = $_REQUEST['product_cat'];
}
else
{
   $my_product_cat = get_query_var('product_cat');
}

if(isset($my_product_cat) AND strcasecmp( trim($my_product_cat),"")!=0)
{
   $tax_query[] = array('taxonomy' => 'product_cat',
                         'field' => 'slug',
                         'terms' => array( $my_product_cat ));
}

$quantity_show_features = 1;

$args = array(
  'post_type' => 'product',   
   'nopaging' => true,   
   'meta_query' => array(
    
      array(
         'key' => '_visibility',
         'value' => 'visible',
       ),
      array(
            'key' => 'week',
            'value' =>",".$shopping_week.",",
            'compare' => 'LIKE'
         ),
  ),
   'tax_query'=> $tax_query   
);

global $wp_query;      
$wp_query = new WP_Query( $args );







#---------------------------------------------------------------------------------
$my_posts = $wp_query->posts;            
$sorters = array();
for($i=0; $i<count($my_posts); $i++)
{
   $array_weeks = explode(",", get_post_meta($my_posts[$i]->ID, "week", true));               
   $target_index = array_search($shopping_week, $array_weeks);                
   $array_sorters = explode(",", get_post_meta($my_posts[$i]->ID, "sorter", true));               
   $sorters[$i] = $array_sorters[$target_index];
}
for($i=0; $i<count($sorters); $i++)
{
   for($j=$i; $j<count($sorters); $j++)
   {
      if($sorters[$i] > $sorters[$j])
      {
         $aux_s = $sorters[$i];
         $sorters[$i] = $sorters[$j];
         $sorters[$j] = $aux_s; 

         $aux = $my_posts[$i];
         $my_posts[$i] = $my_posts[$j];
         $my_posts[$j] = $aux;   
      }
   }
}
$wp_query->posts = $my_posts;
#---------------------------------------------------------------------------------

$index_featured = 0;

while($wp_query->have_posts() AND $index_featured < $quantity_show_features)
{
   $index_featured++;
      
   $wp_query->the_post(); global $product, $post; 
 
   $terms_regular_price = woocommerce_get_product_terms( $product->id, "pa_regular-price", "names"); 
      foreach ($terms_regular_price as $key => $value)
      {
          $regular_price = $value;
      }		           
		$terms_sale_price = woocommerce_get_product_terms( $product->id, "pa_sale-price", "names"); 
      foreach ($terms_sale_price as $key => $value)
      {
          $sale_price = $value;
      }		       
	    
	   $you_save_money =  $regular_price - $sale_price;
	  
	   if((isset($regular_price) AND strcasecmp($regular_price,"")!= 0) AND  
	  	    isset($regular_price) AND strcasecmp($regular_price,"")!= 0) 
	   {
        $save_percent = ceil((1-($sale_price/$regular_price)) * 100);    
	   }
	   else
	   {
         $save_percent = 0;
		}   
      ?>




		<div id="featured_deal">
			<div class="image_featured">
              <a href='<?php echo get_bloginfo("url")."/?product=".$post->post_name ?>' title="<?php echo esc_attr($post->post_title ? $post->post_title : $post->ID); ?>">
                <div class="image_grill">
                  <?php if (has_post_thumbnail( $product->id )) echo get_the_post_thumbnail($product->id, 'thumbnail');?></div>
             </a>
	      	</div>	
	      	<div class="description_deal">
		      	<div class="description_in" >
	                <div id="title_featured_deal"> 
	                	<?php 
				            $terms_website = woocommerce_get_product_terms( $product->id, "pa_owner_name", "names");                     
				            foreach ($terms_website as $key => $value) 
				            {
				                echo $value;
				            }		           
				        ?>
	                </div>
	               
	                	<a style="text-decoration:none;" href="<?php echo get_bloginfo("url")."/?product=".$post->post_name ?>">
	                	<h1><?php the_title(); ?> </h1>
	                	</a>
	               
	                <div class="short_description">     
	                  <span class="content">
						      <?php echo nl2br($post->post_excerpt);?>
						    </span>
	                </div>  
	                <!--div class="price" style="float:left !important;">                                    
	                    <span class="real_price">$<?php echo $product->regular_price?></span>
	                    <span class="sale_price">$<?php echo $product->get_price_excluding_tax()?></span>
	                </div-->

	                <div style="overflow:hidden; ">
	                	<div id="ticket">
	                		<img src="<?php echo get_bloginfo('stylesheet_directory')?>/images/ticket.jpg">
	                	</div>
	                	<div id="content_price">
	                		<div class="real_price">
                           <span style="font-size: 12px">From </span><?php echo woocommerce_price($sale_price)?>
	                		</div>
	                		<div class="sale_price">
	                			<!--instead of <br/-->
                           <div class="regular_price_shop">
                              <?php echo woocommerce_price($regular_price)?>
                           </div>
	                		</div>

	                	</div>

                  <div id="category_product">
            		<?php
            			global $product;
						$product_category = $product->get_categories();
						//echo $cat;
								
						//print_r($cat);
						
$string = <<<XML
<?xml version='1.0'?> 
<document>
 $product_category
</document>
XML;


$data_result = simplexml_load_string($string);

$category_name = $data_result->a;

echo "<div class='".$category_name[0]."'><a>".$category_name[0]."</a></div>";

            		?>

            	</div>
            	<style type="text/css">
            		a.button, button.button, input.button, #respond input#submit, #content input.button{
					font-family: "Lucida Sans","Lucida Grande",Garuda,sans-serif;
				    font-size: 14px;
					color: #ffffff;
					background-color: #346aac;
					background: -moz-linear-gradient(
						top,
						#346aac 0%,
						#164677);
					background: -webkit-gradient(
						linear, left top, left bottom, 
						from(#346aac),
						to(#164677));
					-moz-border-radius: 11px;
					-webkit-border-radius: 11px;
					border-radius: 11px;
					border: 1px solid #124170;
					
					height: 30px;
					line-height: 27px;
					margin-bottom: 5px;
					width: 95px;
					cursor: pointer;
					/*font-weight: bold;*/
					behavior: url(ie-css3.htc);
					text-align: center;
				}
				a.button:hover, button.button:hover, input.button:hover, #respond input#submit:hover, #content input.button:hover{
					font-family: "Lucida Sans","Lucida Grande",Garuda,sans-serif;
				    font-size: 14px;
					color: #ffffff;
					background-color: #346aac;
					background: -moz-linear-gradient(
						top,
						#346aac 0%,
						#164677);
					background: -webkit-gradient(
						linear, left top, left bottom, 
						from(#346aac),
						to(#164677));
					-moz-border-radius: 11px;
					-webkit-border-radius: 11px;
					border-radius: 11px;
					border: 1px solid #124170;
					line-height: 27px;
					height: 30px;
					margin-bottom: 5px;
					width: 95px;
					cursor: pointer;
					/*font-weight: bold;*/
					behavior: url(ie-css3.htc);
					text-align: center;
				}
            	</style>

	                	<div id="buton_featured">
                        <?php
                        /*
                        if(strcasecmp($product->product_type,"variable")==0)
                        {
                           $link 	= apply_filters( 'variable_add_to_cart_url', get_permalink( $product->id ) );
                           $label 	= apply_filters( 'variable_add_to_cart_text', __('Options', 'woocommerce') );

                           ?>
                           <a
                              href="<?php echo $link?>"
                              rel="nofollow"
                              data-product_id="<?php echo $product->id?>"
                              class="buton_featured_options"

                              style=""
                              >
                              <?php echo $label?>
                           </a>
                           <?php                           
                        }
                        else
                        {?>
                           <a style="display:inline-block" href='<?php echo get_bloginfo("url")."/?post_type=product&add-to-cart=".$product->id ?>'>
                              <button type="button" name="" value="" class="css3button">Buy Now</button>
                           </a>
                        <?php
                        }*/
                        ?>
	                		
	                		<a style="display:inline-block; text-decoration:none;" href='<?php echo get_bloginfo("url")."/?product=".$post->post_name ?>'>
		                    	<div class="button_shop_buy">Shop Now</div>
		                	</a>
		                	
	                	</div>
	   	                
	              	</div>	  	
                </div>
            </div>
                           
       </div>



<?php
}
?>




		<?php do_action( 'woocommerce_archive_description' ); ?>

		<?php if ( is_tax() ) : ?>
			<?php do_action( 'woocommerce_taxonomy_archive_description' ); ?>
		<?php elseif ( ! empty( $shop_page ) && is_object( $shop_page ) ) : ?>
			<?php do_action( 'woocommerce_product_archive_description', $shop_page ); ?>
		<?php endif; ?>

		<?php 
      
      
      if ( $wp_query->have_posts() ) : ?>
			<?php //do_action('woocommerce_before_shop_loop'); ?>
			<ul id="all_deals" class="products">
				<?php 
            woocommerce_product_subcategories();				
				 ?>
				<?php 
            global $counter_post;
				$counter_post=0;
            #---------------------------------------------------------------------------------
            #---------------------------------------------------------------------------------
				while( $wp_query->have_posts() ) 
            {  
               $wp_query->the_post();
               global $product, $post;
               
               $counter_post++;
               woocommerce_get_template_part( 'content', 'product' );                   
               
            }// end of the loop.
            $counter_post += $quantity_show_features;
				?>
         </ul>

            

<?php


      $shopping_week = get_option( 'woocommerce_shopping_week');
      
      if(isset($_REQUEST['week']))
      {
         $shopping_week = $_REQUEST['week'];
      }
      $tax_query = array();
      
      $my_product_cat = null;

      if (isset($_REQUEST['product_cat']) AND strcasecmp( trim($_REQUEST['product_cat']), "")!=0 )
      {
         $my_product_cat = $_REQUEST['product_cat'];
      }
      else
      {
         $my_product_cat = get_query_var('product_cat');
      }

      
      if (isset($my_product_cat) AND strcasecmp( trim($my_product_cat),"")!=0)
      {
         $tax_query[] = array(
               'taxonomy' => 'product_cat',
               'field' => 'slug',
               'terms' => array( $my_product_cat )
               );
      }


      $total_args = array(
      'post_type' => 'product',
      'orderby'=>'post_date',
      'nopagging' => true,
      'order'=>'DESC',
      'meta_query' => array(
            array(
               'key' => '_visibility',
               'value' => 'visible',
             ),
             array(
               'key' => 'week',
               'value' =>",".$shopping_week.",",
               'compare' => 'LIKE'
             ),
      ),   
      'tax_query'=> $tax_query

	);
	$total_posts = new WP_Query( $total_args );
	$count_total_posts = $total_posts -> found_posts;
	?>


<div id="pagination_deals">
   
	<div id="showing_deals">
		Showing  <span id="parcial_deals"></span> of <span id="total_deals"><?php echo($count_total_posts);?></span>
	</div>
	
   <input value="See More Deals" type="button" id="see_more_deals" />
   
	<input value="<?php echo $counter_post?>" type="hidden" id="offset_more_deals" />
	<input value="6" type="hidden" id="add_quantity_more_deals" />
</div>

<script type="text/javascript">
	 jQuery(document).ready(function($){
			$("#product_counter").html(<?php echo $counter_post;?>);
			$("#parcial_deals").html(<?php echo $counter_post;?>);
			
         
         
	});
   
   
   
</script>

<script>
jQuery(document).ready(function($){
	$("#see_more_deals").click(function(){

      var offset_more_deals = $("#offset_more_deals").attr("value");
      var add_quantity_more_deals = $("#add_quantity_more_deals").attr("value");

      var url_local = "http://localhost/hot_deal/?page_id=160";
      var url_up = "http://www.vipweeklydeals.com/?page_id=160";

      $.get(url_up+"&offset_more_deals="+offset_more_deals+"&add_quantity_more_deals="+add_quantity_more_deals+"&product_cat=<?php echo $my_product_cat?>&week=<?php echo $shopping_week?>",function(data,status){
         $("#all_deals").append(data);

         offset_more_deals = parseInt(offset_more_deals) + parseInt(add_quantity_more_deals);



          var total_deals = $("#total_deals").html();


         if(offset_more_deals<=parseInt(total_deals))
         {
            $("#product_counter").html(offset_more_deals);
            $("#parcial_deals").html(offset_more_deals);
            $("#offset_more_deals").attr("value",offset_more_deals);
         }
         else{

            $("#product_counter").html(total_deals);
            $("#parcial_deals").html(total_deals);
            $("#offset_more_deals").attr("value",total_deals);
         }
         
         visible_see_more();
         
      });
   });   
   /***************************************************************************/
   
   function visible_see_more(){
      var total_deals = $("#total_deals").html();
      var offset_more_deals = $("#offset_more_deals").attr('value');

      if(offset_more_deals<parseInt(total_deals))
      {
         $("#see_more_deals").css('visibility','visible');
      }
      else
      {
         $("#see_more_deals").css('visibility','hidden');
      }
   }
   
   visible_see_more();
});
</script>

			

			<?php do_action('woocommerce_after_shop_loop'); ?>

		<?php else : ?>

			<?php if ( ! woocommerce_product_subcategories( array( 'before' => '<ul class="products">', 'after' => '</ul>' ) ) ) : ?>

				<p><?php _e( 'No products found which match your selection.', 'woocommerce' ); ?></p>

			<?php endif; ?>

		<?php endif; ?>

		<div class="clear"></div>

		<?php
			/**
			 * woocommerce_pagination hook
			 *
			 * @hooked woocommerce_pagination - 10
			 * @hooked woocommerce_catalog_ordering - 20
			 */
			#do_action( 'woocommerce_pagination' );
		?>

	<?php
		/**
		 * woocommerce_after_main_content hook
		 *
		 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
		 */
		do_action('woocommerce_after_main_content');
	?>

	<?php
		/**
		 * woocommerce_sidebar hook
		 *
		 * @hooked woocommerce_get_sidebar - 10
		 */
		do_action('woocommerce_sidebar');
	?>

<?php get_footer('shop'); ?>