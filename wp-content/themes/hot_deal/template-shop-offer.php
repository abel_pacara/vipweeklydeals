<?php
/*
 * template name: shop offer
 * 
 */

get_header(); 
?>

<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/style_christmas.css" />

<style>
   .text_page_christmas{
      padding-top: 10px;
      line-height: 15px;
      font-size: 14px;      
      color:#949494;
      font-family: "Lucida Sans","Lucida Grande",Garuda,sans-serif;
      line-height: 18px;
      overflow: hidden;
   }
   
   
</style>

<div class="wrapper_christmas">
   <div class="text_page_christmas">
      
         <?php while ( have_posts() ) : the_post(); ?>
            <?php the_content(); ?>
            <?php //comments_template( '', true ); ?>
         <?php endwhile; // end of the loop. ?>
      
   </div>
   
   
	<?php
		/**
		 * woocommerce_before_main_content hook
		 *
		 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
		 * @hooked woocommerce_breadcrumb - 20
		 */
		//do_action('woocommerce_before_main_content');
	?>
	
<?php
global $woocommerce;

function limit_words($string, $word_limit=25) {
$words = explode(' ', $string);
return implode(' ', array_slice($words, 0, $word_limit));
}

?>





		<?php do_action( 'woocommerce_archive_description' ); ?>

		<?php if ( is_tax() ) : ?>
			<?php do_action( 'woocommerce_taxonomy_archive_description' ); ?>
		<?php elseif ( ! empty( $shop_page ) && is_object( $shop_page ) ) : ?>
			<?php do_action( 'woocommerce_product_archive_description', $shop_page ); ?>
		<?php endif; ?>

		<?php 
      
      $tax_query[] = array('taxonomy' => 'product_cat',
                         'field' => 'slug',
                         'terms' => array( "merry-christmas" ));
      
      $args = array(
        'post_type' => 'product',
         'nopagging' => true,
         'meta_key'=>'offer_day',            
         'orderby'=>'meta_value_num',
         'order'=>'ASC',
         
        'tax_query'=>$tax_query
      );
      //print_r($param_by_category);
      global $wp_query;
      
      $wp_query = new WP_Query( $args );
      
      
      
      if ( $wp_query->have_posts() ) : ?>
			<?php //do_action('woocommerce_before_shop_loop'); ?>
			<ul id="all_deals" class="products">
				<?php 
            woocommerce_product_subcategories();				
				 ?>
				<?php 
            global $counter_post;
				$counter_post=0;
            #---------------------------------------------------------------------------------
            #---------------------------------------------------------------------------------
				while( $wp_query->have_posts() ) 
            {  
               $wp_query->the_post();
               global $product, $post;
               
               $counter_post++;
               woocommerce_get_template_part( 'content', 'product_offer' );                   
               
            }// end of the loop.
            $counter_post += $quantity_show_features;
				?>
         </ul>

            





			

			<?php do_action('woocommerce_after_shop_loop'); ?>

		<?php else : ?>

			<?php if ( ! woocommerce_product_subcategories( array( 'before' => '<ul class="products">', 'after' => '</ul>' ) ) ) : ?>

				<p><?php _e( 'No products found which match your selection.', 'woocommerce' ); ?></p>

			<?php endif; ?>

		<?php endif; ?>

		<div class="clear"></div>

		<?php
			/**
			 * woocommerce_pagination hook
			 *
			 * @hooked woocommerce_pagination - 10
			 * @hooked woocommerce_catalog_ordering - 20
			 */
			#do_action( 'woocommerce_pagination' );
		?>

	<?php
		/**
		 * woocommerce_after_main_content hook
		 *
		 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
		 */
		do_action('woocommerce_after_main_content');
	?>

	<?php
		/**
		 * woocommerce_sidebar hook
		 *
		 * @hooked woocommerce_get_sidebar - 10
		 */
		do_action('woocommerce_sidebar');
	?>
</div>


<?php get_footer('shop'); ?>