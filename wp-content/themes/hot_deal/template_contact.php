<?php
/*
* Template Name:contact
*/
get_header(); 
?>


<div id="container2">
	<div id="container">
		<div id="primary" class="site-content">
		<div id="content" role="main">

		<div class="title_page">
			<h1><?php the_title(); ?></h1>
		</div>
		<div id="page_content">	
			<div id="page">	
				<div class="column_form">
	            <?php
	            $str_shorcode='[contact-form-7 id="187" title="Contact form 1"]';
	            /*[contact-form-7 id="187" title="Contact form 1"]*/
	            echo do_shortcode( $str_shorcode );
	            ?>
				</div>

				<div class="column_contact">			
					<?php while ( have_posts() ) : the_post(); ?>
						<?php the_content(); ?>
					<?php endwhile; // end of the loop. ?>
				</div>
			</div> 	<!-- END MAIN CONTENT -->

		</div>
		</div>
		
	</div>
		
	</div>
</div>


<?php get_footer(); ?>