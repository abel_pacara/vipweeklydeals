<?php
/**
 * template name: Template Sorting
 * 
 * The Template for displaying product archives, including the main shop page which is a post type archive.
 *
 * Override this template by copying it to yourtheme/woocommerce/archive-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

get_header(); 

?>

	<div id="title_page">
      <div id="title_shop_page">
         
      </div>     
	</div>
<?php
global $woocommerce;

      
      $shopping_week = get_option( 'woocommerce_shopping_week');
      
      
      if(isset($_REQUEST['week']))
      {
         $shopping_week = $_REQUEST['week'];
      }
    
      $tax_query = array();
      
      $my_product_cat = null;

      if (isset($_REQUEST['product_cat']) AND strcasecmp( trim($_REQUEST['product_cat']), "")!=0 )
      {
         $my_product_cat = $_REQUEST['product_cat'];
      }
      else
      {
         $my_product_cat = get_query_var('product_cat');
      }
      
      if (isset($my_product_cat) AND strcasecmp( trim($my_product_cat),"")!=0)
      {
         $tax_query[] = array(
               'taxonomy' => 'product_cat',
               'field' => 'slug',
               'terms' => array( $my_product_cat )
               );
      }
      
      
      $args = array(
         'post_type' => 'product',
         'nopaging' => true,            
         'meta_query' => array(
                  
                  array(
                     'key' => '_visibility',
                     'value' => 'visible',
                  ),                  
                  array(
                     'key' => 'week',
                     'value' =>",".$shopping_week.",",
                     'compare' => 'LIKE'
                  ),
            ),
        'tax_query'=>$tax_query
      );
      
      global $wp_query;      
      $wp_query = new WP_Query( $args );
      

            global $counter_post;
				$counter_post=0;
            
            #---------------------------------------------------------------
            $my_posts = $wp_query->posts;
            
            $sorters = array();
            
            for($i=0; $i<count($my_posts); $i++)
            {
               $array_weeks = explode(",", get_post_meta($my_posts[$i]->ID, "week", true));               
               $target_index = array_search($shopping_week, $array_weeks);                
               $array_sorters = explode(",", get_post_meta($my_posts[$i]->ID, "sorter", true));               
               $sorters[$i] = $array_sorters[$target_index];
            }
            
            for($i=0; $i<count($sorters); $i++)
            {
               for($j=$i; $j<count($sorters); $j++)
               {
                  if($sorters[$i] > $sorters[$j])
                  {
                     $aux_s = $sorters[$i];
                     $sorters[$i] = $sorters[$j];
                     $sorters[$j] = $aux_s; 
                     
                     $aux = $my_posts[$i];
                     $my_posts[$i] = $my_posts[$j];
                     $my_posts[$j] = $aux;   
                  }
               }
            }
            
            $wp_query->posts = $my_posts;
            #---------------------------------------------------------------
            ?>
            
            <h1 class="page-title">
               Test of sorting by Week <?php echo $shopping_week?> DESC
            </h1>
            <table style="border:1px solid #dcdcdc; width: 100%">
               <tr style="background-color: #fcfcfc; font-weight: bold">
                  <td>#</td>
                  <td></td>
                  <td>Title</td>
                  <td>Weeks</td>
                  <td>Sorters</td>
                  <td>Expiration Date</td>
               </tr>
               <?php            
               foreach($sorters AS $i=>$value)
               {
                  $new_product =new WC_Product($my_posts[$i]->ID);

                  $week = get_post_meta($my_posts[$i]->ID, "week", true);
                  $sorter = get_post_meta($my_posts[$i]->ID, "sorter", true);
                  ?>
                  <tr style="border:1px solid #dcdcdc; line-height: 20px">
                     <td><?php echo $i+1?></td>
                     <td>
                        <a href="<?php echo get_permalink($my_posts[$i]->ID)?>" title="<?php echo "ID=".$my_posts[$i]->ID;?>" alt="<?php echo "ID=".$my_posts[$i]->ID;?>">
                           <?php echo get_the_post_thumbnail($my_posts[$i]->ID, array(105,70)); ?>
                        </a>
                     </td>
                     <td><?php echo $my_posts[$i]->post_title?></td>
                     <td><?php echo $week?></td>
                     <td><?php echo $sorter?></td>
                     <td>
                        <?php 
                        $terms_date_expiration = woocommerce_get_product_terms( $my_posts[$i]->ID, "pa_date-expiration", "names");
   
                        $date_expiration = "";
                        foreach ($terms_date_expiration as $key => $value) 
                        {
                           if(strcasecmp(trim($value), "")!=0)
                           {
                              $date_expiration = $value;         
                           }
                        }
                        
                        echo $date_expiration;
                        ?>
                     </td>
                  </tr>
                  <?php
               }
               ?>
            </table>
         </ul>

            


	
<?php get_footer('shop'); ?>