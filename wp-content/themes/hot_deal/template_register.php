
<?php
/*
template name: tpl register
*/
get_header();
global $woocommerce;
?>

<style type="text/css">
.entry-content {
		width: 870px;
	}
	form .form-row:after{
	clear: both;
	/*float: none !important;*/
	/*padding-top: 10px;*/
}
form .form-row-first, form .form-row-last {
    float: none;
    overflow: visible;
    width: 35%;
}

form .form-row label {
    display: block;
    line-height: 20px;
    font-size: 12px;
}
form .form-row input.input-text, form .form-row textarea{
	-moz-box-sizing: border-box;
    line-height: 1em;
    margin: 0;
    outline: 0 none;
    width: 50%;
   /* border-radius: 5px;*/
    line-height: 15px;
   /* border:1px solid #3469ab;*/
    padding-left: 10px;
}
form .form-row {
    margin: 0 0 6px;
    padding: 10px;
    width: 60%;
}
form .form-row-last {
    float: none;
}
form .form-row-first, form .form-row-last {
    float: none;
    overflow: visible;
    width: 47%;
}
#page {
    background-color: #FFFFFF;
    overflow: hidden;
    padding-bottom: 20px;
    padding-left: 20px;
    padding-right: 20px;
}
</style>
<?php


add_action('append_error_links','action_append_error_links');

$woocommerce->show_messages(); 

/*
if ( $woocommerce->error_count() > 0  )
{
			woocommerce_get_template( 'shop/errors.php', array(
					'errors' => $woocommerce->get_errors()
				) );
      
}*/

function action_append_error_links()
{?>
   
   <a href="<?php echo get_permalink(9)?>">
      Login
   </a>
   Or 
   <a href="<?php echo get_permalink(3056)?>">
      Lost Password?
   </a>
<?php
}


if ( $woocommerce->message_count() > 0  )
   woocommerce_get_template( 'shop/messages.php', array(
         'messages' => $woocommerce->get_messages()
      ) );

$woocommerce->clear_messages();
?>


<header class="entry-header" style="margin: 25px auto 5px;">
<h1 class="entry-title">My Account</h1>
</header>

<h2><?php _e('Register', 'woocommerce'); ?></h2>
		<form method="post" class="register">

			<?php if ( get_option( 'woocommerce_registration_email_for_username' ) == 'no' ) : ?>

				<p class="form-row form-row-first">
					<label for="reg_username"><?php _e('Username', 'woocommerce'); ?> <span class="required">*</span></label>
					<input type="text" class="input-text" name="username" id="reg_username" value="<?php if (isset($_POST['username'])) echo esc_attr($_POST['username']); ?>" />
				</p>

				<p class="form-row form-row-last">

			<?php else : ?>

				<p class="form-row form-row-wide">

			<?php endif; ?>

				<label for="reg_email"><?php _e('Email', 'woocommerce'); ?> <span class="required">*</span></label>
				<input type="email" class="input-text" name="email" id="reg_email" value="<?php if (isset($_POST['email'])) echo esc_attr($_POST['email']); ?>" />
			</p>

			<div class="clear"></div>

			<p class="form-row form-row-first">
				<label for="reg_password"><?php _e('Password', 'woocommerce'); ?> <span class="required">*</span></label>
				<input type="password" class="input-text" name="password" id="reg_password" value="<?php if (isset($_POST['password'])) echo esc_attr($_POST['password']); ?>" />
			</p>
			<p class="form-row form-row-last">
				<label for="reg_password2"><?php _e('Re-enter password', 'woocommerce'); ?> <span class="required">*</span></label>
				<input type="password" class="input-text" name="password2" id="reg_password2" value="<?php if (isset($_POST['password2'])) echo esc_attr($_POST['password2']); ?>" />
			</p>
			<div class="clear"></div>

			<!-- Spam Trap -->
			<div style="left:-999em; position:absolute;"><label for="trap">Anti-spam</label><input type="text" name="email_2" id="trap" /></div>

			<?php do_action( 'register_form' ); ?>

			<p class="form-row">
				<?php $woocommerce->nonce_field('register', 'register') ?>
				<input type="submit" class="button" name="register" value="<?php _e('Register', 'woocommerce'); ?>" />
			</p>

		</form>

</div></div>
		<?php get_footer(); ?>