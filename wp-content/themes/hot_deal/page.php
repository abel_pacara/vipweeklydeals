<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header(); ?>
<style type="text/css">
.entry-content h1, .entry-header{
   padding-left: 10px;
  /* padding-top: 10px;*/
}

.entry-content>p{
	font-size: 13px;
	color: #666666;
	line-height: 20px;
	width: 900px;
	padding-left: 10px;
}
.entry-content>ul{
	padding-left: 30px;
	padding-bottom: 20px;
}
.entry-content>ul>li {
	list-style-type: disc;
	padding-left: 10px;
	line-height: 25px;
	font-size: 13px;
	color: #666666;
}
</style>
	<div id="primary" class="site-content">
		<div id="content" role="main">

			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'content', 'page' ); ?>
				<?php //comments_template( '', true ); ?>
			<?php endwhile; // end of the loop. ?>

		</div><!-- #content -->
	</div><!-- #primary -->
</div>
</div>
<!--?php get_sidebar(); ?-->
<?php get_footer(); ?>