<?php

/*template name: update - solds
 * 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
      
         $args_orders = array(
					'post_type' => 'shop_order',
					'orderby'=>'post_date',
					'order'=>'DESC',
               'nopaging'=>1,
               'post_status'=>'publish',
               'fields'=>'ids',
               'tax_query'=>array(
                     array(
                        'taxonomy'=>'shop_order_status',
                        'terms'=>array( 'completed', 'processing'),
                        'field'=>'slug',
                        'operator'=>'IN'
                     )
                  )
					);
   
               
               
               $query_orders = new WP_Query( $args_orders );
               
               $my_orders = $query_orders->posts;               
               
					$quantity_solds=0;

					//for($i=0; $i<count($my_orders);$i++)
               foreach($my_orders AS $my_order_key=>$my_order_id)
					{  
                     #---------------------------------------------------------------------------------                     
                     $my_order = new WC_Order($my_order_id);                     
                     $order_items = $my_order->get_items();
                      foreach($order_items AS $key=>$item)
                      {
                        $current_sold_real = get_post_meta($item['product_id'], "sold_real", true);      
                        update_post_meta($item['product_id'], "sold_real", $current_sold_real+$item['qty']);
                        //update_post_meta($item['product_id'], "sold_real", 0);
                      }
					}
               
               echo "update solds Successful";