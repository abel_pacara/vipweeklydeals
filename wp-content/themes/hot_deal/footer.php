<?php
/**
 * The template for displaying the footer.
 *
 * Contains footer content and the closing of the
 * #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
?>
	<!--/div><!-- #main .wrapper -->
	<!--footer id="colophon" role="contentinfo">
		<div class="site-info">
			<?php do_action( 'twentytwelve_credits' ); ?>
			<a href="<?php echo esc_url( __( 'http://wordpress.org/', 'twentytwelve' ) ); ?>" title="<?php esc_attr_e( 'Semantic Personal Publishing Platform', 'twentytwelve' ); ?>"><?php printf( __( 'Proudly powered by %s', 'twentytwelve' ), 'WordPress' ); ?></a>
		</div><!-- .site-info -->
	<!--/footer><!-- #colophon -->
<!--/div><!-- #page -->

<?php //wp_footer(); ?>
<!--/body>
</html-->

                    
                <div id="footer">
                    <div style="height:155px; ">
                        <div style="float:left; width:960px; height:155px;">
                            <div id="secure_pay">
                                <img src="<?php echo get_bloginfo('stylesheet_directory')?>/images/flat-shipping7.png"> 
                            </div>
                      
                                <div id="aux_navigation">
                                    <div class="column_footer">
                                       
                                        <div class="column_content">
                                            <span style="font-weight:600; font-size:13px;">Company <br/></span>
                                            <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="link_footer">Home</a><br/>
                                            <a href="<?php echo get_permalink(69);?>" class="link_footer">About Us</a><br/>

                                            <a href="<?php echo get_permalink(73);?>" class="link_footer">Contact Us</a><br/>
                                            <a href="<?php echo get_permalink(52);?>" class="link_footer">Suppliers</a><br/>
                                        </div>
                                    </div>
                                    <div class="column_footer">
                                        
                                        <div class="column_content">
                                            <span style="font-weight:600; font-size:13px;">Help Desk<br/></span>
                                           
                                            <a href="<?php echo get_permalink(179);?>" class="link_footer">FAQ</a><br/>
                                            <a href="<?php echo get_permalink(71);?>" class="link_footer">Terms & Condition</a><br/>
                                            <a href="<?php echo get_permalink(180);?>" class="link_footer">Privacy Policy</a><br/>
                                        </div>                      
                                    </div>
                                    <div class="column_footer">    
                                        
                                                          
                                    </div>
                                                
                                </div><!-- END #aux_navigation--> 
                                <div class="compras">
                                    <img src="<?php echo get_bloginfo('stylesheet_directory');?>/images/compras.png">
                                </div>
                    </div>

                    </div>                      
                    <div id="bottom_footer" class="content">
                        <div class="credit">
                            <div>
                                <img src="<?php echo get_bloginfo('stylesheet_directory');?>/images/credit-cards.png">
                            </div>
                            
                            <div class="paypal-card" style="float:left; border:1px solid #868686; height:23px; margin-top:4px;">
                                <img src="<?php echo get_bloginfo('stylesheet_directory');?>/images/paypal-card.png">
                            </div>
                        

                        </div>
                        <div class="copyright">
                            <div id="company">VIP WEEKLY DEALS</div>
                            <div id="rights"> &copy;Copyright 2012-2013 VipWeeklyDeals.com</div>
                        </div>
                        <div class="credit">
                            <a href="">
                                <img src="<?php echo get_bloginfo('stylesheet_directory');?>/images/verysign.png">
                            </a>
                        </div>
                        <div class="credit">
                            <a href="">
                                <img src="<?php echo get_bloginfo('stylesheet_directory');?>/images/mcaffe.png">
                            </a>
                        </div>
                        
                    </div>
                </div><!-- END #footer-->

                </div><!-- END #page-->
                
<?php wp_footer(); ?>
            <div id="secure_sign">
                <a href="">
                    <img src="<?php echo get_bloginfo('stylesheet_directory')?>/images/secure-sign.png">
                </a>
            </div>
                    </div> <!-- END #page_container-->

                   
            </div> <!-- END #container--> 
            </div> <!-- END #container-wrapper-->

        </div> <!-- END #wrapper-->
    </BODY>
</HTML>