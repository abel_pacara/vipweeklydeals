<?php
/**
* template name: tpl more deals
*/
?>

<?php
global $woocommerce;

function limit_words($string, $word_limit=25) {
$words = explode(' ', $string);
return implode(' ', array_slice($words, 0, $word_limit));
}



$shopping_week = get_option( 'woocommerce_shopping_week');
      
if(isset($_REQUEST['week']))
{
   $shopping_week = $_REQUEST['week'];
}

$tax_query = array(
'relation' => 'AND',
array(
   'taxonomy' => 'pa_week',
   'field' => 'slug',
   'terms' => array( $shopping_week )
   ),      
);

$my_product_cat = null;
if (isset($_REQUEST['product_cat']) AND strcasecmp( trim($_REQUEST['product_cat']), "")!=0 )
{
   $my_product_cat = $_REQUEST['product_cat'];
}
else
{
   $my_product_cat = get_query_var('product_cat');
}

if (isset($my_product_cat) AND strcasecmp( trim($my_product_cat),"")!=0)
{
   $tax_query[] = array(
         'taxonomy' => 'product_cat',
         'field' => 'slug',
         'terms' => array( $my_product_cat )
         );
}


$args = array(
'post_type' => 'product',
'orderby'=>'post_date',

'posts_per_page'=>$_REQUEST['add_quantity_more_deals'],
'offset'=>$_REQUEST['offset_more_deals'],


'order'=>'DESC',
'meta_query' => array(    
    array(
         'key' => '_visibility',
         'value' => 'visible',
       ),
   ),
'tax_query'=> $tax_query

);


global $counter_post;
$counter_post=0;

$query_featured = new WP_Query( $args );

while($query_featured->have_posts())
{
   $query_featured->the_post(); global $product, $post;

   $counter_post++;
   woocommerce_get_template_part( 'content', 'product' );

}
