<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html style="margin-top:none !important;" <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />


<meta name="google-site-verification" content="aUn83QC5aQE_irnT3jwGqi3tGBph0ouqYwCaLiQPxw0" />

<style>
html
{
	margin-top: 0px !important;
}
</style>


<title><?php wp_title( '|', true, 'right' ); ?></title>
<!--link rel="profile" href="http://gmpg.org/xfn/11" /-->
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

<!-- script FAVICON -->
<link rel="shortcut icon" href="<?php bloginfo('stylesheet_directory'); ?>/favicon.ico" />
<!-- script FAVICON -->

<script type="text/javascript" src="<?php echo get_bloginfo('stylesheet_directory') ?>/js/iepngfix_tilebg.js"></script> 
<script type="text/javascript" src="<?php echo get_bloginfo('stylesheet_directory') ?>/js/ga.js"></script> 

<?php // Loads HTML5 JavaScript file to add support for HTML5 elements in older IE versions. ?>
<!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
<![endif]-->
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
   
   <script type="text/javascript">
     var _gaq = _gaq || [];
     _gaq.push(['_setAccount', 'UA-39522892-1']);
     _gaq.push(['_trackPageview']);

     (function() {
       var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
       ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
       var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
     })();
	 
	 /*
	 $(function() {
       
		   var zIndexNumber = 2000;
		   
		   $("div").each(function() {
				   $(this).css('zIndex', zIndexNumber);
				   zIndexNumber -= 10;
		   });
	});*/

   </script>

   
<!--div id="page" class="hfeed site">
	<header id="masthead" class="site-header" role="banner">
		<hgroup>
			<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
			<h2 class="site-description"><?php bloginfo( 'description' ); ?></h2>
		</hgroup>

		<nav id="site-navigation" class="main-navigation" role="navigation">
			<h3 class="menu-toggle"><?php _e( 'Menu', 'twentytwelve' ); ?></h3>
			<a class="assistive-text" href="#content" title="<?php esc_attr_e( 'Skip to content', 'twentytwelve' ); ?>"><?php _e( 'Skip to content', 'twentytwelve' ); ?></a>
			<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu' ) ); ?>
		</nav><!-- #site-navigation -->

		<?php $header_image = get_header_image();
		if ( ! empty( $header_image ) ) : ?>
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo esc_url( $header_image ); ?>" class="header-image" width="<?php echo get_custom_header()->width; ?>" height="<?php echo get_custom_header()->height; ?>" alt="" /></a>
		<?php endif; ?>
	<!--/header><!-- #masthead -->

	<!--div id="main" class="wrapper"-->
<style type="text/css">
img, div { behavior: url(iepngfix.htc) }
div { behavior: url(ie-css3.htc) }
</style> 

    <div id="wrapper">
    
       
       
       <div id="topline"> 
            <div id="topline_in">
                <!--div id="social_links">
                    <div class="share_links">
                        <a href="#">
                            <img src="<?php echo get_bloginfo('stylesheet_directory')?>/images/facebook-head.png">
                        </a>
                    </div>
                    <div class="share_links">
                        <a href="#">
                            <img src="<?php echo get_bloginfo('stylesheet_directory')?>/images/twitter-head.png">
                        </a>
                    </div>
                    <div class="share_links">
                        <a href="#">
                            <img src="<?php echo get_bloginfo('stylesheet_directory')?>/images/google-head.png">
                        </a>
                    </div>
                </div--> 

                <?php 
                    if ( is_user_logged_in() ){
                        ?>
                        <ul>
                            <?php
                            global $woocommerce;
                                $count = $woocommerce->cart->get_cart_contents_count();
                            ?>
                            <li>
                                <a class="link_footer" href="<?php echo get_permalink(6);?>">
                                    my cart 
                                </a>
                                <span class="counter_item "><?php echo $count; ?></span>
                            </li>
                            <li>
                                <a class="link_footer" href="<?php echo get_permalink(9);?>">
                                    my account
                                </a>
                            </li>
                            <li>
                                <a class="link_footer" href="<?php echo wp_logout_url( get_permalink() );?>">
                                   Logout
                                </a>
                            </li>
                        </ul>
                    <?php
                    }
                    else{ 
                        ?>
                        <ul>
                            <!--li class="counter_item">
                                <a href="" class="link_footer">
                                    my cart <span style="padding-left:15px; color:#1a1a1a; font-weight:bold;"><?php echo "0"; ?></span>
                                </a>
                            </li-->
                            
                            
                            <li>
                                <a class="link_footer" href="<?php echo get_permalink(9);?>">
                                    log in
                                </a>
                            </li>
                            <li>
                                <a class="link_footer" href="<?php echo get_permalink(67);?>">
                                    sign up
                                </a>
                            </li>
                        </ul>
                    <?php
                        }
                    ?>
                
                
                <style>
               #mailing_list{
                  background-color: #000000;
                  opacity: 0.5;
                  margin-left: auto;
                  margin-right: auto;
                  width: 960px;
                  min-height: 50px;
                  
                  color: red;
                  font-size: 14px;
                  font-family: Arial, Verdana;
               }
               
               #content_mailing_list{
                  margin-left: auto;
                  margin-right: auto;
                  width: 960px;
                  z-index: 101;
                  position: absolute;
                  text-align: center;
                  top: 50px;
                  padding-top: 5px;
                  min-height: 50px;
               }
               
               #label_mailing_list{
                  color:#ffffff;
                  font-style: italic;
                  font-weight: bold;
                  padding-right: 10px;
               }
               #link_policy_mailing{
                  color:#dcdcdc;
                  font-size: 12px;
                  display: inline-block;
                  font-weight: bold;
                  text-decoration: underline;
               }
               #btn_join_mailing{
                  background-color: #BECF34;
                  background: -moz-linear-gradient(
                     top,
                     ##BECF34 0%,
                     #164677);
                  background: -webkit-gradient(
                     linear, left top, left bottom, 
                     from(##BECF34),
                     to(#164677));
                   border: 1px solid #124170;             
                   color: #FFFFFF;
                   cursor: pointer;
                   font-family: "Lucida Sans","Lucida Grande",Garuda,sans-serif;
                   font-size: 19px;
                   font-weight: bold;
                   margin-bottom: 5px;
                   padding: 7px 23px;
                   white-space: nowrap;
                   display:inline-block;
                   font-style:italic;
               }
               
               #btn_close_mailing{
                  display:inline-block;
                  border-radius: 25px 25px 25px 25px;
                  width: 25px;
                  height: 25px;
                  background-color: #000000;  
                  color:#ffffff;
                  line-height: 20px;
                  font-size: 20px;
                  font-weight: bold;
                  float: right;
                  margin-right: 20px;
                  cursor: pointer;
               }
               
               #ref_msg{                                   
                 position: relative;
               }
               #alo_easymail_widget_feedback{                                    
                  color: red;
                  padding-left: 85px;
                  padding-right: 85px;
                  padding-bottom: 5px;
                  font-size: 14px;
                  font-family: Arial, Verdana;
               }
               #alo_easymail_page{
                  float: left;
                  width: auto;
                  color: white;
                  font-size: 12px;
               }
               #ref_msg{
                        height: 50px;
                     }
             </style>
                
               <script>
                  jQuery(document).ready(function($){
                     
                     $('#content_mailing_list').ready(function(){
                        rezise_msg_subscription();
                     });
                     
                     $('#btn_close_mailing').click(function(){
                        $('#content_mailing_list').css('display','none');
                        $('#mailing_list').css('display','none');
                     });
                   
                   
                     $('#btn_join_mailing').click(function(){
                        var name_subs = $('#name_subs').attr('value');
                        var email_subs = $('#email_subs').attr('value');
                        
                        $('#opt_name').attr('value', name_subs);
                        $('#opt_email').attr('value', email_subs);
                        
                        $('#alo_easymail_widget_form').submit();                        
                        rezise_msg_subscription();
                        
                        $('#name_subs').attr('value','');
                        $('#email_subs').attr('value','');
                     });
                     
                     
                     $('#alo_easymail_widget_feedback').on('ready','div',function(){                        
                        $('#msg_mailing').html( $('#alo_easymail_widget_feedback').html() );
                        rezise_msg_subscription();                        
                     });
                     
             
                     /*------------------------------------------------------------------*/
                     $('#alo_easymail_widget_feedback').remove().insertAfter('#ref_msg');
                     $('#alo_em_widget_loading').remove().insertAfter('#alo_easymail_widget_feedback');                     
                     $('#content_mailing_list p').remove().insertAfter('#alo_easymail_widget_feedback');
                     
                  });
                  
                  function rezise_msg_subscription()
                  {
                     //jQuery('#mailing_list').height( jQuery('#content_mailing_list').height() );
                  }
               </script>
                <div id="content_mailing_list">
                  <?php
                  echo do_shortcode('[ALO-EASYMAIL-PAGE]');
                  ?>
                  <?php
                  if(!is_user_logged_in())
                  {?>
                     <style>
                     
                     #alo_easymail_page{
                        visibility: hidden;
                        display: none;
                     }
                     </style>

                           <span id="label_mailing_list">JOIN OUR VIP MAILING LIST</span>
                           <input type="text" id="name_subs" placeholder="First Name..."/>
                           <input type="text" id="email_subs" placeholder="Email Address..."/>
                           <div id="btn_join_mailing">JOIN</div>

                   <?php
                  }?>
                  <a id="link_policy_mailing" href="<?php echo get_permalink(180)?>">Privacy Policy</a>
                  <div id="btn_close_mailing">x</div>                     
                </div>
            </div>
        </div>
       
       
       
       <div id="mailing_list">          
          <div id="ref_msg"></div>
       </div>
       
       
       
       
       
       
       
       
       
       
        
<div id="background">
  <div class="stripe">&nbsp;</div>
  <div class="fader">&nbsp;</div>
  <div class="stretch-image" style="background-image:url('<?php echo get_bloginfo('stylesheet_directory') ?>/images/big.jpg');"></div>
</div>

        <div id="container">
            <div id="container_wrapper">
                <div id="header">
                    <div id="top_header">
                        <div id="logo">
                        	<a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo get_bloginfo('stylesheet_directory') ?>/images/home_logo.png"></a>
                            
                        </div>
                        <div id="shiping_items">
                            <div id="car_shipping"><img src="<?php echo get_bloginfo('stylesheet_directory') ?>/images/car.png"></div>
                            <div id="text_of_shipping"><img src="<?php echo get_bloginfo('stylesheet_directory') ?>/images/banner-dhl1.png"></div>
                            <div id="safe_secure"><img src="<?php echo get_bloginfo('stylesheet_directory') ?>/images/banner.png"></div>
                        </div>
                    </div>
                    <!--div id="deals_menu">               
                            <a href="<?php echo get_permalink(62);?>" class="radius_left">Featured Deals</a>
                            <a href="<?php echo get_permalink(5);?>" class="radius_rigth">All Deals</a>
                    </div-->
                    <div id="bottom_header">
                        <?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' =>   'secondary' ) ); ?> 
                    </div>
                   
                </div> <!-- END #header-->

            	<div id="page_container">
            		<!--div id="navigation">
            			<nav id="site-navigation" class="main-navigation" role="navigation">						
							<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu' ) ); ?>
						</nav>                   
                    </div--><!-- END #navigation-->

                           
               
                <div id="page">