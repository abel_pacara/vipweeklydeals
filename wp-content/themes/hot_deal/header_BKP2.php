<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html style="margin-top:none !important;" <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />


<meta name="google-site-verification" content="aUn83QC5aQE_irnT3jwGqi3tGBph0ouqYwCaLiQPxw0" />

<style>
html
{
	margin-top: 0px !important;
}
</style>


<title><?php wp_title( '|', true, 'right' ); ?></title>
<!--link rel="profile" href="http://gmpg.org/xfn/11" /-->
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

<!-- script FAVICON -->
<link rel="shortcut icon" href="<?php bloginfo('stylesheet_directory'); ?>/favicon.ico" />
<!-- script FAVICON -->

<script type="text/javascript" src="<?php echo get_bloginfo('stylesheet_directory') ?>/js/iepngfix_tilebg.js"></script> 
<script type="text/javascript" src="<?php echo get_bloginfo('stylesheet_directory') ?>/js/ga.js"></script> 

<?php // Loads HTML5 JavaScript file to add support for HTML5 elements in older IE versions. ?>
<!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
<![endif]-->
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
   
   <script type="text/javascript">
     var _gaq = _gaq || [];
     _gaq.push(['_setAccount', 'UA-39522892-1']);
     _gaq.push(['_trackPageview']);

     (function() {
       var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
       ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
       var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
     })();
	 
	 /*
	 $(function() {
       
		   var zIndexNumber = 2000;
		   
		   $("div").each(function() {
				   $(this).css('zIndex', zIndexNumber);
				   zIndexNumber -= 10;
		   });
	});*/

   </script>

   
<!--div id="page" class="hfeed site">
	<header id="masthead" class="site-header" role="banner">
		<hgroup>
			<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
			<h2 class="site-description"><?php bloginfo( 'description' ); ?></h2>
		</hgroup>

		<nav id="site-navigation" class="main-navigation" role="navigation">
			<h3 class="menu-toggle"><?php _e( 'Menu', 'twentytwelve' ); ?></h3>
			<a class="assistive-text" href="#content" title="<?php esc_attr_e( 'Skip to content', 'twentytwelve' ); ?>"><?php _e( 'Skip to content', 'twentytwelve' ); ?></a>
			<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu' ) ); ?>
		</nav><!-- #site-navigation -->

		<?php $header_image = get_header_image();
		if ( ! empty( $header_image ) ) : ?>
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo esc_url( $header_image ); ?>" class="header-image" width="<?php echo get_custom_header()->width; ?>" height="<?php echo get_custom_header()->height; ?>" alt="" /></a>
		<?php endif; ?>
	<!--/header><!-- #masthead -->

	<!--div id="main" class="wrapper"-->
<style type="text/css">
img, div { behavior: url(iepngfix.htc) }
div { behavior: url(ie-css3.htc) }
</style> 

    <div id="wrapper">
    
       
       
       <div id="topline"> 
            <div id="topline_in">
                <!--div id="social_links">
                    <div class="share_links">
                        <a href="#">
                            <img src="<?php echo get_bloginfo('stylesheet_directory')?>/images/facebook-head.png">
                        </a>
                    </div>
                    <div class="share_links">
                        <a href="#">
                            <img src="<?php echo get_bloginfo('stylesheet_directory')?>/images/twitter-head.png">
                        </a>
                    </div>
                    <div class="share_links">
                        <a href="#">
                            <img src="<?php echo get_bloginfo('stylesheet_directory')?>/images/google-head.png">
                        </a>
                    </div>
                </div--> 

                <?php 
                    if ( is_user_logged_in() ){
                        ?>
                        <ul>
                            <?php
                            global $woocommerce;
                                $count = $woocommerce->cart->get_cart_contents_count();
                            ?>
                            <li>
                                <a class="link_footer" href="<?php echo get_permalink(6);?>">
                                    my cart 
                                </a>
                                <span class="counter_item "><?php echo $count; ?></span>
                            </li>
                            <li>
                                <a class="link_footer" href="<?php echo get_permalink(9);?>">
                                    my account
                                </a>
                            </li>
                            <li>
                                <a class="link_footer" href="<?php echo wp_logout_url( get_permalink() );?>">
                                   Logout
                                </a>
                            </li>
                        </ul>
                    <?php
                    }
                    else{ 
                        ?>
                        <ul>
                            <!--li class="counter_item">
                                <a href="" class="link_footer">
                                    my cart <span style="padding-left:15px; color:#1a1a1a; font-weight:bold;"><?php echo "0"; ?></span>
                                </a>
                            </li-->
                            
                            
                            <li>
                                <a class="link_footer" href="<?php echo get_permalink(9);?>">
                                    log in
                                </a>
                            </li>
                            <li>
                                <a class="link_footer" href="<?php echo get_permalink(67);?>">
                                    sign up
                                </a>
                            </li>
                        </ul>
                    <?php
                        }
                    ?>
            </div>
        </div>
       
       
       
       
       
       
       
       
       
       
        
<div id="background">
  <div class="stripe">&nbsp;</div>
  <div class="fader">&nbsp;</div>
  <div class="stretch-image" style="background-image:url('<?php echo get_bloginfo('stylesheet_directory') ?>/images/big.jpg');"></div>
</div>

        <div id="container">
            <div id="container_wrapper">
                <div id="header">
                    <div id="top_header">
                        <div id="logo">
                        	<a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo get_bloginfo('stylesheet_directory') ?>/images/home_logo.png"></a>
                            
                        </div>
                        <div id="shiping_items">
                            <div id="car_shipping"><img src="<?php echo get_bloginfo('stylesheet_directory') ?>/images/car.png"></div>
                            <div id="text_of_shipping"><img src="<?php echo get_bloginfo('stylesheet_directory') ?>/images/banner-dhl1.png"></div>
                            <div id="safe_secure"><img src="<?php echo get_bloginfo('stylesheet_directory') ?>/images/banner.png"></div>
                        </div>
                    </div>
                    <!--div id="deals_menu">               
                            <a href="<?php echo get_permalink(62);?>" class="radius_left">Featured Deals</a>
                            <a href="<?php echo get_permalink(5);?>" class="radius_rigth">All Deals</a>
                    </div-->
                    <div id="bottom_header">
                        <?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' =>   'secondary' ) ); ?> 
                    </div>
                   
                </div> <!-- END #header-->

            	<div id="page_container">
            		<!--div id="navigation">
            			<nav id="site-navigation" class="main-navigation" role="navigation">						
							<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu' ) ); ?>
						</nav>                   
                    </div--><!-- END #navigation-->

                           
               
                <div id="page">